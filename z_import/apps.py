from django.apps import AppConfig


class ZImportConfig(AppConfig):
    name = 'z_import'
