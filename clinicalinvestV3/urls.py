from django.conf import settings
"""clinicalinvestV3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls
# from django.views.static import serve
# from django.urls import re_path
# from .settings import MEDIA_ROOT

urlpatterns = [
    path('admin/', admin.site.urls),

    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('users/', include('myuser.urls')),
    path('prj001/', include('prj001.urls')),
    path('prj002/', include('prj002.urls')),
    path('prj003/', include('prj003.urls')),

    # 'show_indexes': True
    # re_path(r'^media/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT, 'show_indexes': True}),

    path('api-docs/', include_docs_urls(
        title='v3.0-流调API',
        authentication_classes=[],
        permission_classes=[])),
]


if settings.DEBUG:
    urlpatterns.append(path('prj999/', include('prj999.urls')),)
