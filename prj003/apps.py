from django.apps import AppConfig


class Prj003Config(AppConfig):
    name = 'prj003'
    verbose_name = '项目3-原发性痛经临床流行病学'
