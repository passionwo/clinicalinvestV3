from django.db import models
from django.core.validators import RegexValidator, EmailValidator

from .models import CreateTimeModel

from utils.validators import ValidatorModelBirth

from myuser.models import MyUser


class QuestionNaire(CreateTimeModel):
    """
    调查问卷
    """
    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='questionnaire')

    HOSPITAL_BS = (
        ('省级医院', '省级医院'),
        ('市级医院', '市级医院'),
        ('区/县级医院', '区/县级医院')
    )

    TITLE_POSITION = (
        ('主任医师', '主任医师'),
        ('副主任医师', '副主任医师'),
        ('主治医师', '主治医师')
    )

    CAREER_CHOICE = (
        ('学生', '学生'),
        ('个体', '个体'),
        ('农民', '农民'),
        ('军人', '军人'),
        ('工人', '工人'),
        ('财会人员', '财会人员'),
        ('技术人员', '技术人员'),
        ('服务业', '服务业'),
        ('科教文卫', '科教文卫'),
        ('行政管理', '行政管理'),
        ('无业', '无业'),
        ('其它', '其它'),
    )

    ENTRANCE_CHOICE = (
        ('门诊', '门诊'),
        ('学校', '学校'),
    )

    CULTURE_CHOICE = (
        ('小学及以下', '小学及以下'),
        ('初中', '初中'),
        ('高中/中专', '高中/中专'),
        ('大专', '大专'),
        ('本科', '本科'),
        ('研究生及以上', '研究生及以上'),
        ('未接受国家教育(文盲)', '未接受国家教育(文盲)'),
    )

    serial = models.CharField(unique=True, max_length=12, validators=[RegexValidator(regex=r'^\d{12}$', message='必须为12位数字', code='questionnaire'), ], verbose_name='问卷编码', help_text='问卷编码')

    patient_name = models.CharField(max_length=20, verbose_name='患者姓名', help_text='患者姓名')

    patient_date = models.DateField(verbose_name='就诊日期', help_text='就诊日期')

    hospital_name = models.CharField(max_length=50, verbose_name='医院名称', help_text='医院名称')

    hospital_belong = models.CharField(max_length=10, choices=HOSPITAL_BS, verbose_name='医院所属', help_text='医院所属')

    patient_phone = models.CharField(max_length=11, validators=[RegexValidator(regex=r'^\d{11}$', message='必须为11位数字', code='patient-phone'), ], verbose_name='患者电话', help_text='患者电话')

    expert_name = models.CharField(max_length=20, verbose_name='填表专家姓名', help_text='填表专家姓名')

    expert_hospital = models.CharField(max_length=50, verbose_name='填表专家单位', help_text='填表专家单位')

    expert_phone = models.CharField(max_length=11, validators=[RegexValidator(regex=r'^\d{11}$', message='必须为11位数字', code='expert-phone'), ], verbose_name='填表专家电话', help_text='填表专家电话')

    expert_email = models.CharField(max_length=50, validators=[EmailValidator(message='非法邮箱', code='expert_email'), ], verbose_name='填表专家邮箱', help_text='填表专家邮箱')

    expert_title = models.CharField(max_length=20, choices=TITLE_POSITION, verbose_name='填表专家职称', help_text='填表专家职称')

    patient_birth = models.CharField(max_length=7, validators=[
        RegexValidator(regex=r'^\d{4}-\d{2}$', message='必须符合 [YYYY-MM] 格式', code='patient-birth'),
        ValidatorModelBirth(7)
    ], help_text="出生日期[YYYY-MM]", verbose_name='出生[YYYY-MM]', blank=True, null=True,)

    patient_height = models.IntegerField(blank=True, null=True, verbose_name='身高', help_text='身高')

    patient_weight = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True, verbose_name='体重', help_text='体重')

    patient_address = models.CharField(max_length=100, blank=True, null=True, verbose_name='病人现住址', help_text='病人现住址')

    nation = models.CharField(max_length=20, blank=True, null=True, verbose_name='民族', help_text='民族')

    career = models.CharField(max_length=10, choices=CAREER_CHOICE, blank=True, null=True, verbose_name='职业', help_text='职业')

    environment_shileng = models.BooleanField(blank=True, null=True, verbose_name='湿冷', help_text='湿冷')

    environment_yeban = models.BooleanField(blank=True, null=True, verbose_name='夜班/熬夜', help_text='夜班/熬夜')

    environment_gaowen = models.BooleanField(blank=True, null=True, verbose_name='高温', help_text='高温')

    environment_diwen = models.BooleanField(blank=True, null=True, verbose_name='低温', help_text='低温')

    environment_zaosheng = models.BooleanField(blank=True, null=True, verbose_name='噪声', help_text='噪声')

    environment_fushe = models.BooleanField(blank=True, null=True, verbose_name='辐射', help_text='辐射')

    environment_huagong = models.BooleanField(blank=True, null=True, verbose_name='化工印染', help_text='化工印染')

    environment_julie = models.BooleanField(blank=True, null=True, verbose_name='剧烈运动', help_text='剧烈运动')

    environment_qiyou = models.BooleanField(blank=True, null=True, verbose_name='汽油', help_text='汽油')

    environment_gaokong = models.BooleanField(blank=True, null=True, verbose_name='高空', help_text='高空')

    environment_wu = models.BooleanField(blank=True, null=True, verbose_name='无', help_text='无')

    entrance = models.CharField(max_length=10, blank=True, null=True, choices=ENTRANCE_CHOICE, verbose_name='病人来源', help_text='病人来源')

    entrance_qita = models.CharField(max_length=20, blank=True, null=True, verbose_name='病人来源-其他', help_text='病人来源-其他')

    yinshi_wuteshu = models.BooleanField(blank=True, null=True, verbose_name='无特殊', help_text='无特殊')

    yinshi_shengleng = models.BooleanField(blank=True, null=True, verbose_name='生冷', help_text='生冷')

    yinshi_xinla = models.BooleanField(blank=True, null=True, verbose_name='辛辣', help_text='辛辣')

    yinshi_sushi = models.BooleanField(blank=True, null=True, verbose_name='素食', help_text='素食')

    yinshi_cafei = models.BooleanField(blank=True, null=True, verbose_name='含咖啡因食物或饮品', help_text='含咖啡因食物或饮品')

    yinshi_suan = models.BooleanField(blank=True, null=True, verbose_name='酸', help_text='酸')

    yinshi_tian = models.BooleanField(blank=True, null=True, verbose_name='甜', help_text='甜')

    yinshi_xian = models.BooleanField(blank=True, null=True, verbose_name='咸', help_text='咸')

    yinshi_you = models.BooleanField(blank=True, null=True, verbose_name='油', help_text='油')

    yinshi_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='饮食偏好-其他', help_text='饮食偏好-其他')

    degree_of_completion = models.CharField(blank=True, null=True, verbose_name='完成度', max_length=8, help_text='完成度')

    CHECKED_CHOICE = (
        ('未审核', '未审核'),
        ('审核通过', '审核通过'),
        ('审核不通过', '审核不通过'),
    )
    check_status = models.CharField(blank=True, null=True, verbose_name='审核状态', max_length=6, choices=CHECKED_CHOICE, default='未审核', help_text='审核状态')

    reason_for_check = models.CharField(blank=True, null=True, verbose_name='审核不通过的原因', max_length=200, help_text='审核不通过的原因')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '问卷信息'
        verbose_name_plural = verbose_name
        permissions = (
            ('prj003_all_permissions', 'All Permissions'),
            ('prj003_other_permissions', 'Only User Permissions')
        )
