from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class History(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='history', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_history')

    has_history = models.BooleanField(verbose_name='是否存在既往史', help_text='是否存在既往史', blank=True, null=True)

    history_content = models.CharField(max_length=50, verbose_name='既往史', help_text='既往史', blank=True, null=True)

    hobby_wu = models.BooleanField(verbose_name='无', help_text='无', blank=True, null=True)

    hobby_xiyan = models.BooleanField(verbose_name='吸烟', help_text='吸烟', blank=True, null=True)

    hobby_yinjiu = models.BooleanField(verbose_name='饮酒', help_text='饮酒', blank=True, null=True)

    hobby_qita = models.CharField(max_length=50, verbose_name='特殊嗜好-其他', help_text='特殊嗜好-其他', blank=True, null=True)

    PHYSICAL_CHOICE = (
        ('好', '好'),
        ('一般', '一般'),
        ('易疲倦', '易疲倦'),
    )
    physical = models.CharField(max_length=5, choices=PHYSICAL_CHOICE, verbose_name='体力状况', help_text='体力状况', blank=True, null=True)

    physical_career_choice = (
        ('重体力劳动（如：搬运工、清洁工、农场工人、畜牧场工人等）', '重体力劳动（如：搬运工、清洁工、农场工人、畜牧场工人等）'),
        ('中体力劳动（如：家政服务人员、服务生、厨师、护士等）', '中体力劳动（如：家政服务人员、服务生、厨师、护士等）'),
        ('轻体力劳动（如：教师、美容美发师、批发商、职员等）', '轻体力劳动（如：教师、美容美发师、批发商、职员等）'),
        ('坐式的工作（如：收银员、出纳员、接线员、秘书等）', '坐式的工作（如：收银员、出纳员、接线员、秘书等）'),
    )
    physical_career = models.CharField(max_length=50, choices=PHYSICAL_CHOICE, verbose_name='职业体力活动', help_text='职业体力活动', blank=True, null=True)

    is_exercise = models.BooleanField(blank=True, null=True, verbose_name='体育锻炼', help_text='体育锻炼')

    EXERCISE_PINCI_CHOICE = (
        ('很少（≤1次/周）', '很少（≤1次/周）'),
        ('偶尔（≤3次/周）', '偶尔（≤3次/周）'),
        ('经常（≥4次/周）', '经常（≥4次/周）'),
    )
    exercise_pinci = models.CharField(blank=True, null=True, max_length=10, choices=EXERCISE_PINCI_CHOICE, verbose_name='锻炼频次', help_text='锻炼频次')

    EXERCISE_LEVEL_CHOICE = (
        ('一般(少量出汗，心率≤120次/分)', '一般(少量出汗，心率≤120次/分)'),
        ('高强度(大汗淋漓，心率>120次/分)', '高强度(大汗淋漓，心率>120次/分)'),
    )
    exercise_qiangdu = models.CharField(blank=True, null=True, max_length=50, choices=EXERCISE_LEVEL_CHOICE, verbose_name='锻炼强度', help_text='锻炼强度')

    FRIST_TIME_CHOICE = (
        ('<11岁', '<11岁'),
        ('11岁', '11岁'),
        ('12岁', '12岁'),
        ('13岁', '13岁'),
        ('14岁', '14岁'),
        ('15岁', '15岁'),
        ('16岁', '16岁'),
        ('>16岁', '>16岁')
    )
    first_time = models.CharField(blank=True, null=True, max_length=5, choices=FRIST_TIME_CHOICE, verbose_name='月经初潮年龄', help_text='月经初潮年龄')

    is_normal = models.BooleanField(blank=True, null=True, verbose_name='月经周期是否规律', help_text='月经周期是否规律')

    NORMAL_CHOICE = (
        ('21~25天', '21~25天'),
        ('26~30天', '26~30天'),
        ('31~35天', '31~35天')
    )
    normal = models.CharField(blank=True, null=True, max_length=10, choices=NORMAL_CHOICE, verbose_name='尚规律', help_text='尚规律')

    NOT_NORMAL_CHOICE = (
        ('36~60天', '36~60天'),
        ('61~90天', '61~90天 '),
    )
    not_normal = models.CharField(blank=True, null=True, max_length=10, choices=NOT_NORMAL_CHOICE, verbose_name='不规律', help_text='不规律')

    not_normal_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='不规律-其他', help_text='不规律-其他')

    TOTAL_DAY_CHOICE = (
        ('1天', '1天'),
        ('2天', '2天'),
        ('3天', '3天'),
        ('4天', '4天'),
        ('5天', '5天'),
        ('6天', '6天'),
        ('7天', '7天')
    )
    total_days = models.CharField(blank=True, null=True, max_length=3, choices=TOTAL_DAY_CHOICE, verbose_name='月经经期', help_text='月经经期')

    total_days_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='月经经期-其他', help_text='月经经期-其他')

    last_time = models.DateField(blank=True, null=True, verbose_name='末次行经日期', help_text='末次行经日期')

    liang_choice = (
        ('正常', '正常'),
        ('量少', '量少'),
        ('量多', '量多')
    )
    daixia_liang = models.CharField(blank=True, null=True, max_length=3, verbose_name='平素带下情况-量', help_text='平素带下情况-量')

    se_choice = (
        ('色白', '色白'),
        ('色黄', '色黄'),
        ('透明', '透明'),
        ('色黄绿', '色黄绿'),
    )
    daixia_se = models.CharField(blank=True, null=True, max_length=3, verbose_name='平素带下情况-色', help_text='平素带下情况-色')
    daixia_se_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='平素带下情况-色(其他)', help_text='平素带下情况-色(其他)')

    zhi_choice = (
        ('黏而不稠', '黏而不稠'),
        ('质清稀', '质清稀'),
        ('质稠', '质稠')
    )
    daixia_zhi = models.CharField(blank=True, null=True, max_length=3, verbose_name='平素带下情况-质', help_text='平素带下情况-质')

    body_all = models.BooleanField(blank=True, null=True, verbose_name='平素全身症状', help_text='平素全身症状')

    SPIRIT_CHOICE = (
        ('神疲肢倦', '神疲肢倦'),
        ('气短懒言', '气短懒言'),
        ('精神抑郁', '精神抑郁'),
        ('烦躁易怒', '烦躁易怒'),
        ('时欲叹息', '时欲叹息'),
    )
    body_spirit = models.CharField(choices=SPIRIT_CHOICE, max_length=5, blank=True, null=True, verbose_name='精神情绪', help_text='精神情绪')
    body_spirit_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='精神情绪-其他', help_text='精神情绪-其他')

    FACE_HEAD_CHOICE = (
        ('正常', '正常'),
        ('面色萎黄', '面色萎黄'),
        ('面色晦暗', '面色晦暗'),
        ('面有黯斑', '面有黯斑'),
        ('面浮肢肿', '面浮肢肿'),
        ('颧赤唇红', '颧赤唇红'),
        ('口苦咽干', '口苦咽干'),
        ('头晕耳鸣', '头晕耳鸣'),
        ('眼花', '眼花'),
    )
    body_face_head = models.CharField(choices=FACE_HEAD_CHOICE, max_length=5, blank=True, null=True, verbose_name='头面', help_text='头面')

    body_face_head_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='头面-其他', help_text='头面-其他')

    BODY_LIMB_CHOICE = (
        ('正常', '正常'),
        ('形体肥胖', '形体肥胖'),
        ('形体瘦小', '形体瘦小'),
        ('畏寒肢冷', '畏寒肢冷'),
        ('手足心热', '手足心热'),
        ('腰酸腿软', '腰酸腿软'),
        ('腰痛如折', '腰痛如折'),
    )
    body_limb = models.CharField(choices=BODY_LIMB_CHOICE, max_length=5, blank=True, null=True, verbose_name='形体、四肢', help_text='形体、四肢')
    body_limb_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='形体、四肢-其他', help_text='形体、四肢-其他')

    BODY_BELLY_CHOICE = (
        ('胸闷不舒', '胸闷不舒'),
        ('经前乳房胀痛', '经前乳房胀痛'),
        ('经前胸胁胀痛', '经前胸胁胀痛'),
        ('经前少腹胀痛', '经前少腹胀痛'),
        ('腹空坠', '腹空坠'),
        ('小腹空痛', '小腹空痛'),
        ('小腹刺痛', '小腹刺痛'),
        ('小腹胀痛', '小腹胀痛'),
        ('小腹冷痛', '小腹冷痛'),
        ('小腹隐痛', '小腹隐痛'),
    )

    body_belly = models.CharField(choices=BODY_BELLY_CHOICE, max_length=8, blank=True, null=True, verbose_name='胸腹部', help_text='胸腹部')
    body_belly_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='胸腹部-其他', help_text='胸腹部-其他')

    BODY_SLEEP_CHOICE = (
        ('心悸失眠', '心悸失眠'),
        ('夜寐不宁', '夜寐不宁'),
        ('夜寐梦多', '夜寐梦多'),
    )
    body_sleep = models.CharField(choices=BODY_SLEEP_CHOICE, max_length=5, blank=True, null=True, verbose_name='睡眠', help_text='睡眠')
    body_sleep_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='睡眠-其他', help_text='睡眠-其他')

    MARRIAGE_HISTORY_CHOICE = (
        ('无性生活', '无性生活'),
        ('未婚有性生活', '未婚有性生活'),
        ('已婚同居', '已婚同居'),
        ('已婚分居', '已婚分居'),
        ('离婚', '离婚'),
        ('丧偶', '丧偶'),
    )
    marriage_history = models.CharField(choices=MARRIAGE_HISTORY_CHOICE, max_length=8, blank=True, null=True, verbose_name='婚姻史', help_text='婚姻史')

    pregnancy_yun = models.IntegerField(blank=True, null=True, verbose_name='孕产史-孕', help_text='孕产史-孕')

    pregnancy_shun = models.IntegerField(blank=True, null=True, verbose_name='孕产史-顺产', help_text='孕产史-顺产')

    pregnancy_pou = models.IntegerField(blank=True, null=True, verbose_name='孕产史-剖宫产', help_text='孕产史-剖宫产')

    pregnancy_zao = models.IntegerField(blank=True, null=True, verbose_name='孕产史-早产', help_text='孕产史-早产')

    pregnancy_yao = models.IntegerField(blank=True, null=True, verbose_name='孕产史-药物流产', help_text='孕产史-药物流产')

    pregnancy_ren = models.IntegerField(blank=True, null=True, verbose_name='孕产史-人工流产', help_text='孕产史-人工流产')

    pregnancy_zi = models.IntegerField(blank=True, null=True, verbose_name='孕产史-自然流产', help_text='孕产史-自然流产')

    pregnancy_yi = models.IntegerField(blank=True, null=True, verbose_name='孕产史-异位妊娠', help_text='孕产史-异位妊娠')

    pregnancy_qing = models.IntegerField(blank=True, null=True, verbose_name='孕产史-清宫术', help_text='孕产史-清宫术')

    pregnancy_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='孕产史-其他', help_text='孕产史-其他')

    biyun_wu = models.BooleanField(blank=True, null=True, verbose_name='无', help_text='无')
    biyun_jiezha = models.BooleanField(blank=True, null=True, verbose_name='结扎', help_text='结扎')
    biyun_gongnei = models.BooleanField(blank=True, null=True, verbose_name='宫内节育器', help_text='宫内节育器')
    biyun_biyun = models.BooleanField(blank=True, null=True, verbose_name='避孕套', help_text='避孕套')
    biyun_koufu = models.BooleanField(blank=True, null=True, verbose_name='口服避孕药', help_text='口服避孕药')
    biyun_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='避孕措施-其他', help_text='避孕措施-其他')

    FAMILY_HISTORY_CHOICE = (
        ('无', '无'),
        ('有', '有'),
        ('不详', '不详'),
    )
    family_history = models.CharField(max_length=3, choices=FAMILY_HISTORY_CHOICE, blank=True, null=True, verbose_name='家族史', help_text='家族史')

    FAMILY_HISTORY_IS_ORIGIN_CHOICE = (
        ('是', '是'),
        ('否', '否'),
        ('不详', '不详'),
    )
    family_history_is_origin = models.CharField(max_length=3, choices=FAMILY_HISTORY_IS_ORIGIN_CHOICE, blank=True, null=True, verbose_name='是否为原发性', help_text='是否为原发性')
    family_history_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='家族史-其他', help_text='家族史-其他')
    RELATIVE_HISTORY_CHOICE = (
        ('无', '无'),
        ('有', '有'),
        ('不详', '不详'),
    )
    relative_history = models.CharField(max_length=3, choices=RELATIVE_HISTORY_CHOICE, blank=True, null=True, verbose_name='一级亲属（父母、兄弟姐妹、子女）其他疾病史', help_text='一级亲属（父母、兄弟姐妹、子女）其他疾病史')

    relative_history_you = models.CharField(max_length=50, blank=True, null=True, verbose_name='一级亲属（父母、兄弟姐妹、子女）其他疾病史-有', help_text='一级亲属（父母、兄弟姐妹、子女）其他疾病史-有')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '病史'
        verbose_name_plural = verbose_name
