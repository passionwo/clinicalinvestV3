from django.db import models


class CreateTimeModel(models.Model):
    """
    : 抽象类 => 信息创建时间
    """
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', help_text='创建时间')

    class Meta:
        abstract = True
