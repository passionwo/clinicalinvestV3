from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class Relevant(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='relevant', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_relevant')

    body_check_wu = models.BooleanField(blank=True, null=True, verbose_name='无', help_text='无')

    body_check_fuke = models.BooleanField(blank=True, null=True, verbose_name='妇科检查', help_text='妇科检查')

    body_check_quan = models.BooleanField(blank=True, null=True, verbose_name='全身检查', help_text='全身检查')

    assist_check_wu = models.BooleanField(blank=True, null=True, verbose_name='无', help_text='无')

    assist_check_chao = models.BooleanField(blank=True, null=True, verbose_name='超声', help_text='超声')

    assist_check_mri = models.BooleanField(blank=True, null=True, verbose_name='盆腔MRI检查', help_text='盆腔MRI检查')

    assist_check_gong = models.BooleanField(blank=True, null=True, verbose_name='宫腔镜检查', help_text='宫腔镜检查')

    assist_check_fu = models.BooleanField(blank=True, null=True, verbose_name='腹腔镜检查', help_text='腹腔镜检查')

    assist_check_zi = models.BooleanField(blank=True, null=True, verbose_name='子宫输卵管造影', help_text='子宫输卵管造影')

    assist_check_ji = models.BooleanField(blank=True, null=True, verbose_name='基础体温测定', help_text='基础体温测定')

    assist_check_ji_dan = models.BooleanField(blank=True, null=True, verbose_name='单相', help_text='单相')

    assist_check_ji_shuang = models.BooleanField(blank=True, null=True, verbose_name='双相', help_text='双相')

    assist_check_pen = models.BooleanField(blank=True, null=True, verbose_name='盆腔血流图检查', help_text='盆腔血流图检查')

    assist_check_jing = models.BooleanField(blank=True, null=True, verbose_name='经血前列腺素测定', help_text='经血前列腺素测定')

    check_result = models.CharField(max_length=200, blank=True, null=True, verbose_name='无', help_text='无')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '相关检查'
        verbose_name_plural = verbose_name
