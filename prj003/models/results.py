from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class Results(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='results', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_results')

    VAS_CHIOCE = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )
    vas = models.CharField(max_length=2, choices=VAS_CHIOCE, blank=True, null=True, verbose_name='VAS评分', help_text='VAS评分')

    RESULT_CHOICE = (
        ('无效', '无效'),
        ('好转', '好转'),
        ('痊愈', '痊愈'),
    )
    result = models.CharField(max_length=2, choices=RESULT_CHOICE, blank=True, null=True, verbose_name='自评', help_text='自评')

    result_days = models.IntegerField(blank=True, null=True, verbose_name='周期', help_text='周期')

    result_reduce = models.CharField(max_length=5, blank=True, null=True, verbose_name='减轻%', help_text='减轻%')

    STOP_MEDICINE_CHOICE = (('是', '是'), ('否', '否'))
    stop_medicine = models.CharField(max_length=2, choices=STOP_MEDICINE_CHOICE, blank=True, null=True, verbose_name='停药3个月经周期后复发', help_text='停药3个月经周期后复发')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '疗效'
        verbose_name_plural = verbose_name
