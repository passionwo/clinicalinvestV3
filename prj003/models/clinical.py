from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class Clinical(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='clinical', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_clinical')

    zhong_tongjing = models.BooleanField(blank=True, null=True, verbose_name='痛经', help_text='痛经')

    SHIZHENG_CHOICE = (
        ('寒凝血瘀证', '寒凝血瘀证'),
        ('气滞血瘀证', '气滞血瘀证'),
        ('湿热瘀阻证', '湿热瘀阻证'),
        ('肝郁化火证', '肝郁化火证'),
    )
    shizheng = models.CharField(max_length=6, choices=SHIZHENG_CHOICE, blank=True, null=True, verbose_name='辨证分型-实证', help_text='辨证分型-实证')

    shizheng_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='辨证分型-实证-其他', help_text='辨证分型-实证-其他')

    XUZHENG_CHOICE = (
        ('肾虚证', '肾虚证'),
        ('肾气亏虚证', '肾气亏虚证'),
        ('肝肾亏损证', '肝肾亏损证'),
        ('阳虚内寒证', '阳虚内寒证'),
        ('气血虚弱证', '气血虚弱证'),
    )
    xuzheng = models.CharField(max_length=6, choices=XUZHENG_CHOICE, blank=True, null=True, verbose_name='辨证分型-虚症', help_text='辨证分型-虚症')

    xuzheng_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='辨证分型-虚症-其他', help_text='辨证分型-虚症-其他')

    xushi = models.CharField(max_length=200, blank=True, null=True, verbose_name='虚实夹杂', help_text='虚实夹杂')

    xi_tongjing = models.BooleanField(blank=True, null=True, verbose_name='原发性痛经', help_text='原发性痛经')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '临床诊断'
        verbose_name_plural = verbose_name
