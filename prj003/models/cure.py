from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class Cure(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='cure', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_cure')

    is_together_cure = models.BooleanField(blank=True, null=True, verbose_name='中西医结合治疗', help_text='中西医结合治疗')

    SHIZHENG_CURE_CHOICE = (
        ('温经散寒，化瘀止痛', '温经散寒，化瘀止痛'),
        ('行气活血，化瘀止痛', '行气活血，化瘀止痛'),
        ('清热除湿，化瘀止痛', '清热除湿，化瘀止痛'),
        ('清热解郁，化瘀止痛', '清热解郁，化瘀止痛'),
    )
    shizheng_cure = models.CharField(max_length=10, choices=SHIZHENG_CURE_CHOICE, blank=True, null=True, verbose_name='实证治法', help_text='实证治法')

    shizheng_cure_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='实证治法-其他', help_text='实证治法-其他')

    XUZHENG_CURE_CHOICE = (
        ('补肾通络，化瘀止痛', '补肾通络，化瘀止痛'),
        ('补肾填精，养血止痛', '补肾填精，养血止痛'),
        ('滋养肝肾，缓急止痛', '滋养肝肾，缓急止痛'),
        ('温经扶阳，暖宫止痛', '温经扶阳，暖宫止痛'),
        ('益气养血，调经止痛', '益气养血，调经止痛'),
    )
    xuzheng_cure = models.CharField(max_length=10, choices=XUZHENG_CURE_CHOICE, blank=True, null=True, verbose_name='虚证治法', help_text='虚证治法')

    xuzheng_cure_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='虚证治法-其他', help_text='虚证治法-其他')

    xushi_cure = models.CharField(max_length=50, blank=True, null=True, verbose_name='虚实夹杂证治法', help_text='虚实夹杂证治法')

    shizheng_shaofu = models.BooleanField(blank=True, null=True, verbose_name='少腹逐瘀汤', help_text='少腹逐瘀汤')

    shizheng_wenjing = models.BooleanField(blank=True, null=True, verbose_name='温经汤（《妇人大全良方》）', help_text='温经汤（《妇人大全良方》）')

    shizheng_gexia = models.BooleanField(blank=True, null=True, verbose_name='膈下逐瘀汤', help_text='膈下逐瘀汤')

    shizheng_jiawei = models.BooleanField(blank=True, null=True, verbose_name='加味乌药汤', help_text='加味乌药汤')

    shizheng_qingre = models.BooleanField(blank=True, null=True, verbose_name='清热调血汤', help_text='清热调血汤')

    shizheng_xuanyu = models.BooleanField(blank=True, null=True, verbose_name='宣郁通经汤', help_text='宣郁通经汤')

    shizheng_taohong = models.BooleanField(blank=True, null=True, verbose_name='桃红四物汤', help_text='桃红四物汤')

    shizheng_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='实证代表方-其他', help_text='实证代表方-其他')

    xuzheng_juejin = models.BooleanField(blank=True, null=True, verbose_name='决津煎合折冲饮', help_text='决津煎合折冲饮')

    xuzheng_yishen = models.BooleanField(blank=True, null=True, verbose_name='益肾调经汤', help_text='益肾调经汤')

    xuzheng_tiaogan = models.BooleanField(blank=True, null=True, verbose_name='调肝汤', help_text='调肝汤')

    xuzheng_wenjing = models.BooleanField(blank=True, null=True, verbose_name='温经汤（《金匮要略》）', help_text='温经汤（《金匮要略》）')

    xuzheng_shengyu = models.BooleanField(blank=True, null=True, verbose_name='圣愈汤', help_text='圣愈汤')

    xuzheng_huangqi = models.BooleanField(blank=True, null=True, verbose_name='黄芪建中汤', help_text='黄芪建中汤')

    xuzheng_yimu = models.BooleanField(blank=True, null=True, verbose_name='八珍益母汤', help_text='八珍益母汤')

    xuzheng_bazhen = models.BooleanField(blank=True, null=True, verbose_name='八珍汤', help_text='八珍汤')

    xuzheng_siwu = models.BooleanField(blank=True, null=True, verbose_name='四物汤', help_text='四物汤')

    xuzheng_jianzhong = models.BooleanField(blank=True, null=True, verbose_name='当归建中汤', help_text='当归建中汤')

    xuzheng_yiguan = models.BooleanField(blank=True, null=True, verbose_name='一贯煎', help_text='一贯煎')

    xuzheng_sini = models.BooleanField(blank=True, null=True, verbose_name='当归四逆汤', help_text='当归四逆汤')

    xuzheng_sahoyao = models.BooleanField(blank=True, null=True, verbose_name='当归芍药散', help_text='当归芍药散')

    xuzheng_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='虚证代表方-其他', help_text='虚证代表方-其他')

    xushi_represent = models.CharField(max_length=50, blank=True, null=True, verbose_name='虚实夹杂证代表方', help_text='虚实夹杂证代表方')

    medicine_tongjingbao = models.BooleanField(blank=True, null=True, verbose_name='痛经宝颗粒', help_text='痛经宝颗粒')
    medicine_tongjing = models.BooleanField(blank=True, null=True, verbose_name='痛经片/丸', help_text='痛经片/丸')
    medicine_yuanhu = models.BooleanField(blank=True, null=True, verbose_name='元胡止痛片/胶囊/滴丸', help_text='元胡止痛片/胶囊/滴丸')
    medicine_shaofu = models.BooleanField(blank=True, null=True, verbose_name='少腹逐瘀丸/胶囊/颗粒', help_text='少腹逐瘀丸/胶囊/颗粒')
    medicine_xuefu = models.BooleanField(blank=True, null=True, verbose_name='血府逐瘀胶囊/丸/口服液/颗粒/片', help_text='血府逐瘀胶囊/丸/口服液/颗粒/片')
    medicine_aifu = models.BooleanField(blank=True, null=True, verbose_name='艾附暖宫丸', help_text='艾附暖宫丸')
    medicine_yimu = models.BooleanField(blank=True, null=True, verbose_name='益母草颗粒/胶囊/膏', help_text='益母草颗粒/胶囊/膏')
    medicine_dane = models.BooleanField(blank=True, null=True, verbose_name='丹莪妇康煎膏', help_text='丹莪妇康煎膏')
    medicine_jingdai = models.BooleanField(blank=True, null=True, verbose_name='经带宁胶囊', help_text='经带宁胶囊')
    medicine_tianqi = models.BooleanField(blank=True, null=True, verbose_name='田七痛经胶囊', help_text='田七痛经胶囊')
    medicine_xiaoyao = models.BooleanField(blank=True, null=True, verbose_name='逍遥丸', help_text='逍遥丸')
    medicine_danzhi = models.BooleanField(blank=True, null=True, verbose_name='丹栀逍遥丸/胶囊', help_text='丹栀逍遥丸/胶囊')
    medicine_zaizao = models.BooleanField(blank=True, null=True, verbose_name='妇科再造胶囊', help_text='妇科再造胶囊')
    medicine_jinfo = models.BooleanField(blank=True, null=True, verbose_name='金佛止痛丸', help_text='金佛止痛丸')
    medicine_dangguitiaojing = models.BooleanField(blank=True, null=True, verbose_name='当归调经颗粒', help_text='当归调经颗粒')
    medicine_liqi = models.BooleanField(blank=True, null=True, verbose_name='九气拈痛丸', help_text='九气拈痛丸')
    medicine_dingkun = models.BooleanField(blank=True, null=True, verbose_name='定坤丹', help_text='定坤丹')
    medicine_guizhi = models.BooleanField(blank=True, null=True, verbose_name='桂枝茯苓丸', help_text='桂枝茯苓丸')
    medicine_sanjie = models.BooleanField(blank=True, null=True, verbose_name='散结镇痛胶囊', help_text='散结镇痛胶囊')
    medicine_huahong = models.BooleanField(blank=True, null=True, verbose_name='花红片/胶囊', help_text='花红片/胶囊')
    medicine_qianzhi = models.BooleanField(blank=True, null=True, verbose_name='茜芷胶囊', help_text='茜芷胶囊')
    medicine_longxue = models.BooleanField(blank=True, null=True, verbose_name='龙血竭胶囊', help_text='龙血竭胶囊')
    medicine_funv = models.BooleanField(blank=True, null=True, verbose_name='妇女痛经丸', help_text='妇女痛经丸')
    medicine_qizhi = models.BooleanField(blank=True, null=True, verbose_name='七制香附丸', help_text='七制香附丸')
    medicine_nvjin = models.BooleanField(blank=True, null=True, verbose_name='女金丸/胶囊', help_text='女金丸/胶囊')
    medicine_bazhenyimu = models.BooleanField(blank=True, null=True, verbose_name='八珍益母丸/胶囊', help_text='八珍益母丸/胶囊')
    medicine_wuji = models.BooleanField(blank=True, null=True, verbose_name='乌鸡白凤丸', help_text='乌鸡白凤丸')

    medicine_nuangong = models.BooleanField(blank=True, null=True, verbose_name='暖宫七味丸', help_text='暖宫七味丸')
    medicine_yangkun = models.BooleanField(blank=True, null=True, verbose_name='妇科养坤丸', help_text='妇科养坤丸')
    medicine_tiaojing = models.BooleanField(blank=True, null=True, verbose_name='妇科调经片/颗粒', help_text='妇科调经片/颗粒')
    medicine_shiwei = models.BooleanField(blank=True, null=True, verbose_name='妇科十味片', help_text='妇科十味片')
    medicine_zhitong = models.BooleanField(blank=True, null=True, verbose_name='调经止痛片', help_text='调经止痛片')
    medicine_tjing = models.BooleanField(blank=True, null=True, verbose_name='妇科通经丸', help_text='妇科通经丸')
    medicine_babao = models.BooleanField(blank=True, null=True, verbose_name='八宝坤顺丸', help_text='八宝坤顺丸')
    medicine_ershiqi = models.BooleanField(blank=True, null=True, verbose_name='二十七味定坤丸', help_text='二十七味定坤丸')
    medicine_tj = models.BooleanField(blank=True, null=True, verbose_name='调经丸', help_text='调经丸')
    medicine_desheng = models.BooleanField(blank=True, null=True, verbose_name='得生丸', help_text='得生丸')
    medicine_fudesheng = models.BooleanField(blank=True, null=True, verbose_name='妇科得生丸', help_text='妇科得生丸')
    medicine_buzhong = models.BooleanField(blank=True, null=True, verbose_name='补中益气丸/颗粒', help_text='补中益气丸/颗粒')
    medicine_danggui = models.BooleanField(blank=True, null=True, verbose_name='当归片', help_text='当归片')
    medicine_duyiwei = models.BooleanField(blank=True, null=True, verbose_name='独一味片', help_text='独一味片')
    medicine_dusheng = models.BooleanField(blank=True, null=True, verbose_name='独圣活血片', help_text='独圣活血片')
    medicine_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='具体药物-其他', help_text='具体药物-其他')

    way_ti = models.BooleanField(blank=True, null=True, verbose_name='体针', help_text='体针')
    way_er = models.BooleanField(blank=True, null=True, verbose_name='耳针', help_text='耳针')
    way_fu = models.BooleanField(blank=True, null=True, verbose_name='腹针', help_text='腹针')
    way_ai = models.BooleanField(blank=True, null=True, verbose_name='艾灸', help_text='艾灸')
    way_dou = models.BooleanField(blank=True, null=True, verbose_name='耳穴压豆', help_text='耳穴压豆')
    way_she = models.BooleanField(blank=True, null=True, verbose_name='穴位注射', help_text='穴位注射')
    way_tie = models.BooleanField(blank=True, null=True, verbose_name='穴位敷贴', help_text='穴位敷贴')
    way_qi = models.BooleanField(blank=True, null=True, verbose_name='敷脐法', help_text='敷脐法')
    way_re = models.BooleanField(blank=True, null=True, verbose_name='热熨法', help_text='热熨法')
    way_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='中医其他治疗-其他', help_text='中医其他治疗-其他')

    is_zhitong = models.BooleanField(blank=True, null=True, verbose_name='应用止痛药', help_text='应用止痛药')

    xi_medicine_buluofen = models.BooleanField(blank=True, null=True, verbose_name='布洛芬片', help_text='布洛芬片')
    xi_medicine_fenbide = models.BooleanField(blank=True, null=True, verbose_name='芬必得缓释胶囊', help_text='芬必得缓释胶囊')
    xi_medicine_asipilin = models.BooleanField(blank=True, null=True, verbose_name='复方阿司匹林片', help_text='复方阿司匹林片')
    xi_medicine_qutong = models.BooleanField(blank=True, null=True, verbose_name='去痛片', help_text='去痛片')
    xi_medicine_xiaoyan = models.BooleanField(blank=True, null=True, verbose_name='消炎痛片', help_text='消炎痛片')
    xi_medicine_nimei = models.BooleanField(blank=True, null=True, verbose_name='尼美舒利颗粒', help_text='尼美舒利颗粒')
    xi_medicine_napu = models.BooleanField(blank=True, null=True, verbose_name='萘普生片', help_text='萘普生片')
    xi_medicine_jiafen = models.BooleanField(blank=True, null=True, verbose_name='甲芬那酸片/胶囊', help_text='甲芬那酸片/胶囊')

    xi_medicine_yantong = models.BooleanField(blank=True, null=True, verbose_name='炎痛喜康片', help_text='炎痛喜康片')
    xi_medicine_tongluo = models.BooleanField(blank=True, null=True, verbose_name='酮洛芬片/胶囊', help_text='酮洛芬片/胶囊')
    xi_medicine_quma = models.BooleanField(blank=True, null=True, verbose_name='曲马多缓释片', help_text='曲马多缓释片')
    xi_medicine_yansuan = models.BooleanField(blank=True, null=True, verbose_name='盐酸吗啡片/缓释片', help_text='盐酸吗啡片/缓释片')
    xi_medicine_shuang = models.BooleanField(blank=True, null=True, verbose_name='双氯芬酸钠缓释片/钾片', help_text='双氯芬酸钠缓释片/钾片')
    xi_medicine_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='西医具体药物-其他', help_text='西医具体药物-其他')

    xi_zs_bugui = models.BooleanField(blank=True, null=True, verbose_name='盐酸布桂嗪（强痛定）注射液', help_text='盐酸布桂嗪（强痛定）注射液')
    xi_zs_paiti = models.BooleanField(blank=True, null=True, verbose_name='盐酸哌替啶（杜冷丁）注射液', help_text='盐酸哌替啶（杜冷丁）注射液')
    xi_zs_mafei = models.BooleanField(blank=True, null=True, verbose_name='盐酸吗啡注射液', help_text='盐酸吗啡注射液')
    xi_zs_antong = models.BooleanField(blank=True, null=True, verbose_name='安痛定注射液', help_text='安痛定注射液')
    xi_zs_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='注射-其他', help_text='注射-其他')

    is_biyunyao = models.BooleanField(blank=True, null=True, verbose_name='口服避孕药', help_text='口服避孕药')

    biyun_daying = models.BooleanField(blank=True, null=True, verbose_name='达英-35', help_text='达英-35')
    biyun_ming = models.BooleanField(blank=True, null=True, verbose_name='优思明', help_text='优思明')
    biyun_yue = models.BooleanField(blank=True, null=True, verbose_name='优思悦', help_text='优思悦')
    biyun_ma = models.BooleanField(blank=True, null=True, verbose_name='妈富隆', help_text='妈富隆')
    biyun_nuo = models.BooleanField(blank=True, null=True, verbose_name='复方炔诺酮片', help_text='复方炔诺酮片')
    biyun_jia = models.BooleanField(blank=True, null=True, verbose_name='复方甲地孕酮片', help_text='复方甲地孕酮片')
    biyun_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='避孕具体药物-其他', help_text='避孕具体药物-其他')

    other_cure = models.CharField(max_length=200, blank=True, null=True, verbose_name='其他治疗', help_text='其他治疗')

    EAT_TIMES_CHOICE = (
        ('总是服用（每个月经周期）', '总是服用（每个月经周期）'),
        ('经常服用', '经常服用'),
        ('偶尔服用', '偶尔服用')
    )
    eat_times = models.CharField(max_length=12, choices=EAT_TIMES_CHOICE, blank=True, null=True, verbose_name='', help_text='')

    EAT_START_TIME_CHOICE = (
        ('经前', '经前'),
        ('经期', '经期'),
        ('经后', '经后'),
    )
    eat_start_time = models.CharField(max_length=4, choices=EAT_START_TIME_CHOICE, blank=True, null=True, verbose_name='始用药时间', help_text='始用药时间')

    eat_start_time_days = models.IntegerField(blank=True, null=True, verbose_name='始用药时间-天数', help_text='始用药时间-天数')

    eat_end_time = models.CharField(max_length=4, choices=EAT_START_TIME_CHOICE, blank=True, null=True, verbose_name='停药时间', help_text='停药时间')

    eat_end_time_days = models.IntegerField(blank=True, null=True, verbose_name='停药时间-天数', help_text='停药时间-天数')

    total_eat_days = models.IntegerField(blank=True, null=True, verbose_name='总用药天数', help_text='总用药天数')

    one_day_eat = models.IntegerField(blank=True, null=True, verbose_name='1日服用次数', help_text='1日服用次数')

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '治疗'
        verbose_name_plural = verbose_name
