from django.db import models

from .models import CreateTimeModel

from myuser.models import MyUser

from prj003.models import questionnaire


class Summary(CreateTimeModel):
    info = models.OneToOneField(questionnaire.QuestionNaire, related_name='summary', on_delete=models.CASCADE)

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj003_summary')

    zhusu = models.IntegerField(blank=True, null=True, verbose_name='主诉', help_text='主诉')

    PAIN_CHIOCE = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )
    pain_level = models.CharField(max_length=2, choices=PAIN_CHIOCE, blank=True, null=True, verbose_name='疼痛程度', help_text='疼痛程度')

    pain_character_leng = models.BooleanField(blank=True, null=True, verbose_name='冷痛', help_text='冷痛')

    pain_character_zhui = models.BooleanField(blank=True, null=True, verbose_name='坠痛', help_text='坠痛')

    pain_character_zhang = models.BooleanField(blank=True, null=True, verbose_name='胀痛', help_text='胀痛')

    pain_character_ci = models.BooleanField(blank=True, null=True, verbose_name='刺痛', help_text='刺痛')

    pain_character_jiao = models.BooleanField(blank=True, null=True, verbose_name='绞痛', help_text='绞痛')

    pain_character_che = models.BooleanField(blank=True, null=True, verbose_name='掣痛', help_text='掣痛')

    pain_character_kong = models.BooleanField(blank=True, null=True, verbose_name='空痛', help_text='空痛')

    pain_character_zhuo = models.BooleanField(blank=True, null=True, verbose_name='灼痛', help_text='灼痛')

    pain_character_yin = models.BooleanField(blank=True, null=True, verbose_name='隐痛', help_text='隐痛')

    pain_character_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='疼痛性质-其他', help_text='疼痛性质-其他')

    PAIN_TIME_CHOICE = (
        ('经前', '经前'),
        ('经期', '经期'),
        ('经后', '经后'),
    )
    pain_start_time = models.CharField(max_length=2, choices=PAIN_TIME_CHOICE, blank=True, null=True, verbose_name='疼痛开始时间', help_text='疼痛开始时间')

    pain_start_time_day = models.IntegerField(blank=True, null=True, verbose_name='疼痛开始时间-天数', help_text='疼痛开始时间-天数')

    pain_end_time = models.CharField(max_length=2, choices=PAIN_TIME_CHOICE, blank=True, null=True, verbose_name='疼痛结束时间', help_text='疼痛结束时间')

    pain_start_time_day = models.IntegerField(blank=True, null=True, verbose_name='疼痛结束时间-天数', help_text='疼痛结束时间-天数')

    PAIN_TOTAL_CHOICE = (
        ('<3小时', '<3小时'),
        ('3~7小时', '3~7小时'),
        ('8~24小时', '8~24小时'),
        ('>24小时', '>24小时')
    )
    pain_total_time = models.CharField(max_length=10, choices=PAIN_TOTAL_CHOICE, blank=True, null=True, verbose_name='疼痛总发作时间', help_text='疼痛总发作时间')

    hobby_xian = models.BooleanField(blank=True, null=True, verbose_name='喜按', help_text='喜按')

    hobby_juan = models.BooleanField(blank=True, null=True, verbose_name='拒按', help_text='拒按')

    hobby_xiwen = models.BooleanField(blank=True, null=True, verbose_name='喜温', help_text='喜温')

    hobby_buxiwen = models.BooleanField(blank=True, null=True, verbose_name='不喜温', help_text='不喜温')

    hobby_dewenjian = models.BooleanField(blank=True, null=True, verbose_name='得温痛减', help_text='得温痛减')

    hobby_dewenbu = models.BooleanField(blank=True, null=True, verbose_name='得温痛不减', help_text='得温痛不减')

    hobby_kuaijian = models.BooleanField(blank=True, null=True, verbose_name='块下痛减', help_text='块下痛减')

    hobby_kuaibu = models.BooleanField(blank=True, null=True, verbose_name='块下痛不减', help_text='块下痛不减')

    MENSTRUATION_CHOICE = (
        ('量少(每天少于1片卫生巾)', '量少(每天少于1片卫生巾)'),
        ('量中等(每天约2-4片卫生巾)', '量中等(每天约2-4片卫生巾)'),
        ('量多(每天约5-10片卫生巾)', '量多(每天约5-10片卫生巾)')
    )
    menstruation = models.CharField(max_length=20, choices=MENSTRUATION_CHOICE, blank=True, null=True, verbose_name='月经量', help_text='月经量')

    menstruation_qita = models.CharField(max_length=50, choices=MENSTRUATION_CHOICE, blank=True, null=True, verbose_name='月经量-其他', help_text='月经量-其他')

    BLOOD_COLOR_CHOICE = (
        ('淡红', '淡红'),
        ('鲜红', '鲜红'),
        ('暗红', '暗红'),
        ('紫红', '紫红'),
        ('紫黯', '紫黯'),
        ('紫黑', '紫黑'),
        ('褐色', '褐色'),
    )
    blood_color = models.CharField(max_length=10, choices=BLOOD_COLOR_CHOICE, blank=True, null=True, verbose_name='经血颜色', help_text='经血颜色')

    blood_color_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='经血颜色-其他', help_text='经血颜色-其他')

    BLOOD_QUALITY_FIRST_CHOICE = (
        ('正常', '正常'),
        ('粘稠', '粘稠'),
        ('清稀', '清稀')
    )
    blood_quality_first = models.CharField(max_length=10, choices=BLOOD_QUALITY_FIRST_CHOICE, blank=True, null=True, verbose_name='经血质地-1', help_text='经血质地-1')

    BLOOD_QUALITY_SECOND_CHOICE = (
        ('无血块', '无血块'),
        ('偶有血块', '偶有血块'),
        ('经常出现血块', '经常出现血块')
    )
    blood_quality_second = models.CharField(max_length=10, choices=BLOOD_QUALITY_SECOND_CHOICE, blank=True, null=True, verbose_name='经血质地-2', help_text='经血质地-2')

    BLOOD_QUALITY_THIRD_CHOICE = (
        ('小血块', '小血块'),
        ('大血块', '大血块'),
        ('夹有膜样物', '夹有膜样物')
    )
    blood_quality_third = models.CharField(max_length=10, choices=BLOOD_QUALITY_THIRD_CHOICE, blank=True, null=True, verbose_name='经血质地-3', help_text='经血质地-3')

    TOTAL_TIME_CHOICE = PAIN_TOTAL_CHOICE
    AVERAGE_LEVEL_CHOICE = (
        ('轻度', '轻度'),
        ('中度', '中度'),
        ('较显著', '较显著'),
        ('剧烈', '剧烈')
    )
    is_exin = models.BooleanField(blank=True, null=True, verbose_name='经期恶心', help_text='经期恶心')
    is_exin_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_exin_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_outu = models.BooleanField(blank=True, null=True, verbose_name='经期呕吐', help_text='经期呕吐')
    is_outu_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_outu_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_shiyu = models.BooleanField(blank=True, null=True, verbose_name='经期食欲不振', help_text='经期食欲不振')
    is_shiyu_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_shiyu_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_toutong = models.BooleanField(blank=True, null=True, verbose_name='经期头痛', help_text='经期头痛')
    is_toutong_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_toutong_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_beitong = models.BooleanField(blank=True, null=True, verbose_name='经期背（腰骶部）痛', help_text='经期背（腰骶部）痛')
    is_beitong_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_beitong_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_tuitong = models.BooleanField(blank=True, null=True, verbose_name='经期腿痛', help_text='经期腿痛')
    is_tuitong_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_tuitong_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_fali = models.BooleanField(blank=True, null=True, verbose_name='经期乏力', help_text='经期乏力')
    is_fali_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_fali_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_xuanyun = models.BooleanField(blank=True, null=True, verbose_name='经期眩晕', help_text='经期眩晕')
    is_xuanyun_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_xuanyun_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_fuxie = models.BooleanField(blank=True, null=True, verbose_name='经期腹泻', help_text='经期腹泻')
    is_fuxie_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_fuxie_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_mianse = models.BooleanField(blank=True, null=True, verbose_name='经期面色变化', help_text='经期面色变化')
    is_mianse_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_mianse_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_weitong = models.BooleanField(blank=True, null=True, verbose_name='经期胃痛', help_text='经期胃痛')
    is_weitong_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_weitong_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_mianhong = models.BooleanField(blank=True, null=True, verbose_name='经期面红', help_text='经期面红')
    is_mianhong_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_mianhong_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_shimian = models.BooleanField(blank=True, null=True, verbose_name='经期失眠', help_text='经期失眠')
    is_shimian_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_shimian_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_quanshen = models.BooleanField(blank=True, null=True, verbose_name='经期全身疼痛', help_text='经期全身疼痛')
    is_quanshen_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_quanshen_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_yiyu = models.BooleanField(blank=True, null=True, verbose_name='经期抑郁', help_text='经期抑郁')
    is_yiyu_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_yiyu_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_yiji = models.BooleanField(blank=True, null=True, verbose_name='经期易激惹（烦恼、急躁或愤怒)', help_text='经期易激惹（烦恼、急躁或愤怒)')
    is_yiji_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_yiji_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_shenjing = models.BooleanField(blank=True, null=True, verbose_name='经期神经质（易紧张、好激动、多愁善感、敏感多疑、容易沮丧）', help_text='经期神经质（易紧张、好激动、多愁善感、敏感多疑、容易沮丧）')
    is_shenjing_total_time = models.CharField(choices=TOTAL_TIME_CHOICE, max_length=10, blank=True, null=True, verbose_name='总发作时间', help_text='总发作时间')
    is_shenjing_average = models.CharField(choices=AVERAGE_LEVEL_CHOICE, max_length=10, blank=True, null=True, verbose_name='平均强度', help_text='平均强度')

    is_hanchu = models.BooleanField(blank=True, null=True, verbose_name='汗出', help_text='汗出')

    is_gangmen = models.BooleanField(blank=True, null=True, verbose_name='肛门坠胀', help_text='肛门坠胀')

    is_sizhi = models.BooleanField(blank=True, null=True, verbose_name='四肢厥冷', help_text='四肢厥冷')

    is_yunjue = models.BooleanField(blank=True, null=True, verbose_name='晕厥', help_text='晕厥')
    LAST_CHOICE = (
        ('偶尔', '偶尔'),
        ('经常', '经常'),
    )
    is_yunjue_level = models.CharField(max_length=2, choices=LAST_CHOICE, blank=True, null=True, verbose_name='晕厥频次', help_text='晕厥频次')

    texture_danhong = models.BooleanField(verbose_name='淡红', blank=True, default=False, help_text="淡红")

    texture_danbai = models.BooleanField(verbose_name='淡白', blank=True, default=False, help_text="淡白")

    texture_hong = models.BooleanField(verbose_name='红', blank=True, default=False, help_text="红")

    texture_zian = models.BooleanField(verbose_name='紫暗有瘀点或瘀斑', blank=True, default=False, help_text="紫暗有瘀点或瘀斑")

    texture_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='舌质-其他', help_text='舌质-其他')

    tongue_zhengchang = models.BooleanField(verbose_name='正常', blank=True, null=True, help_text="正常")

    tongue_shouxiao = models.BooleanField(verbose_name='瘦小', blank=True, null=True, help_text="瘦小")

    tongue_pangda = models.BooleanField(verbose_name='胖大', blank=True, null=True, help_text="胖大")

    tongue_chihen = models.BooleanField(verbose_name='有齿痕', blank=True, null=True, help_text="有齿痕")

    tongue_liewen = models.BooleanField(verbose_name='有裂纹', blank=True, null=True, help_text="有裂纹")

    tongue_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='舌体-其他', help_text='舌体-其他')

    coating_bai = models.BooleanField(verbose_name='白', blank=True, null=True, help_text="白")

    coating_huang = models.BooleanField(verbose_name='黄', blank=True, null=True, help_text="黄")

    coating_bo = models.BooleanField(verbose_name='薄', blank=True, null=True, help_text="薄")

    coating_hou = models.BooleanField(verbose_name='厚', blank=True, null=True, help_text="厚")

    coating_ni = models.BooleanField(verbose_name='腻', blank=True, null=True, help_text="腻")

    coating_run = models.BooleanField(verbose_name='润', blank=True, null=True, help_text="润")

    coating_ganzao = models.BooleanField(verbose_name='干燥', blank=True, null=True, help_text="干燥")

    coating_shaotai = models.BooleanField(verbose_name='少苔', blank=True, null=True, help_text="少苔")

    coating_huabo = models.BooleanField(verbose_name='花剥', blank=True, null=True, help_text="花剥")

    coating_wutai = models.BooleanField(verbose_name='无苔', blank=True, null=True, help_text="无苔")

    coating_qita = models.CharField(blank=True, null=True, max_length=50, verbose_name='舌苔-其他', help_text='舌苔-其他')

    pulse_fu = models.BooleanField(verbose_name='浮', blank=True, null=True, help_text="浮")

    pulse_chen = models.BooleanField(verbose_name='沉', blank=True, null=True, help_text="沉")

    pulse_hua = models.BooleanField(verbose_name='滑', blank=True, null=True, help_text="滑")

    pulse_shu = models.BooleanField(verbose_name='数', blank=True, null=True, help_text="数")

    pulse_xian = models.BooleanField(verbose_name='弦', blank=True, null=True, help_text="炫")

    pulse_xi = models.BooleanField(verbose_name='细', blank=True, null=True, help_text="细")

    pulse_ruo = models.BooleanField(verbose_name='弱', blank=True, null=True, help_text="弱")

    pulse_huan = models.BooleanField(verbose_name='缓', blank=True, null=True, help_text="缓")

    pulse_chi = models.BooleanField(verbose_name='迟', blank=True, null=True, help_text="迟")

    pulse_se = models.BooleanField(verbose_name='涩', blank=True, null=True, help_text="涩")

    pulse_jin = models.BooleanField(verbose_name='紧', blank=True, null=True, help_text="紧")

    pulse_qita = models.CharField(verbose_name='脉象-其他', help_text='脉象-其他', blank=True, null=True, max_length=50)

    def __str__(self):
        return '{}'.format(self.pk)

    class Meta:
        verbose_name = '就诊时病情概要'
        verbose_name_plural = verbose_name
