from rest_framework import permissions


class DiffRolePermisssion(permissions.BasePermission):
    """
    :不同角色不同操作权限
    1. 用户必须存在prj003开头的权限才能进行访问数据
    2. 用户如果具备prj003_all_permissions权限, 则可以增删改查;
       prj003_other_permissions则只可以查看
    """

    def has_permission(self, request, view):
        user = request.user

        all_perm = request.user.get_all_permissions()
        own_perm = [perm for perm in all_perm if perm.startswith('prj003')]

        if not own_perm:
            return False

        # 审核信息需要满足:
        # 1. 必须是最高权限
        # 2. 包括用户admin
        if view.action == 'check':
            if all([
                user.has_perm('prj003.prj003_other_permissions'),
                not user.has_perm('prj003.prj003_all_permissions'),
                not user.is_admin,
            ]):
                return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.has_perm('prj003.prj003_all_permissions'):
            return True
        elif request.user.has_perm('prj003.prj003_other_permissions'):
            if request.method in permissions.SAFE_METHODS:
                return True
            else:
                if hasattr(obj, 'info'):
                    obj = obj.info

                if hasattr(obj, 'check_status'):
                    if obj.check_status == '审核通过':
                        return False

                return obj.owner == request.user
        else:
            return False
