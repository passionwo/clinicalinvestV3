from django.contrib import admin

from prj003.models import (
    questionnaire, summary, history,
    relevant, clinical, cure, results
)

from prj003.utils import samecode


@admin.register(questionnaire.QuestionNaire)
class QuestionNaireAdmin(admin.ModelAdmin):
    list_display = ('serial', 'patient_name')

    def save_model(self, request, obj, form, change):
        obj.save()


@admin.register(summary.Summary)
class SummaryAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link', )

    def save_model(self, request, obj, form, change):
        obj.save()


@admin.register(history.History)
class HistoryAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link', )

    def save_model(self, request, obj, form, change):
        obj.save()


@admin.register(relevant.Relevant)
class RelevantAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link',)


@admin.register(clinical.Clinical)
class ClinicalAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link')


@admin.register(cure.Cure)
class CureAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link')


@admin.register(results.Results)
class ResultsAdmin(admin.ModelAdmin, samecode.SameCodeAdmin):
    list_display = ('id', 'owner_link', 'questionnaire_link')
