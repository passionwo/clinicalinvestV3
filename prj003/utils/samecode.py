from django.shortcuts import reverse
from django.utils.html import format_html

from rest_framework import viewsets

from oauth2_provider.contrib.rest_framework import TokenHasScope

from prj003.permissions import DiffRolePermisssion


class SameCodeAdmin(object):
    """
    Create admin foreign key link
    : module:appname_modelname_change
    """

    def owner_link(self, obj):
        url = reverse('admin:myuser_myuser_change', args=[obj.owner.id])
        return format_html("<a href='{}'>{}</a>", url, obj.owner)

    def questionnaire_link(self, obj):
        url = reverse('admin:prj003_questionnaire_change', args=[obj.info.id])
        return format_html("<a href='{}'>{}</a>", url, obj.info.serial)

    owner_link.short_description = '账户'
    questionnaire_link.short_description = '调查问卷编码'


class SameCodeView(viewsets.ModelViewSet):
    """
    A viewset that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()` and `list()` actions.

    Override .perform_create to custom the save instance
    """
    permission_classes = [TokenHasScope, DiffRolePermisssion]
    required_scopes = ['prj003']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
