from rest_framework.decorators import action
from rest_framework.response import Response

from prj003.utils import samecode
from prj003.models import (
    questionnaire, summary, history,
    relevant, clinical, cure, results
)
from prj003.serializers import (
    QuestionNaireLinkSerializer, SummarySerializer,
    CureSerializer, ResultsSerializer, HistorySerializer,
    RelevantSerializer, ClinicalSerializer,
    CheckStatusSerializer,
)


class QuertionNaireView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = QuestionNaireLinkSerializer
    queryset = questionnaire.QuestionNaire.objects.order_by('-create_time', '-pk')

    def get_serializer_class(self):
        if self.action == 'check':
            return CheckStatusSerializer

        return self.serializer_class

    @action(detail=True, methods=['post'])
    def check(self, request, pk, *args, **kwargs):
        info = questionnaire.QuestionNaire.objects.get(pk=pk)
        data = request.data

        for key, value in data.items():
            if hasattr(info, key):
                setattr(info, key, value)

        info.save()
        # self.get_serializer => check status serializer class
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        # self.serializer_class => info serializer class
        info_serializer = self.serializer_class(info, context={'request': request})
        return Response(info_serializer.data)


class SummaryView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = SummarySerializer
    queryset = summary.Summary.objects.order_by('-create_time', '-pk')


class ResultsView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = ResultsSerializer
    queryset = results.Results.objects.order_by('-create_time', '-pk')


class CureView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = CureSerializer
    queryset = cure.Cure.objects.order_by('-create_time', '-pk')


class RelevantView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = RelevantSerializer
    queryset = relevant.Relevant.objects.order_by('-create_time', '-pk')


class ClinicalView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = ClinicalSerializer
    queryset = clinical.Clinical.objects.order_by('-create_time', '-pk')


class HistoryView(samecode.SameCodeView):
    """
    get([list, retrieve]):
    1. Return all questionnaire instance
    2. Return the questionnaire instance by id

    post(create):
    Save a questionnaire instance and Return it.

    patch(partial_update):
    Update the questionnaire instance and Return it.

    delete(destroy):
    Delete the questionnaire instance by id.
    """
    serializer_class = HistorySerializer
    queryset = history.History.objects.order_by('-create_time', '-pk')
