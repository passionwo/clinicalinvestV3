from rest_framework import serializers

from prj003.models import (
    questionnaire, summary, history,
    relevant, clinical, cure, results
)
from utils.samecode import ExtendsSerializer, SCSerializer


class CheckStatusSerializer(serializers.Serializer):
    CHECKED_STATUS = ('未审核', '审核通过', '审核不通过')
    CHECKED_CHOICE = (('未审核', '未审核'), ('审核通过', '审核通过'), ('审核不通过', '审核不通过'))

    check_status = serializers.ChoiceField(required=True, choices=CHECKED_CHOICE, help_text='审核状态')
    reason_for_check = serializers.CharField(required=False, help_text='审核不通过原因')

    def validate_check_status(self, value):
        if value not in self.CHECKED_STATUS:
            raise serializers.ValidationError('非法审核状态')

        return value


class QuestionNaireLinkSerializer(ExtendsSerializer, serializers.HyperlinkedModelSerializer):
    summary = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-summary-detail')
    history = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-history-detail')
    relevant = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-relevant-detail')
    clinical = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-clinical-detail')
    cure = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-cure-detail')
    results = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj003-results-detail')
    info = serializers.HyperlinkedIdentityField(read_only=True, view_name='prj003-question-detail', lookup_field='pk')

    class Meta:
        model = questionnaire.QuestionNaire
        fields = '__all__'
        read_only_fields = ['owner', 'serial', 'degree_of_completion', 'check_status', 'reason_for_check']
        extra_kwargs = {
            'url': {'view_name': 'prj003-question-detail', 'lookup_field': 'pk'},
        }
        project_name = 'prj003'


class SummarySerializer(SCSerializer):

    class Meta:
        model = summary.Summary
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-summary-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }


class HistorySerializer(SCSerializer):

    class Meta:
        model = history.History
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-history-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }


class RelevantSerializer(SCSerializer):

    class Meta:
        model = relevant.Relevant
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-relevant-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }


class ClinicalSerializer(SCSerializer):

    class Meta:
        model = clinical.Clinical
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-clinical-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }


class CureSerializer(SCSerializer):

    class Meta:
        model = cure.Cure
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-cure-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }


class ResultsSerializer(SCSerializer):

    class Meta:
        model = results.Results
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj003-results-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj003-question-detail', }
        }
