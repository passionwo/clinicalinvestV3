from django.urls import path

from prj003.views import (
    QuertionNaireView, SummaryView,
    RelevantView, ResultsView, CureView,
    HistoryView, ClinicalView
)
many_view_func = {
    'get': 'list',
    'post': 'create'
}

single_view_func = {
    'get': 'retrieve',
    'patch': 'partial_update',
    'delete': 'destroy'
}

urlpatterns = [

    path('info/check/<int:pk>/', QuertionNaireView.as_view({'post': 'check'}), name='prj002-info-check'),

    path('info/', QuertionNaireView.as_view(many_view_func), name='prj003-question'),
    path('info/<int:pk>/', QuertionNaireView.as_view(single_view_func), name='prj003-question-detail'),

    path('summary/', SummaryView.as_view(many_view_func), name='prj003-summary'),
    path('summary/<int:pk>/', SummaryView.as_view(single_view_func), name='prj003-summary-detail'),

    path('relevant/', RelevantView.as_view(many_view_func), name='prj003-relevant'),
    path('relevant/<int:pk>/', RelevantView.as_view(single_view_func), name='prj003-relevant-detail'),

    path('results/', ResultsView.as_view(many_view_func), name='prj003-results'),
    path('results/<int:pk>/', ResultsView.as_view(single_view_func), name='prj003-results-detail'),

    path('cure/', CureView.as_view(many_view_func), name='prj003-cure'),
    path('cure/<int:pk>/', CureView.as_view(single_view_func), name='prj003-cure-detail'),

    path('history/', HistoryView.as_view(many_view_func), name='prj003-history'),
    path('history/<int:pk>/', HistoryView.as_view(single_view_func), name='prj003-history-detail'),

    path('clinical/', ClinicalView.as_view(many_view_func), name='prj003-clinical'),
    path('clinical/<int:pk>/', ClinicalView.as_view(single_view_func), name='prj003-clinical-detail'),

]
