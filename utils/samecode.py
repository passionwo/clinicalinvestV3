from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from django.conf import settings

from utils.generate import (
    create_degree_of_completion, create_bmi_and_whr,
    create_instance,
)

from utils.constants import (
    prj002_necessary_info, prj003_necessary_info, permission_deny_message
)


class SCSerializer(serializers.HyperlinkedModelSerializer):
    create_time = serializers.DateTimeField(format=settings.DATETIME_FORMAT, read_only=True)

    def validate_info(self, value):
        owner = value.owner
        user = self.context['view'].request.user

        if owner != user:
            raise PermissionDenied(detail=permission_deny_message.get('exist_info'))
        return value


class ExtendsSerializer(object):

    def need_info(self):
        try:
            prj_name = self.Meta.project_name
        except Exception:
            raise ValueError('序列号器Meta中缺少project_name参数')

        if prj_name == 'prj002':
            necessary_info = prj002_necessary_info
        elif prj_name == 'prj003':
            necessary_info = prj003_necessary_info

        return necessary_info, prj_name

    def create(self, validated_data):
        validated_data = create_instance(validated_data, self.Meta.model)

        instance = self.Meta.model.objects.create(**validated_data)

        necessary_info, prj_name = self.need_info()

        if prj_name == 'prj002':
            instance = create_bmi_and_whr(instance)

        instance = create_degree_of_completion(instance, necessary_info)
        instance.save()
        return instance

    def update(self, instance, validated_data):

        for key, value in validated_data.items():
            if hasattr(instance, key):
                setattr(instance, key, value)
        instance.save()

        necessary_info, prj_name = self.need_info()

        if prj_name == 'prj002':
            instance = create_bmi_and_whr(instance)

        instance = create_degree_of_completion(instance, necessary_info)
        instance.save()
        return instance
