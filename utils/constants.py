prj002_necessary_info = [
    'name', 'phone', 'hospital', 'birth',
    'nation', 'career', 'address', 'culture',
    'height', 'weight', 'waistline', 'hipline'
]

prj003_necessary_info = [
    'patient_name', 'patient_date', 'hospital_name',
    'hospital_belong', 'patient_phone', 'expert_name',
    'expert_hospital', 'expert_phone', 'expert_email',
    'expert_title', 'patient_birth', 'patient_height',
    'patient_weight', 'patient_address', 'nation',
    'career', 'entrance'
]

permission_deny_message = {
    'exist_info': '对不起, 您没有对该记录操作的权限.'
}
