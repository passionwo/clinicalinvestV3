from datetime import datetime


# 判断序列号是否符合
# 1. 12位纯数字
# 2. 前八位是年月日
def is_correct(para):
    if len(para) == 12 and para.isdigit and isinstance(para, str):
        the_date = para[0:8]
        try:
            str_date = datetime.strptime(the_date, '%Y%m%d')
        except Exception:
            raise ValueError('参数中时间对应格式不正确')
        else:
            return str_date.date()

    return False
