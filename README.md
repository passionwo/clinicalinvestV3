clinicalinvestV3-流调version3
---

#### <span style='color: red;'>不支持 xls 文件, 文件可以另存为 xlsx</span>

1. 账号导入数据库: <span style='font-size: 18px;color: blue;'>importdata</span>
    - 初始密码的设置 => settings.py: BEGIN_PASS 

    ```shell
    # 在python虚拟环境下,查看命令帮助
    $ ./manage.py importdata -h
    ```
    
    | 参数简称 | 参数全称 | 参数必要性 | 参数说明 | 默认值 |
    |  :---:  | :---:  |  :---:    |  :---: | :---: |
    |-f|--file_path|**必要**|文件的路径|无|
    |-g|--group_name|**必要**|分组的name名称|无|
    |-s|--start_row|可选|开始导入数据的起始行号|3|
    |-e|--end_row|可选|结束导入数据的终止行号|415|
    |-p|--prj_code|可选|为用户直接添加的项目linkurl|project001|
    
    ```shell
    # 我测试的例子1 => 数据 3-415 有效数据全部导入
    (clinicalV3) wo:clinicalinvestV3 wo$ ./manage.py importdata -f=/Users/wo/Desktop/users.xlsx -g=prj_self
    # 2 => 导入 3-6 行的有效数据
    (clinicalV3) wo:clinicalinvestV3 wo$ ./manage.py importdata -f=/Users/wo/Desktop/users.xlsx -g=prj_self -e=6 -p=project001
    # 3 => 参数形式不同 2
    (clinicalV3) wo:clinicalinvestV3 wo$ ./manage.py importdata --file_path=/Users/wo/Desktop/users.xlsx --group_name=prj_self --end_row=6 --prj_code=project001
    ```
2. 发送邮件: <span style='font-size: 18px;color: blue;'>sendemail</span>
    ```shell
    # 在python虚拟环境下,查看命令帮助
    $ ./manage.py sendemail -h
    ```

    | 参数简称 | 参数全称 | 参数必要性 | 参数说明 | 默认值 |
    |  :---:  | :---:  |  :---:    |  :---: | :---: |
    |-f|--file_path|**必要**|文件的路径|无|
    |-s|--start_row|可选|开始导入数据的起始行号|3|
    |-e|--end_row|可选|结束导入数据的终止行号|415|
    
    ```shell
    # 我测试的例子1 => 参数名和值之间也可以为空格
    (clinicalV3) wo:clinicalinvestV3 wo$ ./manage.py sendemail -f /Users/wo/Desktop/users.xlsx 
    # 2 => 对 3-5 行分析邮箱, 发送邮件
    (clinicalV3) wo:clinicalinvestV3 wo$ ./manage.py sendemail -f /Users/wo/Desktop/users.xlsx -e=5
    ```
