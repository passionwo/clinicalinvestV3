from django.db import models
from .geninfo import GeneralInfo


class Cure(models.Model):
    person = models.OneToOneField(GeneralInfo,
                                  related_name='cure_nine',
                                  on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              null=True,
                              blank=True,
                              related_name='my_cure_nine',
                              on_delete=models.CASCADE)

    YES_NO = (
        ('是', '是'),
        ('否', '否')
    )
    to_cure = models.CharField(verbose_name='中西医结合治疗',
                               help_text='中西医结合治疗',
                               null=True,
                               blank=True,
                               choices=YES_NO,
                               max_length=1,
                               default=None, )

    # 中医-治法-虚症
    XUZHENG_ONE_CHOICES = (
        ('补脾益气,养血固冲调经', '补脾益气,养血固冲调经'),
        ('补益肾气,养血固冲调经', '补益肾气,养血固冲调经'),
        ('补血养营,益气调经', '补血养营,益气调经'),
        ('补气升提,固冲止血调经', '补气升提,固冲止血调经'),
        ('养阴清热,凉血调经', '养阴清热,凉血调经'),
        ('滋肾益阴,清热固冲止血', '滋肾益阴,清热固冲止血'),
        ('温肾助阳,固冲止血', '温肾助阳,固冲止血')
    )
    xuzheng_one = models.CharField(verbose_name='虚症治法',
                                   help_text='虚症治法',
                                   default=None,
                                   null=True,
                                   choices=XUZHENG_ONE_CHOICES,
                                   max_length=12,
                                   blank=True)
    xuzheng_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    blank=True,
                                    null=True,
                                    max_length=100, )

    # 中医-治法-实证
    SHIZHENG_ONE_CHOICES = (
        ('温经散寒,活血调经', '温经散寒,活血调经'),
        ('清热降火,凉血调经', '清热降火,凉血调经'),
        ('燥湿化痰,活血调经', '燥湿化痰,活血调经'),
        ('清热凉血,固冲止血', '清热凉血,固冲止血'),
        ('疏肝理气,活血调经', '疏肝理气,活血调经'),
        ('活血化瘀,固冲止血调经', '活血化瘀,固冲止血调经'),
        ('清热除湿,凉血止血', '清热除湿,凉血止血'),
        ('清肝解郁,凉血调经', '清肝解郁,凉血调经'),
    )
    shizheng_one = models.CharField(verbose_name='实证治法',
                                    help_text='实证治法',
                                    default=None,
                                    null=True,
                                    choices=SHIZHENG_ONE_CHOICES,
                                    max_length=12,
                                    blank=True)
    shizheng_qita = models.CharField(verbose_name='其他',
                                     help_text='其他',
                                     default=None,
                                     blank=True,
                                     null=True,
                                     max_length=100, )
    # 中医-治法-虚实夹杂证
    XUSHI_ONE_CHOICES = (
        ('养阴清热,凉血调经', '养阴清热,凉血调经'),
        ('清热降火,凉血调经', '清热降火,凉血调经'),
    )
    xushi_one = models.CharField(verbose_name='虚实证夹杂治法',
                                 help_text='虚实证夹杂治法',
                                 default=None,
                                 null=True,
                                 choices=XUSHI_ONE_CHOICES,
                                 max_length=12,
                                 blank=True)
    xushi_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  blank=True,
                                  null=True,
                                  max_length=100, )
    # 中医-代表方-虚症
    pre_xu_yiqi = models.BooleanField(verbose_name='补中益气汤',
                                      help_text='补中益气汤',
                                      default=False,
                                      blank=True)
    pre_xu_guyin = models.BooleanField(verbose_name='固阴煎',
                                       help_text='固阴煎',
                                       default=False,
                                       blank=True)
    pre_xu_yuanjian = models.BooleanField(verbose_name='大补元煎',
                                          help_text='大补元煎',
                                          default=False,
                                          blank=True)
    # pre_xu_dihuang = models.BooleanField(verbose_name='当归地黄饮',
    #                                      help_text='当归地黄饮',
    #                                      default=False,
    #                                      blank=True)

    pre_xu_yangrong = models.BooleanField(verbose_name='人参养荣汤',
                                          help_text='人参养荣汤',
                                          default=False,
                                          blank=True)
    # pre_xu_daying = models.BooleanField(verbose_name='大营煎',
    #                                     help_text='大营煎',
    #                                     default=False,
    #                                     blank=True)
    # pre_xu_wenjing = models.BooleanField(verbose_name='温经汤《金匮要略》',
    #                                      help_text='温经汤《金匮要略》',
    #                                      default=False,
    #                                      blank=True)

    pre_xu_guipi = models.BooleanField(verbose_name='归脾汤',
                                       help_text='归脾汤',
                                       default=False,
                                       blank=True)
    pre_xu_anchong = models.BooleanField(verbose_name='安冲汤',
                                         help_text='安冲汤',
                                         default=False,
                                         blank=True)
    # pre_xu_guishen = models.BooleanField(verbose_name='归肾丸',
    #                                      help_text='归肾丸',
    #                                      default=False,
    #                                      blank=True)
    # pre_xu_zixue = models.BooleanField(verbose_name='滋血汤',
    #                                    help_text='滋血汤',
    #                                    default=False,
    #                                    blank=True)
    pre_xu_jianyuan = models.BooleanField(verbose_name='举元煎',
                                          help_text='举元煎',
                                          default=False,
                                          blank=True)
    pre_xu_qingxue = models.BooleanField(verbose_name='清血养阴汤',
                                         help_text='清血养阴汤',
                                         default=False,
                                         blank=True)
    pre_xu_liangdi = models.BooleanField(verbose_name='两地汤合二至丸',
                                         help_text='两地汤合二至丸',
                                         default=False,
                                         blank=True)
    pre_xu_jiajian = models.BooleanField(verbose_name='加减一阴煎',
                                         help_text='加减一阴煎',
                                         default=False,
                                         blank=True)
    pre_xu_liuwei = models.BooleanField(verbose_name='二至丸合六味地黄丸',
                                        help_text='二至丸合六味地黄丸',
                                        default=False,
                                        blank=True)
    pre_xu_zuogui = models.BooleanField(verbose_name='左归丸合二至丸',
                                        help_text='左归丸合二至丸',
                                        default=False,
                                        blank=True)
    pre_xu_yougui = models.BooleanField(verbose_name='右归丸',
                                        help_text='右归丸',
                                        default=False,
                                        blank=True)
    pre_xu_guchong = models.BooleanField(verbose_name='固冲汤',
                                         help_text='固冲汤',
                                         default=False,
                                         blank=True)
    pre_xu_guben = models.BooleanField(verbose_name='固本止崩汤',
                                       help_text='固本止崩汤',
                                       default=False,
                                       blank=True)
    pre_xu_baoyin = models.BooleanField(verbose_name='保阴煎',
                                        help_text='保阴煎',
                                        default=False,
                                        blank=True)
    pre_xu_qita = models.CharField(verbose_name='其他',
                                   help_text='其他',
                                   default=None,
                                   blank=True,
                                   null=True,
                                   max_length=100, )

    # 实证
    pre_shi_xiaoyao = models.BooleanField(verbose_name='逍遥散',
                                          help_text='逍遥散',
                                          default=False,
                                          blank=True)
    pre_shi_qingjing = models.BooleanField(verbose_name='清经散',
                                           help_text='清经散',
                                           default=False,
                                           blank=True)
    pre_shi_wenjing = models.BooleanField(verbose_name='温经汤(《妇人大全良方》)',
                                          help_text='温经汤(《妇人大全良方》)',
                                          default=False,
                                          blank=True)

    # pre_shi_wuyao = models.BooleanField(verbose_name='乌药汤',
    #                                     help_text='乌药汤',
    #                                     default=False,
    #                                     blank=True)
    # pre_shi_erchen = models.BooleanField(verbose_name='芎归二陈汤',
    #                                      help_text='芎归二陈汤',
    #                                      default=False,
    #                                      blank=True)
    # pre_shi_baoyin = models.BooleanField(verbose_name='保阴煎',
    #                                      help_text='保阴煎',
    #                                      default=False,
    #                                      blank=True)
    pre_shi_taohong = models.BooleanField(verbose_name='桃红四物汤',
                                          help_text='桃红四物汤',
                                          default=False,
                                          blank=True)
    pre_shi_siwu = models.BooleanField(verbose_name='四物汤合失笑散',
                                       help_text='四物汤合失笑散',
                                       default=False,
                                       blank=True)
    # pre_shi_tongyu = models.BooleanField(verbose_name='通瘀煎',
    #                                      help_text='通瘀煎',
    #                                      default=False,
    #                                      blank=True)
    pre_shi_cangfu = models.BooleanField(verbose_name='苍附导痰丸',
                                         help_text='苍附导痰丸',
                                         default=False,
                                         blank=True)
    # pre_shi_yangxue = models.BooleanField(verbose_name='清血养阴汤',
    #                                       help_text='清血养阴汤',
    #                                       default=False,
    #                                       blank=True)
    pre_shi_liangdi = models.BooleanField(verbose_name='两地汤合二至丸',
                                          help_text='两地汤合二至丸',
                                          default=False,
                                          blank=True)
    pre_shi_zongpu = models.BooleanField(verbose_name='棕蒲散',
                                         help_text='棕蒲散',
                                         default=False,
                                         blank=True)
    pre_shi_qinggan = models.BooleanField(verbose_name='清肝止淋汤',
                                          help_text='清肝止淋汤',
                                          default=False,
                                          blank=True)
    pre_shi_zuyu = models.BooleanField(verbose_name='逐瘀止血汤',
                                       help_text='逐瘀止血汤',
                                       default=False,
                                       blank=True)
    pre_shi_qingre = models.BooleanField(verbose_name='清热固经汤',
                                         help_text='清热固经汤',
                                         default=False,
                                         blank=True)
    pre_shi_zhibeng = models.BooleanField(verbose_name='逐瘀止崩汤',
                                          help_text='逐瘀止崩汤',
                                          default=False,
                                          blank=True)
    pre_shi_danzhi = models.BooleanField(verbose_name='丹栀逍遥散',
                                         help_text='丹栀逍遥散',
                                         default=False,
                                         blank=True)
    pre_shi_longdan = models.BooleanField(verbose_name='龙胆泻肝汤',
                                          help_text='龙胆泻肝汤',
                                          default=False,
                                          blank=True)
    pre_shi_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    blank=True,
                                    null=True,
                                    max_length=100, )
    # 虚实夹杂证
    pre_xushi_liangdi = models.BooleanField(verbose_name='两地汤',
                                            help_text='两地汤',
                                            default=False,
                                            blank=True)
    pre_xushi_qingjing = models.BooleanField(verbose_name='清经散',
                                             help_text='清经散',
                                             default=False,
                                             blank=True)
    pre_xushi_qita = models.CharField(verbose_name='其他',
                                      help_text='其他',
                                      default=None,
                                      blank=True,
                                      null=True,
                                      max_length=100, )
    # 中成药-虚症
    zhong_xu_buzhong = models.BooleanField(verbose_name='补中益气颗粒',
                                           help_text='补中益气颗粒',
                                           default=False,
                                           blank=True)
    zhong_xu_renshen = models.BooleanField(verbose_name='人参归脾丸',
                                           help_text='人参归脾丸',
                                           default=False,
                                           blank=True)
    zhong_xu_zhixue = models.BooleanField(verbose_name='葆宫止血颗粒',
                                          help_text='葆宫止血颗粒',
                                          default=False,
                                          blank=True)
    zhong_xu_ankun = models.BooleanField(verbose_name='安坤颗粒',
                                         help_text='安坤颗粒',
                                         default=False,
                                         blank=True)

    zhong_xu_fufang = models.BooleanField(verbose_name='复方阿胶浆',
                                          help_text='复方阿胶浆',
                                          default=False,
                                          blank=True)
    zhong_xu_gujing = models.BooleanField(verbose_name='固经丸',
                                          help_text='固经丸',
                                          default=False,
                                          blank=True)
    # zhong_xu_dabu = models.BooleanField(verbose_name='大补阴丸',
    #                                     help_text='大补阴丸',
    #                                     default=False,
    #                                     blank=True)
    # zhong_xu_kuntai = models.BooleanField(verbose_name='坤泰胶囊',
    #                                       help_text='坤泰胶囊',
    #                                       default=False,
    #                                       blank=True)
    # zhong_xu_wuji = models.BooleanField(verbose_name='乌鸡白凤丸',
    #                                     help_text='乌鸡白凤丸',
    #                                     default=False,
    #                                     blank=True)
    # zhong_xu_dingkun = models.BooleanField(verbose_name='定坤丹',
    #                                        help_text='定坤丹',
    #                                        default=False,
    #                                        blank=True)
    # zhong_xu_zanyu = models.BooleanField(verbose_name='安坤赞育丸',
    #                                      help_text='安坤赞育丸',
    #                                      default=False,
    #                                      blank=True)
    # zhong_xu_aifu = models.BooleanField(verbose_name='艾附暖宫丸',
    #                                     help_text='艾附暖宫丸',
    #                                     default=False,
    #                                     blank=True)

    zhong_xu_ejiao = models.BooleanField(verbose_name='阿胶当归合剂',
                                         help_text='阿胶当归合剂',
                                         default=False,
                                         blank=True)
    zhong_xu_liuwei = models.BooleanField(verbose_name='六味地黄丸',
                                          help_text='六味地黄丸',
                                          default=False,
                                          blank=True)
    zhong_xu_erzhi = models.BooleanField(verbose_name='二至丸',
                                         help_text='二至丸',
                                         default=False,
                                         blank=True)
    zhong_xu_fuke = models.BooleanField(verbose_name='妇科止血灵片',
                                        help_text='妇科止血灵片',
                                        default=False,
                                        blank=True)

    zhong_xu_wuzi = models.BooleanField(verbose_name='五子衍宗丸',
                                        help_text='五子衍宗丸',
                                        default=False,
                                        blank=True)
    zhong_xu_qita = models.CharField(verbose_name='其他',
                                     help_text='其他',
                                     default=None,
                                     blank=True,
                                     null=True,
                                     max_length=100, )
    # 实证
    zhong_shi_xuening = models.BooleanField(verbose_name='宫血宁胶囊',
                                            help_text='宫血宁胶囊',
                                            default=False,
                                            blank=True)
    zhong_shi_duanxue = models.BooleanField(verbose_name='断血流片',
                                            help_text='断血流片',
                                            default=False,
                                            blank=True)
    zhong_shi_jiawei = models.BooleanField(verbose_name='加味逍遥口服液/丸',
                                           help_text='加味逍遥口服液/丸',
                                           default=False,
                                           blank=True)
    zhong_shi_qianzhi = models.BooleanField(verbose_name='茜芷胶囊',
                                            help_text='茜芷胶囊',
                                            default=False,
                                            blank=True)
    # zhong_shi_angong = models.BooleanField(verbose_name='安宫止血颗粒',
    #                                        help_text='安宫止血颗粒',
    #                                        default=False,
    #                                        blank=True)
    zhong_shi_yunnan = models.BooleanField(verbose_name='云南白药',
                                           help_text='云南白药',
                                           default=False,
                                           blank=True)
    # zhong_shi_fuan = models.BooleanField(verbose_name='妇血安片',
    #                                      help_text='妇血安片',
    #                                      default=False,
    #                                      blank=True)
    # zhong_shi_fuxue = models.BooleanField(verbose_name='妇血康胶囊',
    #                                       help_text='妇血康胶囊',
    #                                       default=False,
    #                                       blank=True)
    zhong_shi_gongning = models.BooleanField(verbose_name='宫宁颗粒',
                                             help_text='宫宁颗粒',
                                             default=False,
                                             blank=True)
    zhong_shi_zhixue = models.BooleanField(verbose_name='葆宫止血颗粒',
                                           help_text='葆宫止血颗粒',
                                           default=False,
                                           blank=True)
    # zhong_shi_ankun = models.BooleanField(verbose_name='安坤颗粒',
    #                                       help_text='安坤颗粒',
    #                                       default=False,
    #                                       blank=True)
    # zhong_shi_shaofu = models.BooleanField(verbose_name='少腹逐瘀胶囊',
    #                                        help_text='少腹逐瘀胶囊',
    #                                        default=False,
    #                                        blank=True)
    # zhong_shi_xuefu = models.BooleanField(verbose_name='血府逐瘀胶囊',
    #                                       help_text='血府逐瘀胶囊',
    #                                       default=False,
    #                                       blank=True)
    # zhong_shi_qizhi = models.BooleanField(verbose_name='七制香附丸',
    #                                       help_text='七制香附丸',
    #                                       default=False,
    #                                       blank=True)
    zhong_shi_xiaoyao = models.BooleanField(verbose_name='逍遥丸',
                                            help_text='逍遥丸',
                                            default=False,
                                            blank=True)
    zhong_shi_kunning = models.BooleanField(verbose_name='坤宁口服液',
                                            help_text='坤宁口服液',
                                            default=False,
                                            blank=True)
    zhong_shi_qita = models.CharField(verbose_name='其他',
                                      help_text='其他',
                                      default=None,
                                      blank=True,
                                      null=True,
                                      max_length=100, )
    # 虚实夹杂
    zhong_xushi_zhixue = models.BooleanField(verbose_name='葆宫止血颗粒',
                                             help_text='葆宫止血颗粒',
                                             default=False,
                                             blank=True)
    zhong_xushi_gujing = models.BooleanField(verbose_name='固经丸',
                                             help_text='固经丸',
                                             default=False,
                                             blank=True)
    zhong_xushi_nvjin = models.BooleanField(verbose_name='女金胶囊',
                                            help_text='女金胶囊',
                                            default=False,
                                            blank=True)
    zhong_xushi_zhibo = models.BooleanField(verbose_name='知柏地黄丸',
                                            help_text='知柏地黄丸',
                                            default=False,
                                            blank=True)
    zhong_xushi_qita = models.CharField(verbose_name='其他',
                                        help_text='其他',
                                        default=None,
                                        blank=True,
                                        null=True,
                                        max_length=100,)
    # 中医其他治疗
    zhongyi_body = models.BooleanField(verbose_name='体针',
                                       help_text='体针',
                                       default=False,
                                       blank=True)

    zhongyi_ears = models.BooleanField(verbose_name='耳针',
                                       help_text='耳针',
                                       default=False,
                                       blank=True)
    zhongyi_belly = models.BooleanField(verbose_name='腹针',
                                        help_text='腹针',
                                        default=False,
                                        blank=True)
    zhongyi_ai = models.BooleanField(verbose_name='艾灸',
                                     help_text='艾灸',
                                     default=False,
                                     blank=True)
    zhongyi_yadou = models.BooleanField(verbose_name='耳穴压豆',
                                        help_text='耳穴压豆',
                                        default=False,
                                        blank=True)

    zhongyi_zhushe = models.BooleanField(verbose_name='穴位注射',
                                         help_text='穴位注射',
                                         default=False,
                                         blank=True)
    zhongyi_futie = models.BooleanField(verbose_name='穴位敷贴',
                                        help_text='穴位敷贴',
                                        default=False,
                                        blank=True)

    zhongyi_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    blank=True,
                                    null=True,
                                    max_length=100,)
    # 西医治疗-止血治疗-性激素
    hormone_wu = models.BooleanField(verbose_name='无',
                                     help_text='无',
                                     default=False,
                                     blank=True)
    hormone_yun = models.BooleanField(verbose_name='孕激素',
                                      help_text='孕激素',
                                      default=False,
                                      blank=True)

    # 孕激素的具体药物
    yun_jizhu = models.BooleanField(verbose_name='肌注黄体酮',
                                    help_text='肌注黄体酮',
                                    default=False,
                                    blank=True)
    yun_diqu = models.BooleanField(verbose_name='地屈孕酮',
                                   help_text='地屈孕酮',
                                   default=False,
                                   blank=True)
    yun_weihua = models.BooleanField(verbose_name='微粒化黄体酮胶囊胶囊',
                                     help_text='微粒化黄体酮胶囊胶囊',
                                     default=False,
                                     blank=True)
    yun_yuntong = models.BooleanField(verbose_name='甲羟孕酮',
                                      help_text='甲羟孕酮',
                                      default=False,
                                      blank=True)
    yun_qita = models.CharField(verbose_name='其他',
                                help_text='其他',
                                default=None,
                                blank=True,
                                null=True,
                                max_length=100, )
    hormone_ci = models.BooleanField(verbose_name='雌激素',
                                     help_text='雌激素',
                                     default=False,
                                     blank=True)
    # 雌激素具体药物
    ci_benjia = models.BooleanField(verbose_name='苯甲酸雌二醇',
                                    help_text='苯甲酸雌二醇',
                                    default=False,
                                    blank=True)
    ci_jieheji = models.BooleanField(verbose_name='结合雌激素(针剂)',
                                     help_text='结合雌激素(针剂)',
                                     default=False,
                                     blank=True)
    ci_jiehepian = models.BooleanField(verbose_name='结合雌激素(片剂)',
                                       help_text='结合雌激素(片剂)',
                                       default=False,
                                       blank=True)
    ci_qita = models.CharField(verbose_name='其他',
                               help_text='其他',
                               default=None,
                               blank=True,
                               null=True,
                               max_length=100, )
    hormone_xiong = models.BooleanField(verbose_name='雄激素',
                                        help_text='雄激素',
                                        default=False,
                                        blank=True)
    hormone_kou = models.BooleanField(verbose_name='复方短效口服避孕药',
                                      help_text='复方短效口服避孕药',
                                      default=False,
                                      blank=True)

    # 复方短效口服避孕药具体药物
    kou_chunhuan = models.BooleanField(verbose_name='炔雌醇环丙孕酮片',
                                       help_text='炔雌醇环丙孕酮片',
                                       default=False,
                                       blank=True)
    kou_quluoo = models.BooleanField(verbose_name='屈螺酮炔雌醇片',
                                     help_text='屈螺酮炔雌醇片',
                                     default=False,
                                     blank=True)
    kou_quluot = models.BooleanField(verbose_name='屈螺酮炔雌醇片Ⅱ',
                                     help_text='屈螺酮炔雌醇片Ⅱ',
                                     default=False,
                                     blank=True)
    kou_quyang = models.BooleanField(verbose_name='去氧孕烯炔雌醇片',
                                     help_text='去氧孕烯炔雌醇片',
                                     default=False,
                                     blank=True)
    kou_fufang = models.BooleanField(verbose_name='复方孕二烯酮片',
                                     help_text='复方孕二烯酮片',
                                     default=False,
                                     blank=True)
    kou_qita = models.CharField(verbose_name='其他',
                                help_text='其他',
                                default=None,
                                blank=True,
                                null=True,
                                max_length=100, )

    hormone_gn = models.BooleanField(verbose_name='GnRH-α',
                                     help_text='GnRH-α',
                                     default=False,
                                     blank=True)
    hormone_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    blank=True,
                                    null=True,
                                    max_length=100, )
    # 西医治疗-止血治疗-刮宫术
    GUAGONG_CHOICES = (
        ('是', '是'),
        ('否', '否')
    )

    guagongshu = models.CharField(verbose_name='刮宫术',
                                  help_text='刮宫术',
                                  blank=True,
                                  default=None,
                                  max_length=10,
                                  null=True,)
    # 调周治疗
    zhou_wu = models.BooleanField(verbose_name='无',
                                  help_text='无',
                                  default=False,
                                  blank=True)
    zhou_yun = models.BooleanField(verbose_name='孕激素',
                                   help_text='孕激素',
                                   default=False,
                                   blank=True)
    zhou_kou = models.BooleanField(verbose_name='口服避孕药',
                                   help_text='口服避孕药',
                                   default=False,
                                   blank=True)
    zhou_ci = models.BooleanField(verbose_name='雌、孕激素序贯法',
                                  help_text='雌、孕激素序贯法',
                                  default=False,
                                  blank=True)
    zhou_zuo = models.BooleanField(verbose_name='左炔诺孕酮宫内缓释系统',
                                   help_text='左炔诺孕酮宫内缓释系统',
                                   default=False,
                                   blank=True)
    zhou_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 default=None,
                                 blank=True,
                                 null=True,
                                 max_length=100,)
    # 促排卵
    cu_wu = models.BooleanField(verbose_name='无',
                                help_text='无',
                                default=False,
                                blank=True)
    cu_mifen = models.BooleanField(verbose_name='氯米芬',
                                   help_text='氯米芬',
                                   default=False,
                                   blank=True)
    cu_rongmao = models.BooleanField(verbose_name='人绒毛膜促性腺激素(hCG)',
                                     help_text='人绒毛膜促性腺激素(hCG)',
                                     default=False,
                                     blank=True)
    cu_niao = models.BooleanField(verbose_name='尿促性素(hMG)',
                                  help_text='尿促性素(hMG)',
                                  default=False,
                                  blank=True)
    cu_luan = models.BooleanField(verbose_name='卵泡期使用低剂量雌激素',
                                  help_text='卵泡期使用低剂量雌激素',
                                  default=False,
                                  blank=True)
    cu_lai = models.BooleanField(verbose_name='来曲唑',
                                 help_text='来曲唑',
                                 default=False,
                                 blank=True)
    cu_cu = models.BooleanField(verbose_name='促卵泡生成激素(FSH)',
                                help_text='促卵泡生成激素(FSH)',
                                default=False,
                                blank=True)
    cu_qita = models.CharField(verbose_name='其他',
                               help_text='其他',
                               default=None,
                               blank=True,
                               null=True,
                               max_length=100,)
    # 手术治疗
    shu_wu = models.BooleanField(verbose_name='无',
                                 help_text='无',
                                 default=False,
                                 blank=True)
    shu_neimo = models.BooleanField(verbose_name='子宫内膜去除术',
                                    help_text='子宫内膜去除术',
                                    default=False,
                                    blank=True)
    shu_qiechu = models.BooleanField(verbose_name='子宫切除术',
                                     help_text='子宫切除术',
                                     default=False,
                                     blank=True)
    shu_qita = models.CharField(verbose_name='其他',
                                help_text='其他',
                                default=None,
                                blank=True,
                                null=True,
                                max_length=100,)

    # 其他治疗
    other_cure = models.CharField(verbose_name='有',
                                  help_text='有',
                                  default=None,
                                  blank=True,
                                  null=True,
                                  max_length=200,)
    other_cure_wu = models.BooleanField(verbose_name='无',
                                        help_text='无',
                                        default=False,
                                        blank=True)

    class Meta:
        verbose_name = '中西治疗'
        verbose_name_plural = verbose_name
        ordering = ('-pk', 'person')

    def __str__(self):
        return '%s' % self.pk
