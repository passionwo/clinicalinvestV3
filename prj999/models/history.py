from django.db import models
from .geninfo import GeneralInfo
from .menstruation import Menstruation


class History(Menstruation):
    person = models.OneToOneField(GeneralInfo,
                                  related_name='history_nine',
                                  on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              related_name='my_history_nine',
                              null=True,
                              blank=True,
                              on_delete=models.CASCADE)

    pasthistory_wu = models.BooleanField(verbose_name=u'无',
                                         blank=True,
                                         default=False,
                                         help_text="无")

    pasthistory_yindaoyan = models.BooleanField(verbose_name=u'阴道炎',
                                                blank=True,
                                                default=False,
                                                help_text="阴道炎")
    pasthistory_zigongneimoyan = models.BooleanField(verbose_name=u'子宫内膜炎',
                                                     blank=True,
                                                     default=False,
                                                     help_text="子宫内膜炎")
    pasthistory_zigongneimoyiwei = models.BooleanField(verbose_name=u'子宫内膜异位症',
                                                       blank=True,
                                                       default=False,
                                                       help_text="子宫内膜异位症")
    pasthistory_zigongxianjizheng = models.BooleanField(verbose_name=u'子宫腺肌症',
                                                        blank=True,
                                                        default=False,
                                                        help_text="子宫腺肌症")
    pasthistory_penqiangyan = models.BooleanField(verbose_name=u'盆腔炎性疾病',
                                                  blank=True,
                                                  default=False,
                                                  help_text="盆腔炎性疾病")
    pasthistory_zigongjiliu = models.BooleanField(verbose_name=u'子宫肌瘤',
                                                  blank=True,
                                                  default=False,
                                                  help_text="子宫肌瘤")
    pasthistory_luancaonangzhong = models.BooleanField(verbose_name=u'卵巢囊肿',
                                                       blank=True,
                                                       default=False,
                                                       help_text="卵巢囊肿")
    pasthistory_ruxianzengsheng = models.BooleanField(verbose_name=u'乳腺增生',
                                                      blank=True,
                                                      default=False,
                                                      help_text="乳腺增生")

    pasthistory_shengzhiyichang = models.BooleanField(verbose_name=u'生殖器官发育异常',
                                                      blank=True,
                                                      default=False,
                                                      help_text="生殖器发育异常")
    # pasthistory_jiazhuangxian = models.BooleanField(verbose_name=u'甲状腺相关疾病',
    #                                                 blank=True,
    #                                                 default=False,
    #                                                 help_text="甲状腺相关疾病")

    pasthistory_minus = models.BooleanField(verbose_name=u'甲减',
                                            blank=True,
                                            default=False,
                                            help_text="甲减")

    pasthistory_plus = models.BooleanField(verbose_name=u'甲亢',
                                           blank=True,
                                           default=False,
                                           help_text="甲亢")

    pasthistory_shenshangxian = models.BooleanField(verbose_name=u'肾上腺相关疾病',
                                                    blank=True,
                                                    default=False,
                                                    help_text="肾上腺相关疾病")
    pasthistory_xueye = models.BooleanField(verbose_name=u'血液系统相关疾病',
                                            blank=True,
                                            default=False,
                                            help_text="血液系统相关疾病")
    pasthistory_naochuitiliu = models.BooleanField(verbose_name=u'脑垂体瘤',
                                                   blank=True,
                                                   default=False,
                                                   help_text="脑垂体瘤")
    pasthistory_tangniaobing = models.BooleanField(verbose_name=u'糖尿病',
                                                   blank=True,
                                                   default=False,
                                                   help_text="糖尿病")
    pasthistory_feipang = models.BooleanField(verbose_name=u'肥胖',
                                              blank=True,
                                              default=False, help_text="肥胖")
    pasthistory_ganyan = models.BooleanField(verbose_name=u'肝炎',
                                             blank=True,
                                             default=False, help_text="肝炎")
    pasthistory_jiehe = models.BooleanField(verbose_name=u'结核',
                                            blank=True,
                                            default=False, help_text="结核")
    pasthistory_qita = models.CharField(verbose_name=u'其它病史',
                                        blank=True,
                                        null=True,
                                        max_length=50,
                                        default=None,
                                        help_text="其他")
    # 嗜好
    hobbies_wu = models.BooleanField(verbose_name=u'无',
                                     blank=True,
                                     default=False,
                                     help_text="无")
    hobbies_xiyan = models.BooleanField(verbose_name=u'吸烟',
                                        blank=True,
                                        default=False,
                                        help_text="吸烟")
    hobbies_yinjiu = models.BooleanField(verbose_name=u'饮酒',
                                         blank=True,
                                         default=False,
                                         help_text="饮酒")
    hobbies_aoye = models.BooleanField(verbose_name=u'熬夜',
                                       blank=True,
                                       default=False,
                                       help_text="熬夜")
    hobbies_qita = models.CharField(verbose_name=u'其它嗜好',
                                    blank=True,
                                    null=True,
                                    max_length=50,
                                    default=None,
                                    help_text="其他嗜好")
    BODY_COND = (
        ('好', '好'),
        ('一般', '一般'),
        ('易疲倦', '易疲倦'),
    )
    body_cond = models.CharField(verbose_name=u'体力状况',
                                 blank=True,
                                 default=None,
                                 null=True,
                                 choices=BODY_COND,
                                 max_length=3,
                                 help_text="体力状况")
    CAREERLABOR = (
        (u'重体力劳动(如:搬运工、清洁工、农场工人、畜牧场工人等)', u'重体力劳动(如:搬运工、清洁工、农场工人、畜牧场工人等)'),
        (u'中体力劳动(如:家政服务人员、服务生、厨师、护士等)', u'中体力劳动(如:家政服务人员、服务生、厨师、护士等)'),
        (u'轻体力劳动(如:教师、美容美发师、批发商、职员等)', u'中体力劳动(如:教师、美容美发师、批发商、职员等)'),
        (u'坐式的工作(如:收银员、出纳员、接线员、秘书等)', u'坐式的工作(如:收银员、出纳员、接线员、秘书等)'),
    )
    career_labor = models.CharField(verbose_name=u'职业体力活动',
                                    blank=True,
                                    null=True,
                                    choices=CAREERLABOR,
                                    max_length=30,
                                    help_text="职业体力活动")
    PHYCIALEXER = (
        (u'很少(≤1次/周)', u'很少(≤1次/周)'),
        (u'偶尔(≤3次/周)', u'偶尔(≤3次/周)'),
        (u'经常(≥4次/周)', u'经常(≥4次/周)')
    )
    EXERCISE_INTE = (
        (u'一般(少量出汗,心率≤120次/分)', u' 一般(少量出汗,心率≤120次/分)'),
        (u'高强度(大汗淋漓,心率>120次/分)', u' 高强度(大汗淋漓,心率>120次/分)'),
    )
    physical_exercise = models.CharField(verbose_name=u'频次',
                                         blank=True,
                                         null=True,
                                         choices=PHYCIALEXER,
                                         max_length=20,
                                         help_text="频次")

    physical_intensity = models.CharField(verbose_name=u'强度',
                                          blank=True,
                                          null=True,
                                          choices=EXERCISE_INTE,
                                          max_length=30,
                                          help_text="强度")

    # 减肥情况
    reducefat_persist = models.IntegerField(verbose_name=u'减肥时长(月)',
                                            blank=True,
                                            null=True,
                                            help_text="减肥时长(月)")
    reducefat_yundong = models.BooleanField(verbose_name=u'运动减肥',
                                            blank=True,
                                            default=False,
                                            help_text="运动减肥")
    reducefat_jieshi = models.BooleanField(verbose_name=u'节食减肥',
                                           blank=True,
                                           default=False,
                                           help_text="节食减肥")
    reducefat_yaowu = models.BooleanField(verbose_name=u'药物减肥',
                                          blank=True,
                                          default=False,
                                          help_text="药物减肥")
    reducefat_qita = models.CharField(verbose_name=u'其它减肥',
                                      blank=True,
                                      null=True,
                                      max_length=50,
                                      default=None,
                                      help_text="其他减肥")

    class Meta:
        verbose_name = '患者病史'
        verbose_name_plural = verbose_name
        ordering = ('-pk', 'person')

    def __str__(self):
        return '%s' % self.pk
