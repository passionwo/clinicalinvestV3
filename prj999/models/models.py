from django.db import models

from myuser.models import MyUser


class InvestFileUpload(models.Model):
    name = models.CharField(
        verbose_name=u'名称', max_length=10, help_text="名称", default="")
    ivfile = models.FileField(verbose_name=u'文件地址',
                              upload_to='%Y-%m-%d/%H-%M',
                              # default="/avatars/default.xlsx",
                              help_text="文件地址",)
    owner = models.ForeignKey(MyUser,
                              related_name='myinvestfileupload_nine',
                              on_delete=models.CASCADE,
                              help_text="用户")

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = u'上传文件'
        verbose_name_plural = verbose_name
