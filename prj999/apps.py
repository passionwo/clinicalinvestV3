from django.apps import AppConfig

VERBOSE_APP_NAME = "项目测试-测试的模块"


class Prj999Config(AppConfig):
    name = 'prj999'
    verbose_name = VERBOSE_APP_NAME
