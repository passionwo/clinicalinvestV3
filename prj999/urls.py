from django.urls import path
from .views import GenInfoView, CCView
from .views import RelevantView, CureView, HistoryView
from .views import SummaryView, ResultsView, UploadExcelView
from .views import MobileGeninfoView
from .views import SearchGenInfoView

get_put_delete = {
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
}

get_post = {
    'post': 'create',
    'get': 'list'
}

gen_info_list_view = GenInfoView.as_view({
    'get': 'list',
})
# gen_info_single_view = GenInfoView.as_view({
#     'get': 'retrieve',
# })
gen_info_create_view = GenInfoView.as_view({
    'post': 'create',
})
gen_info_view = GenInfoView.as_view(get_put_delete)

cc_pk_view = CCView.as_view(get_put_delete)
cc_view = CCView.as_view(get_post)

cure_pk_view = CureView.as_view(get_put_delete)
cure_view = CureView.as_view(get_post)

relevant_pk_view = RelevantView.as_view(get_put_delete)
relevant_view = RelevantView.as_view(get_post)

history_pk_view = HistoryView.as_view(get_put_delete)
history_view = HistoryView.as_view(get_post)

summary_pk_view = SummaryView.as_view(get_put_delete)
summary_view = SummaryView.as_view(get_post)

results_pk_view = ResultsView.as_view(get_put_delete)
results_view = ResultsView.as_view(get_post)

urlpatterns = [
    path('mobile/', MobileGeninfoView.as_view({'post': 'create'})),

    path('info/search/', SearchGenInfoView.as_view({'post': 'list'})),

    path('info/<pk>/checked/',
         GenInfoView.as_view({'post': 'partial_update'})),

    path('info/', gen_info_list_view, name='generalinfo-nine-details'),
    path('info/create/', gen_info_create_view),
    # all info about cure and cc...
    path('info/<pk>/', gen_info_view, name='generalinfo-nine-detail'),
    # user owner
    # path('info/user/', gen_info_single_view, name='generalinfo-detail'),

    path('cc/<pk>/', cc_pk_view, name='cc-nine-detail'),
    path('cc/', cc_view, name='cc-nine-detail'),

    path('cure/<pk>/', cure_pk_view, name='cure-nine-detail'),
    path('cure/', cure_view, name='cure-nine-detail'),

    path('relevant/<pk>/', relevant_pk_view, name='relevant-nine-detail'),
    path('relevant/', relevant_view, name='relevant-nine-detail'),

    path('history/<pk>/', history_pk_view, name='history-nine-detail'),
    path('history/', history_view, name='history-nine-detail'),

    path('summary/<pk>/', summary_pk_view, name='summary-nine-detail'),
    path('summary/', summary_view, name='summary-nine-detail'),

    path('results/<pk>/', results_pk_view, name='results-nine-detail'),
    path('results/', results_view, name='results-nine-detail'),

    path('upload/', UploadExcelView.as_view({'post': 'create'})),

    # path('fileout/', FileDownloadView.as_view({'post': 'list'})),

]
