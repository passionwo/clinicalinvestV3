from rest_framework import permissions

from .utils.group_info import get_all_group_info

from .models.geninfo import GeneralInfo

from rest_framework.exceptions import PermissionDenied as permDenied


class CheckOptionPermission(permissions.BasePermission):

    def has_permission(self, request, view):

        all_perm = request.user.get_all_permissions()
        own_perm = [perm for perm in all_perm if perm.startswith('prj999')]

        if not own_perm:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if request.user.has_perm('prj999.prj999_all'):
            return True
        elif request.user.has_perm('prj999.prj999_operation'):
            if request.method in permissions.SAFE_METHODS:
                return True
            else:
                return obj.owner == request.user
        else:
            return False


class MobileClientPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            if request.user.has_perm('prj999.prj999_patient'):
                return True
            else:
                return False
        # return True


def get_queryset_perm(sf, obj=None):
    # check perm
    user = sf.request.user
    is_self = user.has_perm('prj999.prj999_operation')
    is_all = user.has_perm('prj999.prj999_all')

    if is_all:
        return get_all_group_info(obj=obj)
    elif is_self:
        if obj:
            return obj.objects.filter(owner=user).order_by('-serial')
        return GeneralInfo.objects.filter(owner=user).order_by('-serial')
    else:
        raise permDenied(detail={'msg': '对不起, 您没有查看数据的权限'})
