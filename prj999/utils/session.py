

def set_session_list(request=None, gen_objects=None):
    try:
        del request.session[request.user.id]
    except KeyError:
        pass

    # save data into session
    try:
        id_ = [obj.id for obj in gen_objects]
        request.session[request.user.id] = id_
    except ValueError:
        pass
