from django.contrib import admin
from .models.geninfo import GeneralInfo
from .models.cure import Cure
from .models.history import History
from .models.results import Results
from .models.summary import Summary
from .models.cc import CC
from .models.relevant import Relevant

from django.contrib.admin import ModelAdmin

from .utils.create import computed_cure, update_model


@admin.register(GeneralInfo)
class GeneralInfoAdmin(ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('owner', 'serial', 'name', 'recdate',
                       'hospital', 'telephone', 'expert', 'title',
                       'birth_year', 'birth_month', 'height', 'weight', 'waistline',
                       'hipline', 'nation', 'career',
                       'address', 'entrance', 'culture'),
        }),
        ('更多内容(可选内容)', {
            'classes': 'collapse',
            'fields': ('gaowen', 'diwen', 'yeban', 'zaosheng',
                       'fushe', 'huagongyinran', 'julieyundong',
                       'qiyou', 'gaokong', 'wu', 'wuteshu', 'sushi',
                       'suan', 'tian', 'xian', 'xinla',
                       'you', 'shengleng', 'kafei', 'qita',),
        })
    )
    list_display = ('pk', 'serial', 'name', 'recdate', 'telephone',
                    'hospital', 'title', 'birth_year', 'birth_month', 'height', 'weight',
                    'degree_of_completion', 'is_checked')
    empty_value_display = '空'
    # list_editable = ('recdate',)
    list_filter = ('serial', 'name', 'telephone', 'title')

    def save_model(self, request, obj, form, change):
        cure_degree = computed_cure(data=obj, tps='object')
        info_degree = update_model(obj, tps='info')
        num_float = (cure_degree + info_degree) * 100
        obj.degree_of_completion = '%.2f%%' % num_float
        obj.save()


@admin.register(Cure)
class CureAdmin(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'to_cure')
    empty_value_display = '空'
    list_filter = ('person', 'owner',)


@admin.register(History)
class History(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'pasthistory_yindaoyan', 'hobbies_wu',
                    'body_cond', 'physical_exercise')
    empty_value_display = '空'
    list_filter = ('person', 'owner',)


@admin.register(Relevant)
class RelevantAdmin(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'menstruation_check',
                    'accessory_hgb_value', 'accessory_ningxue',
                    'accessory_neifenmi')
    empty_value_display = '空'
    list_filter = ('person', 'owner',)


@admin.register(Results)
class ResultsAdmin(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'last_result', 'far_result',)
    empty_value_display = '空'
    list_filter = ('person', 'owner',)


@admin.register(CC)
class CCAdmin(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'xuzheng')
    empty_value_display = '空'
    list_filter = ('person', 'owner',)


@admin.register(Summary)
class SummaryAdmin(ModelAdmin):
    list_display = ('pk', 'person', 'owner', 'owner_buguize', 'owner_suoduan',
                    'owner_yanchang', 'owner_liangduo')
    empty_value_display = '空'
    list_filter = ('person', 'owner',)
