from .basemenstruation import BaseMenstruation, models


# 月经带下史
class Menstruation(BaseMenstruation):

    mm_blood_cond_qita = models.CharField(verbose_name='出血量-其他',
                                          help_text='出血量-其他',
                                          default=None,
                                          null=True,
                                          blank=True,
                                          max_length=200)

    mm_blood_color_qita = models.CharField(verbose_name='出血颜色-其他',
                                           help_text='出血颜色-其他',
                                           default=None,
                                           null=True,
                                           blank=True,
                                           max_length=200)
    mm_blood_block_qita = models.CharField(verbose_name='出血质地(2)-其他',
                                           help_text='出血质地(2)-其他',
                                           default=None,
                                           null=True,
                                           blank=True,
                                           max_length=200)

    TRUE_FALSE = (
        ('true', '有'),
        # ('True', '有'),
    )
    ABNORMAL = (
        (u'或1月多次', u'或1月多次'),
        (u'1-2个月1行', u'1-2个月1行'),
        (u'2-3个月1行', u'2-3个月1行'),
        (u'3-4个月1行', u'3-4个月1行'),
        (u'4-6个月1行', u'4-6个月1行'),
        (u'>6个月1行', u'>6个月1行'),
    )
    CYCLICITY_SUM = (
        (u'≤2天', u'≤2天'),
        (u'3-7天', u'3-7天'),
        (u'7天以上甚至半月', u'7天以上甚至半月'),
    )
    FIRST_TIME = (
        ('10岁以前', '10岁以前'),
        ('11岁以后', '11岁以后'),
        ('14岁以后', '14岁以后'),
        ('16岁以后', '16岁以后'),
    )
    NORMAL_AGE = (
        ('21-25天', '21-25天'),
        ('26-30天', '26-30天'),
        ('31-35天', '31-35天')
    )

    first_time = models.CharField(verbose_name='月经初潮年龄',
                                  help_text="月经初潮年龄",
                                  choices=FIRST_TIME,
                                  max_length=20,
                                  null=True,
                                  default=None,
                                  blank=True)
    first_time_qita = models.CharField(verbose_name='其他',
                                       help_text='其他',
                                       default=None,
                                       null=True,
                                       blank=True,
                                       max_length=200)

    normal = models.CharField(verbose_name=u'月经周期尚规律的间隔天数',
                              blank=True,
                              default=None,
                              null=True,
                              choices=NORMAL_AGE,
                              max_length=10,
                              help_text="月经周期尚规律的间隔天数")
    abnormal = models.CharField(verbose_name=u'月经不规律的情况',
                                choices=ABNORMAL,
                                max_length=10,
                                blank=True,
                                null=True,
                                default=None,
                                help_text="月经不规律的情况")
    cyclicity_sum = models.CharField(verbose_name=u'行经天数',
                                     choices=CYCLICITY_SUM,
                                     max_length=100,
                                     blank=True,
                                     null=True,
                                     default=None,
                                     help_text="行经天数")

    cyclicity_sum_qita = models.CharField(verbose_name='其他',
                                          help_text='其他',
                                          default=None,
                                          null=True,
                                          blank=True,
                                          max_length=200)
    # 经期伴随症状
    menstruation_is_accompany = models.BooleanField(verbose_name='经期是否伴随症状',
                                                    default=False,
                                                    blank=True,
                                                    help_text='经期是否伴随症状')

    # if menstruation_is_accompany
    DEEP_LEVEL = (
        ('没有', '没有'),
        ('很少', '很少'),
        ('有时', '有时'),
        ('经常', '经常'),
        ('总是', '总是')
    )
    # 精神/情绪
    spirit_shenpi = models.CharField(verbose_name='神疲肢倦',
                                     default=None,
                                     max_length=2,
                                     choices=DEEP_LEVEL,
                                     null=True,
                                     blank=True,
                                     help_text='神疲肢倦')
    spirit_qiduan = models.CharField(verbose_name='气短懒言',
                                     default=None,
                                     max_length=2,
                                     blank=True,
                                     choices=DEEP_LEVEL,
                                     null=True,
                                     help_text='气短懒言')
    spirit_yiyu_m = models.CharField(verbose_name='精神抑郁',
                                     default=None,
                                     blank=True,
                                     max_length=2,
                                     choices=DEEP_LEVEL,
                                     null=True,
                                     help_text='精神抑郁')
    spirit_tanxi = models.CharField(verbose_name='时欲叹息',
                                    default=None,
                                    blank=True,
                                    max_length=2,
                                    choices=DEEP_LEVEL,
                                    null=True,
                                    help_text='时欲叹息')
    spirit_yinu = models.CharField(verbose_name='烦躁易怒',
                                   default=None,
                                   blank=True,
                                   max_length=2,
                                   null=True,
                                   choices=DEEP_LEVEL,
                                   help_text='烦躁易怒')
    spirit_qita = models.CharField(verbose_name='其他',
                                   default=None,
                                   blank=True,
                                   null=True,
                                   max_length=200,
                                   help_text='其他')
    # 形体/四肢
    body_normal = models.BooleanField(verbose_name='正常',
                                      help_text='正常',
                                      blank=True,
                                      default=False)
    body_fat = models.BooleanField(verbose_name='形体肥胖',
                                   help_text='形体肥胖',
                                   blank=True,
                                   default=False, )
    body_thin = models.BooleanField(verbose_name='形体瘦小',
                                    help_text='形体瘦小',
                                    blank=True,
                                    default=False, )
    body_skin = models.BooleanField(verbose_name='皮肤不润',
                                    help_text='皮肤不润',
                                    blank=True,
                                    default=False, )
    body_cold = models.BooleanField(verbose_name='畏寒肢冷',
                                    help_text='畏寒肢冷',
                                    blank=True,
                                    default=False, )
    body_hot = models.BooleanField(verbose_name='手足心热',
                                   help_text='手足心热',
                                   blank=True,
                                   default=False, )
    body_leg = models.BooleanField(verbose_name='腰酸腿软',
                                   help_text='腰酸腿软',
                                   blank=True,
                                   default=False, )
    body_waist = models.BooleanField(verbose_name='腰痛如折',
                                     help_text='腰痛如折',
                                     blank=True,
                                     default=False, )
    body_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 max_length=100,
                                 null=True,
                                 blank=True,
                                 default=None, )
    # 头面
    face_head_normal = models.BooleanField(verbose_name='正常',
                                           help_text='正常',
                                           blank=True,
                                           default=False)
    face_head_cangbai = models.CharField(verbose_name='面色苍白',
                                         choices=TRUE_FALSE,
                                         max_length=4,
                                         null=True,
                                         blank=True,
                                         default=None,
                                         help_text='面色苍白', )
    face_head_huangbai = models.CharField(verbose_name='面色晄白',
                                          help_text='面色晄白',
                                          choices=TRUE_FALSE,
                                          max_length=4,
                                          blank=True,
                                          null=True,
                                          default=None, )
    face_head_weihuang = models.CharField(verbose_name='面色萎黄',
                                          help_text='面色萎黄',
                                          choices=TRUE_FALSE,
                                          max_length=4,
                                          null=True,
                                          blank=True,
                                          default=None, )
    face_head_huian = models.CharField(verbose_name='面色晦暗',
                                       help_text='面色晦暗',
                                       choices=TRUE_FALSE,
                                       max_length=4,
                                       null=True,
                                       blank=True,
                                       default=None, )
    face_head_anban = models.CharField(verbose_name='面有黯斑',
                                       help_text='面有黯斑',
                                       choices=TRUE_FALSE,
                                       max_length=4,
                                       null=True,
                                       blank=True,
                                       default=None, )
    face_head_zhizhong = models.CharField(verbose_name='面浮肢肿',
                                          help_text='面浮肢肿',
                                          choices=TRUE_FALSE,
                                          max_length=4,
                                          null=True,
                                          blank=True,
                                          default=None, )
    face_head_chunhong = models.CharField(verbose_name='颧赤唇红',
                                          help_text='颧赤唇红',
                                          choices=TRUE_FALSE,
                                          max_length=4,
                                          null=True,
                                          blank=True,
                                          default=None, )
    face_head_kouku = models.CharField(verbose_name='口苦咽干',
                                       help_text='口苦咽干',
                                       choices=DEEP_LEVEL,
                                       max_length=2,
                                       null=True,
                                       blank=True,
                                       default=None, )
    face_head_erming = models.CharField(verbose_name='头晕耳鸣',
                                        help_text='头晕耳鸣',
                                        choices=DEEP_LEVEL,
                                        max_length=2,
                                        null=True,
                                        blank=True,
                                        default=None, )
    face_head_yanghua = models.CharField(verbose_name='眼花',
                                         help_text='眼花',
                                         choices=DEEP_LEVEL,
                                         max_length=2,
                                         blank=True,
                                         null=True,
                                         default=None, )
    face_head_qita = models.CharField(verbose_name='其他',
                                      help_text='其他',
                                      max_length=100,
                                      null=True,
                                      blank=True,
                                      default=None, )

    # 胸腹
    belly_fanmen = models.CharField(verbose_name='心胸烦闷',
                                    help_text='心胸烦闷',
                                    choices=DEEP_LEVEL,
                                    max_length=2,
                                    blank=True,
                                    null=True, )

    belly_rufangzhangtong = models.CharField(verbose_name='经前乳房胀痛',
                                             help_text='经前乳房胀痛',
                                             choices=DEEP_LEVEL,
                                             max_length=2,
                                             null=True,
                                             blank=True,
                                             default=None, )
    belly_xiongxiezhangtong = models.CharField(verbose_name='经前胸胁胀痛',
                                               help_text='经前胸胁胀痛',
                                               choices=DEEP_LEVEL,
                                               max_length=2,
                                               blank=True,
                                               null=True,
                                               default=None, )
    belly_shaofuzhangtong = models.CharField(verbose_name='经前少腹胀痛',
                                             help_text='经前少腹胀痛',
                                             choices=DEEP_LEVEL,
                                             max_length=2,
                                             null=True,
                                             blank=True,
                                             default=None, )
    belly_kongzhui = models.CharField(verbose_name='小腹空坠',
                                      help_text='小腹空坠',
                                      choices=DEEP_LEVEL,
                                      max_length=2,
                                      null=True,
                                      blank=True,
                                      default=None, )
    belly_kongtong = models.CharField(verbose_name='小腹空痛',
                                      help_text='小腹空痛',
                                      choices=DEEP_LEVEL,
                                      max_length=2,
                                      null=True,
                                      blank=True,
                                      default=None, )
    belly_citong = models.CharField(verbose_name='小腹刺痛',
                                    help_text='小腹刺痛',
                                    choices=DEEP_LEVEL,
                                    max_length=2,
                                    blank=True,
                                    null=True,
                                    default=None, )
    belly_zhangtong = models.CharField(verbose_name='小腹胀痛',
                                       help_text='小腹胀痛',
                                       choices=DEEP_LEVEL,
                                       max_length=2,
                                       null=True,
                                       blank=True,
                                       default=None, )
    belly_lengtong = models.CharField(verbose_name='小腹冷痛',
                                      help_text='小腹冷痛',
                                      choices=DEEP_LEVEL,
                                      max_length=2,
                                      null=True,
                                      blank=True,
                                      default=None, )
    belly_yintong = models.CharField(verbose_name='小腹隐痛',
                                     help_text='小腹隐痛',
                                     choices=DEEP_LEVEL,
                                     max_length=2,
                                     blank=True,
                                     null=True,
                                     default=None, )
    belly_juan = models.CharField(verbose_name='疼痛拒按',
                                  help_text='疼痛拒按',
                                  choices=TRUE_FALSE,
                                  max_length=4,
                                  null=True,
                                  blank=True,
                                  default=None, )
    belly_xian = models.CharField(verbose_name='腹痛喜按',
                                  help_text='腹痛喜按',
                                  choices=TRUE_FALSE,
                                  max_length=4,
                                  blank=True,
                                  null=True,
                                  default=None, )
    belly_deretongjian = models.CharField(verbose_name='腹痛得热痛减',
                                          help_text='腹痛得热痛减',
                                          choices=TRUE_FALSE,
                                          max_length=4,
                                          blank=True,
                                          null=True,
                                          default=None, )
    belly_tongjian = models.CharField(verbose_name='血块下痛减',
                                      help_text='血块下痛减',
                                      choices=TRUE_FALSE,
                                      max_length=4,
                                      null=True,
                                      blank=True,
                                      default=None, )
    belly_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  null=True,
                                  blank=True,
                                  default=None,
                                  max_length=100, )
    # 饮食
    diet_exin = models.CharField(verbose_name='脘闷恶心',
                                 help_text='脘闷恶心',
                                 choices=DEEP_LEVEL,
                                 max_length=2,
                                 null=True,
                                 blank=True,
                                 default=None, )
    diet_shishao = models.CharField(verbose_name='嗳气食少',
                                    help_text='嗳气食少',
                                    choices=DEEP_LEVEL,
                                    max_length=2,
                                    null=True,
                                    blank=True,
                                    default=None, )
    diet_zhangman = models.CharField(verbose_name='脘腹胀满',
                                     help_text='脘腹胀满',
                                     choices=DEEP_LEVEL,
                                     max_length=2,
                                     null=True,
                                     blank=True,
                                     default=None, )
    diet_bujia = models.CharField(verbose_name='食欲不佳',
                                  help_text='食欲不佳',
                                  choices=DEEP_LEVEL,
                                  max_length=2,
                                  null=True,
                                  blank=True,
                                  default=None, )
    diet_lengyin = models.CharField(verbose_name='渴喜冷饮',
                                    help_text='渴喜冷饮',
                                    choices=DEEP_LEVEL,
                                    max_length=2,
                                    null=True,
                                    blank=True,
                                    default=None, )
    diet_kouzao = models.CharField(verbose_name='咽干口燥',
                                   help_text='咽干口燥',
                                   choices=DEEP_LEVEL,
                                   max_length=2,
                                   null=True,
                                   blank=True,
                                   default=None, )
    diet_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 null=True,
                                 blank=True,
                                 default=None,
                                 max_length=100, )
    # 睡眠
    sleep_shimian = models.CharField(verbose_name='心悸失眠',
                                     help_text='心悸失眠',
                                     choices=DEEP_LEVEL,
                                     max_length=2,
                                     null=True,
                                     blank=True,
                                     default=None, )
    sleep_buning = models.CharField(verbose_name='夜寐不宁',
                                    help_text='夜寐不宁',
                                    choices=DEEP_LEVEL,
                                    max_length=2,
                                    null=True,
                                    blank=True,
                                    default=None, )
    sleep_mengduo = models.CharField(verbose_name='夜寐梦多',
                                     help_text='夜寐梦多',
                                     choices=DEEP_LEVEL,
                                     max_length=2,
                                     null=True,
                                     blank=True,
                                     default=None, )
    sleep_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  null=True,
                                  blank=True,
                                  default=None,
                                  max_length=100, )
    # 二便
    erbian_normal = models.BooleanField(verbose_name='正常',
                                        help_text='正常',
                                        blank=True,
                                        default=False, )
    erbian_zaojie = models.BooleanField(verbose_name='大便燥结',
                                        help_text='大便燥结',
                                        blank=True,
                                        default=False, )
    erbian_tangbo = models.BooleanField(verbose_name='大便溏薄',
                                        help_text='大便溏薄',
                                        blank=True,
                                        default=False, )
    erbian_pinshu = models.BooleanField(verbose_name='小便频数',
                                        help_text='小便频数',
                                        blank=True,
                                        default=False, )
    erbian_duanchi = models.BooleanField(verbose_name='小便短赤',
                                         help_text='小便短赤',
                                         blank=True,
                                         default=False, )
    erbian_qingchang = models.BooleanField(verbose_name='小便清长',
                                           help_text='小便清长',
                                           blank=True,
                                           default=False, )
    erbian_qita = models.CharField(verbose_name='其他',
                                   help_text='其他',
                                   null=True,
                                   blank=True,
                                   default=None,
                                   max_length=100, )
    # 经期情况
    J_LEVEL = (
        ('无', '无'),
        ('有', '有'),
        ('偶尔', '偶尔'),
        ('经常', '经常')
    )
    jingqi_yundong = models.CharField(verbose_name='经期运动',
                                      help_text='经期运动',
                                      default=None,
                                      blank=True,
                                      null=True,
                                      choices=J_LEVEL,
                                      max_length=2)
    jingqi_ganmao = models.CharField(verbose_name='经期感冒',
                                     help_text='经期感冒',
                                     default=None,
                                     blank=True,
                                     null=True,
                                     choices=J_LEVEL,
                                     max_length=2)
    jingqi_tongfang = models.CharField(verbose_name='经期同房',
                                       help_text='经期同房',
                                       default=None,
                                       blank=True,
                                       null=True,
                                       choices=J_LEVEL,
                                       max_length=2)
    jingqi_zhaoliang = models.CharField(verbose_name='经期着凉',
                                        help_text='经期着凉',
                                        default=None,
                                        blank=True,
                                        null=True,
                                        choices=J_LEVEL,
                                        max_length=2)
    # YYYY-MM-DD
    last_time = models.DateField(null=True,
                                 default=None,
                                 blank=True,
                                 verbose_name='末次行经日期',
                                 help_text='末次行经日期')
    # 平素带下
    FIRST_LE = (
        ('带下量正常', '带下量正常'),
        ('带下量少', '带下量少'),
        ('带下量多', '带下量多')
    )
    SECOND_LE = (
        ('带下透明', '带下透明'),
        ('带下色黄', '带下色黄'),
        ('带下色白', '带下色白'),
        ('带下色黄绿', '带下色黄绿'),
        ('其他', '其他')
    )
    THIRD_LE = (
        ('带下质黏而不稠', '带下质黏而不稠'),
        ('带下质清稀', '带下质清稀'),
        ('带下质稠', '带下质稠')
    )
    leucorrhea_quantity = models.CharField(verbose_name='平素带下情况-量',
                                           blank=True,
                                           null=True,
                                           choices=FIRST_LE,
                                           max_length=5,
                                           default=None,
                                           help_text='平素带下情况-量')
    leucorrhea_color = models.CharField(verbose_name='平素带下情况-色',
                                        blank=True,
                                        null=True,
                                        max_length=5,
                                        choices=SECOND_LE,
                                        default=None,
                                        help_text='平素带下情况-色')
    leucorrhea_feature = models.CharField(verbose_name='平素带下情况-质',
                                          blank=True,
                                          null=True,
                                          max_length=10,
                                          choices=THIRD_LE,
                                          default=None,
                                          help_text='平素带下情况-质')

    MARRIAGE_BOX = (
        ('未婚无性生活', '未婚无性生活'),
        ('未婚有性生活', '未婚有性生活'),
        ('已婚同居', '已婚同居'),
        ('已婚分居', '已婚分居'),
        ('离婚', '离婚'),
        ('丧偶', '丧偶')
    )
    marriage = models.CharField(verbose_name='婚姻史',
                                help_text='婚姻史',
                                blank=True,
                                choices=MARRIAGE_BOX,
                                null=True,
                                default=None,
                                max_length=6)

    # 孕产史
    pastpreg_yuncount = models.CharField(verbose_name=u'孕次总数',
                                         blank=True,
                                         null=True,
                                         max_length=3,
                                         help_text="孕次总数")
    pastpreg_shunchan = models.CharField(verbose_name=u'顺产次数',
                                         blank=True,
                                         null=True,
                                         max_length=3,
                                         help_text="顺产次数")
    pastpreg_pougong = models.CharField(verbose_name=u'剖宫产次数',
                                        blank=True,
                                        null=True,
                                        max_length=3,
                                        help_text="剖宫产次数")
    pastpreg_yaoliu = models.CharField(verbose_name=u'药物流产次数',
                                       blank=True,
                                       null=True,
                                       max_length=3,
                                       help_text="药物流产次数")
    pastpreg_renliu = models.CharField(verbose_name=u'人工流产次数',
                                       blank=True,
                                       null=True,
                                       max_length=3,
                                       help_text="人工流产次数")
    pastpreg_ziranliu = models.CharField(verbose_name=u'自然流产次数',
                                         blank=True,
                                         null=True,
                                         max_length=3,
                                         help_text="自然流产次数")
    pastpreg_zaochan = models.CharField(verbose_name=u'早产次数',
                                        blank=True,
                                        null=True,
                                        max_length=3,
                                        help_text="早产次数")
    pastpreg_yiweirenshen = models.CharField(verbose_name=u'异位妊娠次数',
                                             blank=True,
                                             null=True,
                                             max_length=3,
                                             help_text="异位妊娠次数")
    pastpreg_qinggongshu = models.CharField(verbose_name=u'清宫术次数',
                                            blank=True,
                                            null=True,
                                            max_length=3,
                                            help_text="清宫术次数")
    pastpreg_qita = models.CharField(verbose_name='其他',
                                     blank=True,
                                     null=True,
                                     default=None,
                                     max_length=100,
                                     help_text='其他')

    # 避孕措施
    prevent_wu = models.BooleanField(verbose_name='无',
                                     blank=True,
                                     default=False,
                                     help_text="无")
    prevent_jieza = models.BooleanField(verbose_name='结扎',
                                        blank=True,
                                        default=False,
                                        help_text="结扎")
    prevent_jieyuqi = models.BooleanField(verbose_name='宫内节育器',
                                          blank=True,
                                          default=False,
                                          help_text="宫内节育器")
    prevent_biyuntao = models.BooleanField(verbose_name='避孕套',
                                           blank=True,
                                           default=False,
                                           help_text="避孕套")
    prevent_biyunyao = models.BooleanField(verbose_name='口服避孕药',
                                           blank=True,
                                           default=False,
                                           help_text="口服避孕药")
    # 家族史
    WOMB_BLOOD = (
        ('无', '无'),
        ('有', '有'),
        ('不详', '不详')
    )
    OVULATION = (
        ('否', '否'),
        ('是', '是'),
        ('不详', '不详')
    )
    pastfamily_womb_blood = models.CharField(choices=WOMB_BLOOD,
                                             verbose_name=u'一级亲属(母亲、姐妹、女儿)异常子宫出血史',
                                             null=True,
                                             blank=True,
                                             default=None,
                                             max_length=2,
                                             help_text="一级亲属(母亲、姐妹、女儿)异常子宫出血史")
    pastfamily_ovulation = models.CharField(verbose_name=u'是否为排卵障碍性',
                                            null=True,
                                            blank=True,
                                            default=None,
                                            choices=OVULATION,
                                            max_length=2,
                                            help_text="是否为排卵障碍性")
    #  一级亲属（父母、兄弟姐妹、子女）其他疾病史
    # pastfamily_xinzangbing = models.BooleanField(verbose_name=u'甲状腺相关疾病',
    #                                              blank=True,
    #                                              default=False,
    #                                              help_text="甲状腺相关疾病")
    pastfamily_minus = models.BooleanField(verbose_name=u'甲减',
                                           blank=True,
                                           default=False,
                                           help_text="甲减")
    pastfamily_plus = models.BooleanField(verbose_name=u'甲亢',
                                          blank=True,
                                          default=False,
                                          help_text="甲亢")
    pastfamily_duonangluanchao = models.BooleanField(verbose_name=u'多囊卵巢综合征',
                                                     blank=True,
                                                     default=False,
                                                     help_text="多囊卵巢综合征")
    pastfamily_tangniaobing = models.BooleanField(verbose_name=u'糖尿病',
                                                  blank=True,
                                                  default=False,
                                                  help_text="糖尿病")
    pastfamily_buxiang = models.BooleanField(verbose_name=u'不详',
                                             blank=True,
                                             default=False,
                                             help_text="不详")
    pastfamily_qita = models.CharField(verbose_name=u'其它',
                                       null=True,
                                       max_length=100,
                                       default=None,
                                       help_text="其他")

    class Meta:
        abstract = True
