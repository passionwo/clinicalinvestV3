from .basemenstruation import BaseMenstruation, models
from .geninfo import GeneralInfo


class Summary(BaseMenstruation):
    person = models.OneToOneField(
        GeneralInfo, related_name='summary', on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              related_name='my_summary',
                              null=True,
                              blank=True,
                              on_delete=models.CASCADE)

    # 主诉
    owner_buguize = models.DecimalField(verbose_name='阴道不规则流血(天)',
                                        help_text='阴道不规则流血(天)',
                                        default=None,
                                        null=True,
                                        max_digits=4,
                                        decimal_places=0,
                                        blank=True, )
    owner_suoduan = models.DecimalField(verbose_name='月经周期缩短(月)',
                                        help_text='月经周期缩短(月)',
                                        default=None,
                                        null=True,
                                        max_digits=4,
                                        decimal_places=0,
                                        blank=True, )
    owner_yanchang = models.DecimalField(verbose_name='行经时间延长(月)',
                                         help_text='行经时间延长(月)',
                                         default=None,
                                         null=True,
                                         max_digits=4,
                                         decimal_places=0,
                                         blank=True, )
    owner_liangduo = models.DecimalField(verbose_name='月经量多(月)',
                                         help_text='月经量多(月)',
                                         default=None,
                                         null=True,
                                         max_digits=4,
                                         decimal_places=0,
                                         blank=True, )
    owner_pailuan = models.DecimalField(verbose_name='排卵期出血(月)',
                                        help_text='排卵期出血(月)',
                                        default=None,
                                        null=True,
                                        max_digits=4,
                                        decimal_places=0,
                                        blank=True, )
    owner_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)

    # 出血量
    blood_cond = models.CharField(verbose_name=u'出血所需卫生巾数',
                                  choices=BaseMenstruation.S_BLOOD_COND,
                                  max_length=20,
                                  blank=True,
                                  null=True,
                                  help_text="出血所需卫生巾数")
    ss_blood_character_qita = models.CharField(verbose_name='其他',
                                               help_text='其他',
                                               default=None,
                                               null=True,
                                               blank=True,
                                               max_length=200)
    ss_blood_cond_qita = models.CharField(verbose_name='其他',
                                          help_text='其他',
                                          default=None,
                                          null=True,
                                          blank=True,
                                          max_length=200)

    ss_blood_color_qita = models.CharField(verbose_name='其他',
                                           help_text='其他',
                                           default=None,
                                           null=True,
                                           blank=True,
                                           max_length=200)
    ss_blood_block_qita = models.CharField(verbose_name='其他',
                                           help_text='其他',
                                           default=None,
                                           null=True,
                                           blank=True,
                                           max_length=200)

    blood_character = models.CharField(verbose_name=u'出血特点',
                                       choices=BaseMenstruation.S_BLOOD_CHARACTER,
                                       max_length=6,
                                       blank=True,
                                       null=True,
                                       help_text="出血特点")
    # 精神情绪
    spirit_jinglichongpei = models.BooleanField(verbose_name=u'精力充沛',
                                                default=False,
                                                blank=True,
                                                help_text="精力充沛")
    spirit_shenpifali = models.BooleanField(verbose_name=u'神疲乏力',
                                            default=False,
                                            blank=True,
                                            help_text="神疲乏力")
    spirit_jianwang = models.BooleanField(verbose_name=u'健忘',
                                          default=False,
                                          blank=True,
                                          help_text="健忘")
    spirit_yalida = models.BooleanField(verbose_name=u'压力大',
                                        default=False,
                                        blank=True,
                                        help_text="压力大")

    spirit_jiaolv = models.BooleanField(verbose_name=u'焦虑',
                                        default=False,
                                        blank=True,
                                        help_text="焦虑")
    spirit_yiyu = models.BooleanField(verbose_name=u'抑郁',
                                      default=False,
                                      blank=True,
                                      help_text="抑郁")
    spirit_xinu = models.BooleanField(verbose_name=u'喜怒无常',
                                      default=False,
                                      blank=True,
                                      help_text="喜怒无常")
    spirit_yinu = models.BooleanField(verbose_name=u'烦躁易怒',
                                      default=False,
                                      blank=True,
                                      help_text="烦躁易怒")
    spirit_beishangyuku = models.BooleanField(verbose_name=u'悲伤欲哭',
                                              default=False,
                                              blank=True,
                                              help_text="悲伤欲哭")
    spirit_qita = models.CharField(verbose_name='其他',
                                   help_text='其他',
                                   default=None,
                                   null=True,
                                   blank=True,
                                   max_length=200)
    # 寒热及汗出
    sweat_zhengchang = models.BooleanField(verbose_name=u'正常',
                                           default=False,
                                           blank=True,
                                           help_text="正常")
    sweat_weihan = models.BooleanField(verbose_name=u'畏寒',
                                       default=False,
                                       blank=True,
                                       help_text="畏寒")
    sweat_weire = models.BooleanField(verbose_name=u'畏热',
                                      default=False,
                                      blank=True,
                                      help_text="畏热")
    sweat_wuxin = models.BooleanField(verbose_name=u'五心烦热',
                                      default=False,
                                      blank=True,
                                      help_text="五心烦热")
    sweat_chaore = models.BooleanField(verbose_name=u'潮热',
                                       default=False,
                                       blank=True,
                                       help_text="潮热")
    sweat_dire = models.BooleanField(verbose_name=u'低热不退',
                                     default=False,
                                     blank=True,
                                     help_text="低热不退")
    sweat_dongze = models.BooleanField(verbose_name=u'动则汗出',
                                       default=False,
                                       blank=True,
                                       help_text="动则汗出")
    sweat_yewo = models.BooleanField(verbose_name=u'夜卧汗出,醒后汗止',
                                     default=False,
                                     blank=True,
                                     help_text="夜卧汗出,醒后汗止")
    sweat_hongre = models.BooleanField(verbose_name=u'烘热汗出',
                                       default=False,
                                       blank=True,
                                       help_text="烘热汗出")
    sweat_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)
    # 面色
    face_zhengchang = models.BooleanField(verbose_name=u'正常',
                                          default=False,
                                          blank=True,
                                          help_text="正常")
    face_danbaiwuhua = models.BooleanField(verbose_name=u'淡白无华',
                                           default=False,
                                           blank=True,
                                           help_text="淡白无华")
    face_cangbai = models.BooleanField(verbose_name=u'苍白',
                                       default=False,
                                       blank=True,
                                       help_text="苍白")
    face_qingbai = models.BooleanField(verbose_name=u'青白',
                                       default=False,
                                       blank=True,
                                       help_text="青白")
    face_baierfuzhong = models.BooleanField(verbose_name=u'白而浮肿',
                                            blank=True,
                                            default=False,
                                            help_text="白而浮肿")
    face_weihuang = models.BooleanField(verbose_name=u'萎黄',
                                        default=False,
                                        blank=True,
                                        help_text="萎黄")
    face_huangzhong = models.BooleanField(verbose_name=u'黄肿',
                                          default=False,
                                          blank=True,
                                          help_text="黄肿")
    face_chaohong = models.BooleanField(verbose_name=u'潮红',
                                        default=False,
                                        blank=True,
                                        help_text="潮红")
    face_huian = models.BooleanField(verbose_name=u'晦黯',
                                     default=False,
                                     blank=True,
                                     help_text="晦黯")
    face_mianmulihei = models.BooleanField(verbose_name=u'面目黧黑',
                                           blank=True,
                                           default=False,
                                           help_text="面目黧黑")
    face_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 default=None,
                                 null=True,
                                 blank=True,
                                 max_length=200)

    # 头面部
    head_zhengchang = models.BooleanField(verbose_name=u'正常',
                                          blank=True,
                                          default=False,
                                          help_text="正常")
    head_touyun = models.BooleanField(verbose_name=u'头晕',
                                      blank=True,
                                      default=False,
                                      help_text="头晕")
    head_toutong = models.BooleanField(verbose_name=u'头痛',
                                       blank=True,
                                       default=False,
                                       help_text="头痛")
    head_touchenzhong = models.BooleanField(verbose_name=u'头沉重',
                                            blank=True,
                                            default=False,
                                            help_text="头沉重")
    eyes_muxuan = models.BooleanField(verbose_name=u'目眩',
                                      blank=True,
                                      default=False,
                                      help_text="目眩")
    eyes_muse = models.BooleanField(verbose_name=u'目涩',
                                    blank=True,
                                    default=False,
                                    help_text="目涩")
    eyes_yanhua = models.BooleanField(verbose_name=u'眼花',
                                      blank=True,
                                      default=False,
                                      help_text="眼花")
    eyes_muyang = models.BooleanField(verbose_name=u'目痒',
                                      blank=True,
                                      default=False,
                                      help_text="目痒")
    ear_erming = models.BooleanField(verbose_name=u'耳鸣',
                                     blank=True,
                                     default=False,
                                     help_text="耳鸣")
    ear_erlong = models.BooleanField(verbose_name=u'耳聋',
                                     blank=True,
                                     default=False,
                                     help_text="耳聋")
    ear_chongfu = models.BooleanField(verbose_name=u'声音重复',
                                      blank=True,
                                      default=False,
                                      help_text="声音重复")
    ear_xiajiang = models.BooleanField(verbose_name=u'听力下降',
                                       blank=True,
                                       default=False,
                                       help_text="听力下降")
    eyes_chenqifz = models.BooleanField(verbose_name=u'晨起眼睑浮肿',
                                        blank=True,
                                        default=False,
                                        help_text="晨起眼睑浮肿")
    e_qita = models.CharField(verbose_name='其他',
                              help_text='其他',
                              default=None,
                              null=True,
                              blank=True,
                              max_length=200)
    # 口咽部
    throat_yantong = models.BooleanField(verbose_name=u'咽痛',
                                         blank=True,
                                         default=False,
                                         help_text="咽痛")
    throat_yanyang = models.BooleanField(verbose_name=u'咽痒',
                                         blank=True,
                                         default=False,
                                         help_text="烟痒")
    throat_yiwugan = models.BooleanField(verbose_name=u'咽部异物感',
                                         blank=True,
                                         default=False,
                                         help_text="咽部异物感")
    breath_wuyiwei = models.BooleanField(verbose_name=u'口中无异味',
                                         blank=True,
                                         default=False,
                                         help_text="口中无异味")
    breath_kouku = models.BooleanField(verbose_name=u'口苦',
                                       blank=True,
                                       default=False,
                                       help_text="口苦")
    breath_kougan = models.BooleanField(verbose_name=u'口干',
                                        blank=True,
                                        default=False,
                                        help_text="口干")
    breath_kounian = models.BooleanField(verbose_name=u'口粘',
                                         blank=True,
                                         default=False,
                                         help_text="口粘")
    breath_buyuyan = models.BooleanField(verbose_name=u'口干不欲饮',
                                         blank=True,
                                         default=False,
                                         help_text="口干不欲饮")
    breath_qita = models.CharField(verbose_name='其他',
                                   help_text='其他',
                                   default=None,
                                   null=True,
                                   blank=True,
                                   max_length=200)
    # 胸胁及语音
    sound_zhengchang = models.BooleanField(verbose_name=u'正常',
                                           default=False,
                                           blank=True,
                                           help_text="正常")
    sound_xinhuang = models.BooleanField(verbose_name=u'心慌',
                                         default=False,
                                         blank=True,
                                         help_text="心慌")
    sound_qiduan = models.BooleanField(verbose_name=u'气短',
                                       default=False,
                                       blank=True,
                                       help_text="气短")
    breast_zhangmen = models.BooleanField(verbose_name=u'胸胁胀闷',
                                          blank=True,
                                          default=False,
                                          help_text="胸胁胀闷")
    breast_citong = models.BooleanField(verbose_name=u'胸胁刺痛',
                                        blank=True,
                                        default=False,
                                        help_text="胸胁刺痛")
    breast_yintong = models.BooleanField(verbose_name=u'胸胁隐痛',
                                         blank=True,
                                         default=False,
                                         help_text="胸胁隐痛")
    breast_biezhang = models.BooleanField(verbose_name=u'乳房憋胀',
                                          blank=True,
                                          default=False,
                                          help_text="乳房憋胀")
    bre_citong = models.BooleanField(verbose_name=u'乳房刺痛',
                                     default=False,
                                     blank=True,
                                     help_text="乳房刺痛")
    bre_zhangtong = models.BooleanField(verbose_name=u'乳房胀痛',
                                        default=False,
                                        blank=True,
                                        help_text="乳房胀痛")
    sound_xitanxi = models.BooleanField(verbose_name=u'喜叹息',
                                        default=False,
                                        blank=True,
                                        help_text="喜叹息")
    sound_shaoqi = models.BooleanField(verbose_name=u'少气懒言',
                                       default=False,
                                       blank=True,
                                       help_text="少气懒言")
    s_qita = models.CharField(verbose_name='其他',
                              help_text='其他',
                              default=None,
                              null=True,
                              blank=True,
                              max_length=200)
    # 腹腰
    stomach_zhengchang = models.BooleanField(verbose_name=u'正常',
                                             blank=True,
                                             default=False,
                                             help_text="正常")
    stomach_zhangtongjuan = models.BooleanField(verbose_name=u'小腹胀痛拒按',
                                                blank=True,
                                                default=False,
                                                help_text="小腹胀痛拒按")
    stomach_yintongxian = models.BooleanField(verbose_name=u'小腹隐痛喜按',
                                              blank=True,
                                              default=False,
                                              help_text="小腹隐痛喜按")
    stomach_xiaofuzhuizhang = models.BooleanField(verbose_name=u'小腹下坠感',
                                                  blank=True,
                                                  default=False,
                                                  help_text="小腹下坠感")
    stomach_xiaofubiezhang = models.BooleanField(verbose_name=u'小腹憋胀',
                                                 blank=True,
                                                 default=False,
                                                 help_text="小腹憋胀")
    stomach_zhuotong = models.BooleanField(verbose_name=u'小腹灼痛',
                                           blank=True,
                                           default=False,
                                           help_text="小腹灼痛")
    stomach_xiaofulengtong = models.BooleanField(verbose_name=u'小腹冷痛',
                                                 blank=True,
                                                 default=False,
                                                 help_text="小腹冷痛")
    stomach_xiaofucitong = models.BooleanField(verbose_name=u'小腹刺痛',
                                               blank=True,
                                               default=False,
                                               help_text="小腹刺痛")
    stomach_yaosuan = models.BooleanField(verbose_name=u'腰酸痛',
                                          blank=True,
                                          default=False,
                                          help_text="腰酸痛")
    stomach_yaoleng = models.BooleanField(verbose_name=u'腰冷痛',
                                          blank=True,
                                          default=False,
                                          help_text="腰冷痛")
    stomach_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    null=True,
                                    blank=True,
                                    max_length=200)
    # 四肢
    limb_zhengchang = models.BooleanField(verbose_name=u'正常',
                                          blank=True,
                                          default=False,
                                          help_text="正常")
    limb_wuli = models.BooleanField(verbose_name=u'无力',
                                    blank=True,
                                    default=False,
                                    help_text="无力")
    limb_mamu = models.BooleanField(verbose_name=u'麻木',
                                    blank=True,
                                    default=False,
                                    help_text="麻木")
    limb_kunzhong = models.BooleanField(verbose_name=u'困重',
                                        blank=True,
                                        default=False,
                                        help_text="困重")
    limb_zhileng = models.BooleanField(verbose_name=u'肢冷',
                                       blank=True,
                                       default=False,
                                       help_text="肢冷")
    limb_fuzhong = models.BooleanField(verbose_name=u'浮肿',
                                       blank=True,
                                       default=False,
                                       help_text="浮肿")
    limb_szxinre = models.BooleanField(verbose_name=u'手足心热',
                                       blank=True,
                                       default=False,
                                       help_text="手足心热")
    limb_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 default=None,
                                 null=True,
                                 blank=True,
                                 max_length=200)
    # 饮食
    # 全身症状-饮食
    diet_zhengchang = models.BooleanField(verbose_name=u'正常',
                                          blank=True,
                                          default=False,
                                          help_text="正常")
    diet_nadaishishao = models.BooleanField(verbose_name=u'食欲不振',
                                            blank=True,
                                            default=False,
                                            help_text="食欲不振")
    diet_shiyuws = models.BooleanField(verbose_name=u'多食易饥',
                                       blank=True,
                                       default=False,
                                       help_text="多食易饥")
    diet_xireyin = models.BooleanField(verbose_name=u'喜热饮',
                                       blank=True,
                                       default=False,
                                       help_text="喜热饮")
    diet_xilengyin = models.BooleanField(verbose_name=u'喜冷饮',
                                         blank=True,
                                         default=False,
                                         help_text="喜冷饮")
    diet_shixinla = models.BooleanField(verbose_name=u'喜辛辣',
                                        blank=True,
                                        default=False,
                                        help_text="喜辛辣")
    diet_bushu = models.BooleanField(verbose_name=u'食后胃脘不舒',
                                     blank=True,
                                     default=False,
                                     help_text="食后胃脘不舒")
    diet_qita = models.CharField(verbose_name='其他',
                                 help_text='其他',
                                 default=None,
                                 null=True,
                                 blank=True,
                                 max_length=200)
    # 睡眠
    # 全身症状-睡眠
    sleep_zhengchang = models.BooleanField(verbose_name=u'正常',
                                           blank=True,
                                           default=False,
                                           help_text="正常")
    sleep_duomeng = models.BooleanField(verbose_name=u'多梦',
                                        blank=True,
                                        default=False,
                                        help_text="多梦")
    sleep_yixing = models.BooleanField(verbose_name=u'易醒',
                                       blank=True,
                                       default=False,
                                       help_text="易醒")
    sleep_nanyirumian = models.BooleanField(verbose_name=u'难以入眠',
                                            blank=True,
                                            default=False,
                                            help_text="难以入眠")
    sleep_cheyebumian = models.BooleanField(verbose_name=u'彻夜不眠',
                                            blank=True,
                                            default=False,
                                            help_text="彻夜不眠")
    sleep_shishui = models.BooleanField(verbose_name=u'嗜睡',
                                        blank=True,
                                        default=False,
                                        help_text="嗜睡")
    sleep_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)
    # 性欲
    sex_zhengchang = models.BooleanField(verbose_name=u'正常',
                                         blank=True,
                                         default=False,
                                         help_text="正常")
    sex_xywangsheng = models.BooleanField(verbose_name=u'性欲旺盛',
                                          blank=True,
                                          default=False,
                                          help_text="性欲旺盛")
    sex_xyjiantui = models.BooleanField(verbose_name=u'性欲减退',
                                        blank=True,
                                        default=False,
                                        help_text="性欲减退")
    # 大便
    stool_zhengchang = models.BooleanField(verbose_name=u'正常',
                                           blank=True,
                                           default=False,
                                           help_text="正常")

    stool_biangan = models.BooleanField(verbose_name=u'便干',
                                        blank=True,
                                        default=False,
                                        help_text="便干")
    stool_zhixi = models.BooleanField(verbose_name=u'质稀',
                                      blank=True,
                                      default=False,
                                      help_text="质稀")
    stool_sgsx = models.BooleanField(verbose_name=u'时干时稀',
                                     blank=True,
                                     default=False,
                                     help_text="时干时稀")
    stool_xiexie = models.BooleanField(verbose_name=u'泄泻',
                                       blank=True,
                                       default=False,
                                       help_text="泄泻")
    stool_tlzqxiexie = models.BooleanField(verbose_name=u'天亮前泄泻',
                                           blank=True,
                                           default=False,
                                           help_text="天亮前泄泻")
    stool_zhinian = models.BooleanField(verbose_name=u'质黏,排不尽',
                                        blank=True,
                                        default=False,
                                        help_text="质黏,排不尽")
    stool_weixiaohua = models.BooleanField(verbose_name=u'夹杂未消化食物',
                                           blank=True,
                                           default=False,
                                           help_text="夹杂未消化食物")
    stool_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)
    # 小便
    urine_zhengchang = models.BooleanField(verbose_name=u'正常',
                                           blank=True,
                                           default=False,
                                           help_text="正常")
    urine_duanhuang = models.BooleanField(verbose_name=u'短黄',
                                          blank=True,
                                          default=False,
                                          help_text="短黄")
    urine_qingchang = models.BooleanField(verbose_name=u'清长',
                                          blank=True,
                                          default=False,
                                          help_text="清长")
    urine_xbpinshu = models.BooleanField(verbose_name=u'频数',
                                         blank=True,
                                         default=False,
                                         help_text="频数")
    urine_niaoji = models.BooleanField(verbose_name=u'尿急',
                                       blank=True,
                                       default=False,
                                       help_text="尿急")
    urine_niaotong = models.BooleanField(verbose_name=u'尿痛',
                                         blank=True,
                                         default=False,
                                         help_text="尿痛")
    urine_shaoniao = models.BooleanField(verbose_name=u'少尿',
                                         blank=True,
                                         default=False,
                                         help_text="少尿")
    urine_yulibujin = models.BooleanField(verbose_name=u'余沥不尽',
                                          blank=True,
                                          default=False,
                                          help_text="余力不尽")
    urine_yeniaopin = models.BooleanField(verbose_name=u'夜尿频',
                                          blank=True,
                                          default=False,
                                          help_text="夜尿频")
    urine_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)
    # 舌质
    texture_danhong = models.BooleanField(verbose_name=u'淡红',
                                          blank=True,
                                          default=False,
                                          help_text="淡红")
    texture_danbai = models.BooleanField(verbose_name=u'淡白',
                                         blank=True,
                                         default=False,
                                         help_text="淡白")
    texture_pianhong = models.BooleanField(verbose_name=u'鲜红',
                                           blank=True,
                                           default=False,
                                           help_text="鲜红")
    texture_shenhong = models.BooleanField(verbose_name=u'深红',
                                           blank=True,
                                           default=False,
                                           help_text="深红")
    texture_zihong = models.BooleanField(verbose_name=u'紫红',
                                         blank=True,
                                         default=False,
                                         help_text="紫红")
    texture_anhong = models.BooleanField(verbose_name=u'黯红',
                                         blank=True,
                                         default=False,
                                         help_text="黯红")
    texture_danan = models.BooleanField(verbose_name=u'淡黯',
                                        blank=True,
                                        default=False,
                                        help_text="淡黯")
    texture_zian = models.BooleanField(verbose_name=u'紫黯',
                                       blank=True,
                                       default=False,
                                       help_text="紫黯")
    texture_yudian = models.BooleanField(verbose_name=u'有瘀点或瘀斑',
                                         blank=True,
                                         default=False,
                                         help_text="有淤点或淤斑")
    texture_jianhong = models.BooleanField(verbose_name=u'舌边尖红',
                                           blank=True,
                                           default=False,
                                           help_text="舌边尖红")
    texture_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    null=True,
                                    blank=True,
                                    max_length=200)

    # 舌苔
    coating_bai = models.BooleanField(verbose_name=u'白',
                                      blank=True,
                                      default=False,
                                      help_text="白")
    coating_huang = models.BooleanField(verbose_name=u'黄',
                                        blank=True,
                                        default=False,
                                        help_text="黄")
    coating_bo = models.BooleanField(verbose_name=u'薄',
                                     blank=True,
                                     default=False,
                                     help_text="薄")
    coating_hou = models.BooleanField(verbose_name=u'厚',
                                      blank=True,
                                      default=False,
                                      help_text="厚")
    coating_ni = models.BooleanField(verbose_name=u'腻',
                                     blank=True,
                                     default=False,
                                     help_text="腻")
    coating_run = models.BooleanField(verbose_name=u'润',
                                      blank=True,
                                      default=False,
                                      help_text="润")
    coating_hua = models.BooleanField(verbose_name=u'滑',
                                      blank=True,
                                      default=False,
                                      help_text="滑")
    coating_gan = models.BooleanField(verbose_name=u'干',
                                      blank=True,
                                      default=False,
                                      help_text="干")
    coating_shaotai = models.BooleanField(verbose_name=u'少苔',
                                          blank=True,
                                          default=False,
                                          help_text="少苔")
    coating_huabo = models.BooleanField(verbose_name=u'花剥',
                                        blank=True,
                                        default=False,
                                        help_text="花剥")
    coating_wutai = models.BooleanField(verbose_name=u'无苔',
                                        blank=True,
                                        default=False,
                                        help_text="无苔")
    coating_qita = models.CharField(verbose_name='其他',
                                    help_text='其他',
                                    default=None,
                                    null=True,
                                    blank=True,
                                    max_length=200)
    # 舌体
    tongue_zhengchang = models.BooleanField(verbose_name=u'正常',
                                            blank=True,
                                            default=False,
                                            help_text="正常")
    tongue_shouxiao = models.BooleanField(verbose_name=u'瘦小',
                                          blank=True,
                                          default=False,
                                          help_text="瘦小")
    tongue_pangda = models.BooleanField(verbose_name=u'胖大',
                                        blank=True,
                                        default=False,
                                        help_text="胖大")
    tongue_youchihen = models.BooleanField(verbose_name=u'有齿痕',
                                           blank=True,
                                           default=False,
                                           help_text="有齿痕")
    tongue_zhongyouliewen = models.BooleanField(verbose_name=u'有裂纹',
                                                blank=True,
                                                default=False,
                                                help_text="有裂纹")
    tongue_qita = models.CharField(verbose_name='其他',
                                   help_text='其他',
                                   default=None,
                                   null=True,
                                   blank=True,
                                   max_length=200)
    # 脉象
    pulse_fu = models.BooleanField(verbose_name=u'浮',
                                   blank=True,
                                   default=False, help_text="浮")
    pulse_chen = models.BooleanField(verbose_name=u'沉',
                                     blank=True,
                                     default=False, help_text="沉")
    pulse_hua = models.BooleanField(verbose_name=u'滑',
                                    blank=True,
                                    default=False, help_text="滑")
    pulse_shu = models.BooleanField(verbose_name=u'数',
                                    blank=True,
                                    default=False, help_text="数")
    pulse_xian = models.BooleanField(verbose_name=u'弦',
                                     blank=True,
                                     default=False, help_text="炫")
    pulse_xi = models.BooleanField(verbose_name=u'细',
                                   blank=True,
                                   default=False, help_text="细")
    pulse_ruo = models.BooleanField(verbose_name=u'弱',
                                    blank=True,
                                    default=False, help_text="弱")
    pulse_huan = models.BooleanField(verbose_name=u'缓',
                                     blank=True,
                                     default=False, help_text="缓")
    pulse_chi = models.BooleanField(verbose_name=u'迟',
                                    blank=True,
                                    default=False, help_text="迟")
    pulse_se = models.BooleanField(verbose_name=u'涩',
                                   blank=True,
                                   default=False, help_text="涩")
    pulse_jin = models.BooleanField(verbose_name=u'紧',
                                    blank=True,
                                    default=False, help_text="紧")
    pulse_qita = models.CharField(verbose_name='其他',
                                  help_text='其他',
                                  default=None,
                                  null=True,
                                  blank=True,
                                  max_length=200)

    class Meta:
        verbose_name = '病情概要'
        verbose_name_plural = verbose_name
        ordering = ('-pk', 'person')

    def __str__(self):
        return '%s' % self.pk
