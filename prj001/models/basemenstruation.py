from django.db import models


class BaseMenstruation(models.Model):
    # M_BLOOD_COND = (
    #     (u'出血量中等,每次约需15-20张卫生巾', u'出血量中等,每次约需15-20张卫生巾'),
    #     (u'出血量多,每次多于20张卫生巾', u'出血量多,每次多于20张卫生巾'),
    #     (u'经量少,每次少于5张卫生巾', u'经量少,每次少于5张卫生巾'),
    #     (u'经量极少,几乎不用卫生巾,用护垫即可', u'经量极少,几乎不用卫生巾,用护垫即可'),
    #     (u'其他', u'其他')
    # )
    M_BLOOD_COND = (
        ('≤5张卫生巾(日用)', '≤5张卫生巾(日用)'),
        ('6-10张卫生巾(日用)', '6-10张卫生巾(日用)'),
        ('11-19张卫生巾(日用)', '11-19张卫生巾(日用)'),
        ('≥20张卫生巾(日用或夜用)', '≥20张卫生巾(日用或夜用)'),
        ('几乎不用卫生巾,用护垫即可', '几乎不用卫生巾,用护垫即可'),
    )
    S_BLOOD_COND = (
        (u'出血量中等,每天约需2-4张卫生巾', u'出血量中等,每天约需2-4张卫生巾'),
        (u'出血量多,每天约需5-10张卫生巾', u'出血量多,每天约需5-10张卫生巾'),
        (u'出血量极多,每天多于10张卫生巾', u'出血量极多,每天多于10张卫生巾'),
        (u'经量少,每天少于1张卫生巾', u'经量少,每天少于1张卫生巾'),
        (u'经量极少,几乎不用卫生巾,用护垫即可', u'经量极少,几乎不用卫生巾,用护垫即可'),
        (u'其他', u'其他')
    )
    BLOOD_COLOR = (
        (u'淡红', u'淡红'),
        (u'鲜红', u'鲜红'),
        (u'暗红', u'暗红'),
        (u'紫红', u'紫红'),
        (u'紫黯', u'紫黯'),
        (u'紫黑', u'紫黑'),
        (u'褐色', u'褐色'),
        (u'其他', u'其他'),
    )
    BLOOD_QUALITY = (
        (u'正常', u'正常'),
        (u'粘稠', u'粘稠'),
        (u'清稀', u'清稀')
    )
    BLOOD_BLOCK = (
        (u'无血块', u'无血块'),
        (u'偶有血块', u'偶有血块'),
        (u'夹少量小血块', u'夹少量小血块'),
        (u'夹较大血块', u'夹较大血块'),
        (u'经常出现血块', u'经常出现血块'),
        (u'其他', u'其他')
    )
    M_BLOOD_CHARACTER = (
        (u'顺畅', u'顺畅'),
        (u'势急暴下', u'势急暴下'),
        (u'淋漓不断', u'淋漓不断'),
        (u'点滴即净', u'点滴即净'),
    )
    S_BLOOD_CHARACTER = (
        (u'不畅', u'不畅'),
        (u'势急暴下', u'势急暴下'),
        (u'淋漓不断', u'淋漓不断'),
        (u'点滴即净', u'点滴即净'),
    )

    blood_cond = models.CharField(verbose_name=u'出血所需卫生巾数',
                                  choices=M_BLOOD_COND,
                                  max_length=100,
                                  blank=True,
                                  null=True,
                                  default=None,
                                  help_text="出血所需卫生巾数")

    blood_color = models.CharField(verbose_name=u'出血颜色',
                                   choices=BLOOD_COLOR,
                                   max_length=10,
                                   blank=True,
                                   null=True,
                                   default=None,
                                   help_text="出血颜色")
    blood_quality = models.CharField(verbose_name=u'出血质地(1)',
                                     choices=BLOOD_QUALITY,
                                     max_length=10,
                                     blank=True,
                                     null=True,
                                     default=None,
                                     help_text="出血质地(1)")
    blood_block = models.CharField(verbose_name='出血质地(2)',
                                   choices=BLOOD_BLOCK,
                                   max_length=10,
                                   blank=True,
                                   null=True,
                                   default=None,
                                   help_text='出血质地(2)')
    blood_character = models.CharField(verbose_name=u'出血特点',
                                       choices=M_BLOOD_CHARACTER,
                                       max_length=6,
                                       blank=True,
                                       null=True,
                                       default=None,
                                       help_text="出血特点")

    class Meta:
        abstract = True
