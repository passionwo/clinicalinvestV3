from django.db import models

from django.core.validators import RegexValidator

from prj001.utils.validators_custom import ValidatorModelYear
from datetime import date


# 一般基本信息
class GeneralInfo(models.Model):
    TITLE = (
        (u'主任医师', u'主任医师'),
        (u'副主任医师', u'副主任医师'),
        (u'主治医师', u'主治医师'),
    )
    NATION = (
        (u'汉族', u'汉族'),
        (u'蒙古族', u'蒙古族'),
        (u'回族', u'回族'),
        (u'藏族', u'藏族'),
        (u'维吾尔族', u'维吾尔族'),
        (u'苗族', u'苗族'),
        (u'彝族', u'彝族'),
        (u'壮族', u'壮族'),
        (u'布依族', u'布依族'),
        (u'朝鲜族', u'朝鲜族'),
        (u'满族', u'满族'),
        (u'侗族', u'侗族'),
        (u'瑶族', u'瑶族'),
        (u'白族', u'白族'),
        (u'土家族', u'土家族'),
        (u'哈尼族', u'哈尼族'),
        (u'哈萨克族', u'哈萨克族'),
        (u'傣族', u'傣族'),
        (u'黎族', u'黎族'),
        (u'傈傈族', u'傈傈族'),
        (u'佤族', u'佤族'),
        (u'畲族', u'畲族'),
        (u'高山族', u'高山族'),
        (u'拉祜族', u'拉祜族'),
        (u'水族', u'水族'),
        (u'东乡族', u'东乡族'),
        (u'纳西族', u'纳西族'),
        (u'景颇族', u'景颇族'),
        (u'科尔克孜族', u'科尔克孜族'),
        (u'土族', u'土族'),
        (u'达斡尔族', u'达斡尔族'),
        (u'仫佬族', u'仫佬族'),
        (u'羌族', u'羌族'),
        (u'布朗族', u'布朗族'),
        (u'撒拉族', u'撒拉族'),
        (u'毛难族', u'毛难族'),
        (u'仡佬族', u'仡佬族'),
        (u'锡伯族', u'锡伯族'),
        (u'阿昌族', u'阿昌族'),
        (u'普米族', u'普米族'),
        (u'塔吉克族', u'塔吉克族'),
        (u'怒族', u'怒族'),
        (u'乌孜别克族', u'乌孜别克族'),
        (u'俄罗斯族', u'俄罗斯族'),
        (u'鄂温克族', u'鄂温克族'),
        (u'崩龙族', u'崩龙族'),
        (u'保安族', u'保安族'),
        (u'裕固族', u'裕固族'),
        (u'京族', u'京族'),
        (u'塔塔尔族', u'塔塔尔族'),
        (u'独龙族', u'独龙族'),
        (u'鄂伦春族', u'鄂伦春族'),
        (u'赫哲族', u'赫哲族'),
        (u'门巴族', u'门巴族'),
        (u'珞巴族', u'珞巴族'),
        (u'基诺族', u'基诺族'),
        (u'其他', u'其他'),
    )
    CAREER = (
        (u'学生', u'学生'),
        (u'个体', u'个体'),
        (u'农民', u'农民'),
        (u'军人', u'军人'),
        (u'工人', u'工人'),
        (u'财会人员', u'财会人员'),
        (u'技术人员', u'技术人员'),
        (u'服务业', u'服务业'),
        (u'科教文卫', u'科教文卫'),
        (u'行政管理', u'行政管理'),
        (u'无业', u'无业'),
        (u'其它', u'其它'),
    )
    ENTRANCE = (
        (u'门诊', u'门诊'),
        (u'病房', u'病房'),
    )
    CULTURE = (
        (u'未接受国家教育(文盲)', u'未接受国家教育(文盲)'),
        (u'小学及以下', u'小学及以下'),
        (u'初中', u'初中'),
        (u'高中/中专', u'高中/中专'),
        (u'大专', u'大专'),
        (u'本科', u'本科'),
        (u'研究生及以上', u'研究生及以上'),
    )

    owner = models.ForeignKey('myuser.MyUser',
                              related_name='mygi',
                              on_delete=models.CASCADE)
    serial = models.CharField(verbose_name=u'问卷编码',
                              max_length=13,
                              unique=True,
                              blank=True,
                              help_text="问卷编码")
    name = models.CharField(verbose_name=u'患者姓名',
                            max_length=50,
                            help_text="患者姓名")
    recdate = models.DateField(verbose_name=u'就诊日期',
                               default=date.today,
                               help_text="就诊日期")
    hospital = models.CharField(verbose_name=u'医院名称',
                                max_length=100,
                                help_text="医院名称")
    telephone = models.CharField(verbose_name=u'电话',
                                 max_length=11,
                                 help_text="电话")
    expert = models.CharField(verbose_name=u'填表专家姓名',
                              max_length=50,
                              help_text="填表专家姓名")
    title = models.CharField(verbose_name=u'职称',
                             choices=TITLE,
                             max_length=30,
                             help_text="职称")

    # age = models.IntegerField(verbose_name=u'年龄',
    #                           help_text="年龄")

    birth_year = models.CharField(verbose_name='出生年份',
                                  max_length=4,
                                  validators=[RegexValidator(regex=r'^\d{4}$',
                                                             message='必须为4位数字',
                                                             code='year'),
                                              ValidatorModelYear(4, 'year')],
                                  default=0000,
                                  help_text='出生年份')
    birth_month = models.CharField(verbose_name='出生月份',
                                   max_length=2,
                                   default=00,
                                   validators=[RegexValidator(regex=r'^\d{2}$',
                                                              message='必须为2位数字',
                                                              code='month'),
                                               ValidatorModelYear(2, 'month')],
                                   help_text='出生月份')

    height = models.DecimalField(verbose_name=u'身高cm',
                                 max_digits=4,
                                 default='0',
                                 decimal_places=0,
                                 help_text="身高cm")
    weight = models.DecimalField(verbose_name=u'体重kg',
                                 max_digits=4,
                                 default='0',
                                 decimal_places=1,
                                 help_text="体重kg")
    waistline = models.DecimalField(verbose_name=u'腰围cm',
                                    max_digits=4,
                                    default='0',
                                    decimal_places=1,
                                    help_text="腰围cm")
    hipline = models.DecimalField(verbose_name=u'臀围cm',
                                  max_digits=4,
                                  default='0',
                                  decimal_places=1,
                                  help_text="臀围cm")

    # blood_type = models.CharField(verbose_name=u'血型',
    #                               choices=BLOOD_TYPE,
    #                               max_length=10,
    #                               help_text="血型")
    nation = models.CharField(verbose_name=u'民族',
                              choices=NATION,
                              max_length=25,
                              help_text="民族(汉字)")
    career = models.CharField(verbose_name=u'职业',
                              choices=CAREER,
                              max_length=20,
                              help_text="职业")

    # 一般情况-特殊工作环境
    gaowen = models.BooleanField(verbose_name='高温',
                                 default=False,
                                 help_text='高温', )
    diwen = models.BooleanField(verbose_name=u'低温',
                                default=False,
                                help_text="低温")
    yeban = models.BooleanField(verbose_name='夜班/熬夜',
                                default=False,
                                help_text='夜班/熬夜')
    zaosheng = models.BooleanField(verbose_name=u'噪声',
                                   default=False,
                                   help_text="噪声")
    fushe = models.BooleanField(verbose_name=u'辐射',
                                default=False,
                                help_text="辐射")
    huagongyinran = models.BooleanField(verbose_name=u'化工印染',
                                        default=False,
                                        help_text="化工印染")
    julieyundong = models.BooleanField(verbose_name=u'剧烈运动',
                                       default=False,
                                       help_text="剧烈运动")
    qiyou = models.BooleanField(verbose_name=u'汽油',
                                default=False,
                                help_text="汽油")
    gaokong = models.BooleanField(verbose_name=u'高空',
                                  default=False,
                                  help_text="高空")
    wu = models.BooleanField(verbose_name=u'无',
                             default=False,
                             help_text="无")

    address = models.CharField(verbose_name=u'病人现住址',
                               max_length=150,
                               help_text="病人住址")
    entrance = models.CharField(verbose_name=u'病人来源',
                                choices=ENTRANCE,
                                max_length=10,
                                help_text="病人来源")
    culture = models.CharField(verbose_name=u'文化程度',
                               choices=CULTURE,
                               max_length=30,
                               help_text="文化程度")

    # 一般情况-饮食偏好
    wuteshu = models.BooleanField(
        verbose_name=u'无特殊', default=False, help_text="无特殊")
    sushi = models.BooleanField(
        verbose_name=u'素食', default=False, help_text="素食")
    suan = models.BooleanField(verbose_name=u'酸', default=False, help_text="酸")
    tian = models.BooleanField(verbose_name=u'甜', default=False, help_text="甜")
    xian = models.BooleanField(verbose_name=u'咸', default=False, help_text="咸")
    xinla = models.BooleanField(
        verbose_name=u'辛辣', default=False, help_text="辛辣")
    you = models.BooleanField(verbose_name=u'油', default=False, help_text="油")
    shengleng = models.BooleanField(
        verbose_name=u'生冷', default=False, help_text="生冷")
    kafei = models.BooleanField(
        verbose_name=u'含咖啡因食物或饮品', default=False, help_text="咖啡因")
    qita = models.CharField(verbose_name=u'其它',
                            null=True,
                            blank=True,
                            max_length=50,
                            default=None,
                            help_text="其他")

    degree_of_completion = models.CharField(verbose_name='完成度',
                                            max_length=8,
                                            default='0%',
                                            help_text='完成度')

    CHECKED_CHOICE = (
        ('未审核', '未审核'),
        ('审核通过', '审核通过'),
        ('审核不通过', '审核不通过'),
    )
    is_checked = models.CharField(verbose_name='审核状态',
                                  max_length=5,
                                  choices=CHECKED_CHOICE,
                                  default='未审核',
                                  help_text='审核状态')

    reasons_for_not_passing = models.CharField(verbose_name='审核不通过原因',
                                               max_length=200,
                                               default=None,
                                               help_text='审核不通过原因',
                                               null=True,)

    def __int__(self):
        return self.name

    def __str__(self):
        return '%s' % self.serial

    class Meta:
        verbose_name = u'基本信息'
        ordering = ('-recdate', '-pk')
        permissions = (
            ("prj001_operation", "prj001_self_permissions"),
            ('prj001_patient', 'data_can_be_modified'),
            ('prj001_all', 'prj001_all_permissions'),
        )

        verbose_name_plural = verbose_name
