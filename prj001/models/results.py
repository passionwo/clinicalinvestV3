from django.db import models
from .geninfo import GeneralInfo


class Results(models.Model):
    person = models.OneToOneField(
        GeneralInfo, related_name='results', on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              related_name='my_results',
                              null=True,
                              blank=True,
                              on_delete=models.CASCADE)
    last_result = models.IntegerField(verbose_name='近期疗效',
                                      help_text='近期疗效',
                                      default=None,
                                      null=True,)
    far_result = models.IntegerField(verbose_name='远期疗效(天)',
                                     help_text='远期疗效(天)',
                                     default=None,
                                     null=True,)

    class Meta:
        verbose_name = '疗效情况'
        verbose_name_plural = verbose_name
        ordering = ('-pk', 'person')

    def __str__(self):
        return '%s' % self.pk
