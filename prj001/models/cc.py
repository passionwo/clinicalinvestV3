from django.db import models
from .geninfo import GeneralInfo


class CC(models.Model):
    person = models.OneToOneField(
        GeneralInfo, related_name='cc', on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              null=True,
                              blank=True,
                              related_name='my_cc',
                              on_delete=models.CASCADE)

    # 临床诊断-中医诊断
    YUEJING_CHOICES = (
        ('月经先期', '月经先期'),
        ('月经过多', '月经过多'),
        ('经期延长', '经期延长'),
        ('经间期出血', '经间期出血'),
        ('崩漏', '崩漏')
    )
    # yuejing_zhenduan = models.CharField(verbose_name='中医诊断',
    #                                     help_text='中医诊断',
    #                                     default=None,
    #                                     null=True,
    #                                     choices=YUEJING_CHOICES,
    #                                     max_length=10,
    #                                     blank=True)
    yuejing_xian = models.BooleanField(verbose_name='月经先期',
                                       blank=True,
                                       default=False,
                                       help_text="月经先期")

    yuejing_duo = models.BooleanField(verbose_name='月经过多',
                                      blank=True,
                                      default=False,
                                      help_text="月经过多")
    yuejing_yan = models.BooleanField(verbose_name='经期延长',
                                      blank=True,
                                      default=False,
                                      help_text="经期延长")
    yuejing_chu = models.BooleanField(verbose_name='经间期出血',
                                      blank=True,
                                      default=False,
                                      help_text="经间期出血")
    yuejing_beng = models.BooleanField(verbose_name='崩漏',
                                       blank=True,
                                       default=False,
                                       help_text="崩漏")

    yuejing_qita = models.CharField(verbose_name='其他',
                                    null=True,
                                    blank=True,
                                    default=None,
                                    max_length=150,
                                    help_text='其他')

    # 临床诊断-辩证分型-虚证
    XUZHENG_CHOICES = (
        ('肾气虚证', '肾气虚证'),
        ('肾阴虚证', '肾阴虚证'),
        ('肾阳虚证', '肾阳虚证'),
        ('气虚证', '气虚证'),
        ('血虚证', '血虚证'),
        ('虚热证', '虚热证'),
        ('脾气虚证', '脾气虚证'),
    )
    xuzheng = models.CharField(verbose_name='辩证分型-虚证',
                               help_text='辩证分型-虚证',
                               default=None,
                               null=True,
                               choices=XUZHENG_CHOICES,
                               max_length=10,
                               blank=True)
    qita_asthenic = models.CharField(verbose_name='其它虚证',
                                     max_length=50,
                                     null=True,
                                     blank=True,
                                     default=None,
                                     help_text="其它虚证")

    # 临床诊断-辩证分型-实证
    SHIZHENG_CHOICES = (
        ('肝郁血热证', '肝郁血热证'),
        ('阳盛血热证', '阳盛血热证'),
        ('湿热证', '湿热证'),
        ('痰湿证', '痰湿证'),
        ('肝郁气滞证', '肝郁气滞证'),
        ('血瘀证', '血瘀证'),
        ('实热证', '实热证'),
        ('血寒证', '血寒证'),
    )
    shizheng = models.CharField(verbose_name='辩证分型-实证',
                                help_text='辩证分型-实证',
                                default=None,
                                null=True,
                                choices=SHIZHENG_CHOICES,
                                max_length=5,
                                blank=True)
    qita_demonstration = models.CharField(verbose_name='其它',
                                          blank=True,
                                          null=True,
                                          max_length=50,
                                          default=None,
                                          help_text="其它")

    # 临床诊断-辩证分型-虚实夹杂证
    XUSHI_CHOICES = (
        ('阴虚血热证', '阴虚血热证'),
        # ('阴虚火旺证', '阴虚火旺证'),
    )

    xushi = models.CharField(verbose_name='辩证分型-虚实夹杂证',
                             help_text='辩证分型-虚实夹杂证',
                             default=None,
                             null=True,
                             choices=XUSHI_CHOICES,
                             max_length=5,
                             blank=True)
    qita_def_ex = models.CharField(verbose_name=u'其它',
                                   blank=True,
                                   null=True,
                                   max_length=50,
                                   default=None,
                                   help_text="其它")

    # 临床诊断-西医诊断
    zigongchuxue = models.BooleanField(verbose_name=u'排卵障碍性异常子宫出血',
                                       blank=True,
                                       default=False,
                                       help_text="排卵障碍性异常子宫出血")
    qita_west = models.CharField(verbose_name=u'其它西医诊断',
                                 max_length=50,
                                 null=True,
                                 blank=True,
                                 default=None,
                                 help_text="其它西医诊断")

    class Meta:
        verbose_name = u'临床诊断'
        ordering = ('-pk', 'person')
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s' % self.pk
