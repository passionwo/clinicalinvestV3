from django.db import models
from .geninfo import GeneralInfo


class Relevant(models.Model):
    person = models.OneToOneField(GeneralInfo,
                                  related_name='relevant',
                                  on_delete=models.CASCADE)
    owner = models.ForeignKey('myuser.MyUser',
                              null=True,
                              blank=True,
                              related_name='my_relevant',
                              on_delete=models.CASCADE)

    # 体格检查
    body_wu = models.BooleanField(verbose_name='无',
                                  default=False,
                                  help_text='无')
    menstruation_check = models.BooleanField(verbose_name='妇科检查',
                                             default=False,
                                             help_text='妇科检查')
    body_check = models.BooleanField(verbose_name='全身检查',
                                     default=False,
                                     help_text='全身检查')
    # 辅助检查
    HGB_VALUE = (
        ('不详', '不详'),
        ('>110', '>110'),
        ('91-110', '91-110'),
        ('61-90', '61-90'),
        ('30-60', '30-60'),
    )
    accessory_wu = models.BooleanField(verbose_name='无',
                                       default=False,
                                       help_text='无')
    accessory_chaosheng = models.BooleanField(verbose_name=u'超声检查',
                                              default=False,
                                              help_text="超声检查")
    accessory_quanxuexibaojishu = models.BooleanField(verbose_name=u'全血细胞计数',
                                                      default=False,
                                                      help_text="全血细胞计数")
    accessory_hgb_value = models.CharField(verbose_name=u'血红蛋白值',
                                           choices=HGB_VALUE,
                                           max_length=10,
                                           blank=True,
                                           null=True,
                                           default=None,
                                           help_text="血红蛋白值")
    accessory_ningxue = models.BooleanField(verbose_name=u'凝血功能检查',
                                            default=False,
                                            help_text="凝血功能检查")
    accessory_niaorenshen = models.BooleanField(verbose_name=u'尿妊娠试验或血hCG检测',
                                                default=False,
                                                help_text="尿妊娠试验或血hCG检测")
    accessory_jichutiwen = models.BooleanField(verbose_name=u'基础体温测定(BBT)',
                                               default=False,
                                               help_text="基础体温测定(BBT)")
    accessory_neifenmi = models.BooleanField(verbose_name=u'生殖内分泌测定(激素六项、促甲状腺素水平)',
                                             default=False,
                                             help_text="生殖内分泌测定(激素六项、促甲状腺素水平)")
    accessory_zuzhi = models.BooleanField(verbose_name=u'刮宫或子宫内膜活组织检查',
                                          default=False,
                                          help_text="刮宫或子宫内膜活组织检查")
    accessory_gongqiangjing = models.BooleanField(verbose_name=u'宫腔镜检查',
                                                  default=False,
                                                  help_text="宫腔镜检查")
    accessory_jiejing = models.BooleanField(verbose_name=u'宫颈粘液结晶检查',
                                            default=False,
                                            help_text="宫颈粘液结晶检查")

    accessory_qita = models.CharField(verbose_name=u'其它',
                                      max_length=100,
                                      default=None,
                                      blank=True,
                                      null=True,
                                      help_text="其它")

    class Meta:
        verbose_name = '相关检查'
        verbose_name_plural = verbose_name
        ordering = ('-pk', 'person')

    def __str__(self):
        return '%s' % self.pk
