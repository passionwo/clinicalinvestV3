from rest_framework.filters import SearchFilter
from urllib.parse import unquote


class GenInfoFilter(SearchFilter):

    def get_search_terms(self, request):
        value = request.query_params.get(self.search_param, '')
        params = unquote(value, 'utf-8')
        # print(params)
        return params.replace(',', ' ').split()
