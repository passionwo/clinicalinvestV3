import re


def split_doc(doc_s):
    '''切割__doc__, 返回数据库的字段'''

    pattern_model = re.compile(r'.*[\(](.*)[\)]')

    results = pattern_model.findall(doc_s)

    if results:
        db_str = results[0].split(', ')
    else:
        db_str = None

    return db_str
