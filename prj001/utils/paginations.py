# paginations for views
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from collections import OrderedDict

from django.conf import settings

from myuser.models import MyUser

import math


class ReturenPagination(PageNumberPagination):

    def get_paginated_response(self, data):

        per_page_num = settings.GEN_PAGE_SIZE

        count = self.page.paginator.count
        try:
            user = MyUser.objects.get(id=self.request.user.id)
        except MyUser.DoesNotExist:
            raise ValueError('用户未登录')

        pages = math.ceil(count / per_page_num)

        return Response(OrderedDict([
            ('total_pages', pages),
            ('code', user.verification_code),
            ('is_admin', user.has_perm('prj001.prj001_all')),
            ('count', count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
