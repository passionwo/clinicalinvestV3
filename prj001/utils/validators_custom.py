from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError

from datetime import date


@deconstructible
class ValidatorModelYear(BaseValidator):
    ''' To validate 'birth_year' & 'birth_month' '''
    message = '填写的年份不符合要求'
    code = 'year_msg'

    def __init__(self, num, types):
        self.num = num
        self.tps = types

    def __call__(self, value):
        try:
            value_ = int(value)
        except Exception:
            raise ValidationError('必须为数字类型')

        if self.tps == 'year':
            if not (value_ > 1900 and value_ < date.today().year):
                raise ValidationError(self.message)
        else:
            if not (value_ > 0 and value_ < 13):
                raise ValidationError(self.message)
