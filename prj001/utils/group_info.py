from prj001.models.geninfo import GeneralInfo


def get_group_info(sf=None, obj=None, type=None, request=None):
    '''返回同组数据'''
    if not type:
        of_groups = sf.request.user.groups.all()
    else:
        of_groups = request.user.groups.all()
    users = [group.user_set.all() for group in of_groups]
    gen_objects = None

    _ID = set()
    for u in users:
        for user in u:
            _ID.add(user.id)

    if not _ID:
        _ID.add(sf.request.user.id)

    if not type:
        gen_objects = obj.objects.filter(owner__in=_ID).order_by('-serial')
    return (gen_objects, _ID)


def get_all_group_info(obj=None, is_not_info=None):
    '''返回所有的信息(包括不同组)'''
    if is_not_info and obj:
        return obj.objects.all().order_by('-pk')
    if obj:
        return obj.objects.all().order_by('-serial')
    return GeneralInfo.objects.all().order_by('-serial')
