upload_file_type = ('xlsx', 'xls')

excel_sheet_names = [
    '一般情况',
    '就诊时病情概要',
    '病史',
    '相关检查',
    '临床诊断',
    '治疗',
    '疗效',
    '病情概要',
    '病史1',
    '单选',
    '一般情况-',
    '治疗及检查',
    '诊断'
]

table_contain_qita = ('info', 'cure', 'summary', 'cc', 'relevant', 'history')

info_table_of_info = []

cure_table_of_qita = []

cc_table_of_qita = []

relevant_table_of_qita = []

history_table_of_info = []

true_change_bool = {
    'face_head_cangbai': '面色苍白',
    'face_head_huangbai': '面色晄白',
    'face_head_weihuang': '面色萎黄',
    'face_head_huian': '面色晦暗',
    'face_head_anban': '面有黯斑',
    'face_head_zhizhong': '面浮肢肿',
    'face_head_chunhong': '颧赤唇红',
    'belly_juan': '疼痛拒按',
    'belly_xian': '腹痛喜按',
    'belly_deretongjian': '腹痛得热痛减',
    'belly_tongjian': '血块下痛减',
}

summary_menstruation = {
    'blood_cond': 's_blood_cond',
    'blood_color': 's_blood_color',
    'blood_quality': 's_blood_quality',
    'blood_block': 's_blood_block',
    'blood_character': 's_blood_character',
}

history_table = {
    'diet_qita': 'h_diet_qita',
    'sleep_qita': 'h_sleep_qita',
    'spirit_yinu': 'h_spirit_yinu',
    'spirit_qita': 'h_spirit_qita',
}

font_size = '12'
normal_font_size = '12'
header_border = 1

excel_title_style = {
    'info': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#91b684',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'summary': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#538ac5',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'history': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#f5b964',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'relevant': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#c3624e',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'cc': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#7db2dc',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'cure': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#91b684',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'results': {
        'font_color': 'white',
        'border': header_border,
        'bold': True,
        'fg_color': '#538ac5',
        'font_size': font_size,
        'align': 'center',
        'valign': 'vcenter',
    },
    'normal': {
        'bold': False,
        'font_size': normal_font_size,
        'align': 'center',
        'valign': 'vcenter',
    }
}
