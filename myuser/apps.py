from django.apps import AppConfig

VERBOSE_APP_NAME = u"帐户管理"


class MyUserConfig(AppConfig):
    name = 'myuser'
    verbose_name = VERBOSE_APP_NAME
