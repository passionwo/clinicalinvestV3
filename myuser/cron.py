from .models import MyUser
from datetime import date

import random


constant_sign = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def random_sign():
    sign = ''.join(random.sample(constant_sign, k=4))
    return sign


def update_user_code():
    print('%s-%s-%s---begin---' % (date.today().year, date.today().month, date.today().day))

    users = MyUser.objects.all()

    for user in users:
        user.verification_code = random_sign()
        user.save()

    print('%s-%s-%s---end---' % (date.today().year, date.today().month, date.today().day))
