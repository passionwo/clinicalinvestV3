from django.urls import path

from .views import MyUserList, MyUserDetail, UpdatePassword
from .views import UserView, SendEmailView, LoginView
from .views import AreaListView, AreaListOfView

area_api = {
    'post': 'create',
    'put': 'update'
}

area_list = {
    'post': 'create'
}

urlpatterns = [
    path('sendemail/', SendEmailView.as_view()),

    # area
    path('area-list/', AreaListOfView.as_view(area_list), name='area-list-detail'),
    path('area/', AreaListView.as_view(area_api), name='area-detail'),

    path("register/", UserView.as_view(), name="myuser-register"),
    path("login/", LoginView.as_view(), name="myuser-login"),
    path('', MyUserList.as_view(), name='myuser-list'),
    path('<pk>/', MyUserDetail.as_view(), name='myuser-detail'),
    path('<pk>/changepassword/', UpdatePassword.as_view(), name='myuser-changepassword'),

]
