from django.db import models

from myuser.models import MyUser

from django.core.validators import RegexValidator

from utils.validators import ValidatorModelBirth


class CreatTimeModel(models.Model):

    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', help_text='创建时间')

    class Meta:
        abstract = True


# 一般信息 info_owner
class Info(CreatTimeModel):
    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_info')

    CAREER_CHOICE = (
        ('学生', '学生'),
        ('个体', '个体'),
        ('农民', '农民'),
        ('军人', '军人'),
        ('工人', '工人'),
        ('财会人员', '财会人员'),
        ('技术人员', '技术人员'),
        ('服务业', '服务业'),
        ('科教文卫', '科教文卫'),
        ('行政管理', '行政管理'),
        ('无业', '无业'),
        ('其它', '其它'),
    )

    SCORE_CHOICE = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5')
    )

    LEVEL_CHOICE = (
        ('轻', '轻'),
        ('中', '中'),
        ('重', '重')
    )
    serial = models.CharField(unique=True, max_length=12, validators=[RegexValidator(regex=r'^\d{12}$', message='必须为12位数字', code='questionnaire'), ], verbose_name='问卷编码', help_text='问卷编码')

    name = models.CharField(max_length=30, verbose_name='姓名', help_text='姓名')

    phone = models.CharField(max_length=11, validators=[
        RegexValidator(regex=r'^\d{11}$', message='必须为11位数字', code='phone')
    ], verbose_name='手机号码', help_text='手机号码')

    hospital = models.CharField(max_length=30, verbose_name='所在医院', help_text='所在医院')

    birth = models.CharField(max_length=7, verbose_name='出生[YYYY-MM]', validators=[
        RegexValidator(regex=r'^\d{4}-\d{2}$', message='必须符合 [YYYY-MM] 格式', code='birth'),
        ValidatorModelBirth(7)
    ], help_text="出生日期[YYYY-MM]")

    nation = models.CharField(max_length=25, blank=True, null=True, default=None, verbose_name='民族', help_text='民族')

    career = models.CharField(max_length=25, blank=True, null=True, choices=CAREER_CHOICE, verbose_name='职业', help_text='职业')

    special_gaowen = models.BooleanField(blank=True, null=True, default=None, verbose_name='高温', help_text='高温')

    special_diwen = models.BooleanField(blank=True, null=True, default=None, verbose_name='低温', help_text='低温')

    special_yeban = models.BooleanField(blank=True, null=True, default=None, verbose_name='夜班/熬夜', help_text='夜班/熬夜')

    special_zao = models.BooleanField(blank=True, null=True, default=None, verbose_name='噪声', help_text='噪声')

    special_fu = models.BooleanField(blank=True, null=True, default=None, verbose_name='辐射', help_text='辐射')

    special_hua = models.BooleanField(blank=True, null=True, default=None, verbose_name='化工印染', help_text='化工印染')

    special_ju = models.BooleanField(blank=True, null=True, default=None, verbose_name='剧烈运动', help_text='剧烈运动')

    special_qi = models.BooleanField(blank=True, null=True, default=None, verbose_name='汽油', help_text='汽油')

    special_kong = models.BooleanField(blank=True, null=True, default=None, verbose_name='高空', help_text='高空')

    special_wu = models.BooleanField(blank=True, null=True, default=None, verbose_name='无', help_text='无')

    address = models.CharField(max_length=100, blank=True, null=True, verbose_name='病人现住址', help_text='病人现住址')

    culture = models.CharField(max_length=25, blank=True, null=True, verbose_name='文化程度', help_text='文化程度')

    yinshi_wuteshu = models.BooleanField(blank=True, null=True, default=None, verbose_name='无特殊', help_text='无特殊')

    yinshi_sushi = models.BooleanField(blank=True, null=True, default=None, verbose_name='素食', help_text='素食')

    yinshi_suan = models.BooleanField(blank=True, null=True, default=None, verbose_name='酸', help_text='酸')

    yinshi_xian = models.BooleanField(blank=True, null=True, default=None, verbose_name='咸', help_text='咸')

    yinshi_xinla = models.BooleanField(blank=True, null=True, default=None, verbose_name='辛辣', help_text='辛辣')

    yinshi_you = models.BooleanField(blank=True, null=True, default=None, verbose_name='油', help_text='油')

    yinshi_shengleng = models.BooleanField(blank=True, null=True, default=None, verbose_name='生冷', help_text='生冷')

    yinshi_cafei = models.BooleanField(blank=True, null=True, default=None, verbose_name='含咖啡因食物或饮品', help_text='含咖啡因食物或饮品')

    yinshi_qita = models.CharField(max_length=100, blank=True, null=True, verbose_name='其他', help_text='其他')

    height = models.IntegerField(blank=True, null=True, verbose_name='身高', help_text='身高')

    weight = models.IntegerField(blank=True, null=True, verbose_name='体重', help_text='体重')

    bmi = models.CharField(max_length=10, blank=True, null=True, verbose_name='体重指数', help_text='体重指数')

    waistline = models.IntegerField(blank=True, null=True, verbose_name='腰围', help_text='腰围')

    hipline = models.IntegerField(blank=True, null=True, verbose_name='臀围', help_text='臀围')

    whr = models.CharField(max_length=10, blank=True, null=True, verbose_name='腰臀比', help_text='腰臀比')

    hairy = models.IntegerField(blank=True, null=True, verbose_name='多毛评分', help_text='多毛评分')

    acne = models.BooleanField(blank=True, null=True, default=None, verbose_name='痤疮', help_text='痤疮')

    acne_part = models.CharField(blank=True, null=True, max_length=20, verbose_name='痤疮部位', help_text='痤疮部位')

    acne_score = models.CharField(blank=True, null=True, max_length=1, choices=SCORE_CHOICE, verbose_name='评分', help_text='评分')

    glandula = models.BooleanField(blank=True, null=True, default=None, verbose_name='皮脂腺分泌过旺', help_text='皮脂腺分泌过旺')

    glandula_part = models.CharField(blank=True, null=True, max_length=20, verbose_name='分泌过旺具体部位', help_text='分泌过旺具体部位')

    glandula_level = models.CharField(blank=True, null=True, choices=LEVEL_CHOICE, max_length=5, verbose_name='分泌过旺程度', help_text='分泌过旺程度')

    male = models.BooleanField(blank=True, null=True, default=None, verbose_name='雄性脱发', help_text='雄性脱发')

    male_part = models.CharField(blank=True, null=True, max_length=20, verbose_name='雄性脱发具体部位', help_text='雄性脱发具体部位')

    male_level = models.CharField(blank=True, null=True, choices=LEVEL_CHOICE, max_length=5, verbose_name='雄性脱发程度', help_text='雄性脱发程度')

    degree_of_completion = models.CharField(blank=True, null=True, verbose_name='完成度', max_length=8, help_text='完成度')

    CHECKED_CHOICE = (
        ('未审核', '未审核'),
        ('审核通过', '审核通过'),
        ('审核不通过', '审核不通过'),
    )
    check_status = models.CharField(blank=True, null=True, verbose_name='审核状态', max_length=6, choices=CHECKED_CHOICE, default='未审核', help_text='审核状态')

    reason_for_check = models.CharField(blank=True, null=True, verbose_name='审核不通过的原因', max_length=200, help_text='审核不通过的原因')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '一般情况'
        verbose_name_plural = verbose_name
        permissions = [
            ('prj002_all_permissions', 'Prj002 all permissions.'),
            ('prj002_other_permissions', 'Prj002 user permissions.')
        ]


# 病情概要 summary_owner
class Summary(CreatTimeModel):

    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='summary')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_summary')

    LEVEL_CHOICE = (
        ('无', '无'),
        ('轻', '轻'),
        ('中', '中'),
        ('重', '重')
    )

    face_head = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    face_head_tou = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='头晕头胀', help_text='头晕头胀')

    face_head_er = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='耳鸣', help_text='耳鸣')

    face_head_muxuan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='目眩眼花', help_text='目眩眼花')

    face_head_muse = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='目涩干痒', help_text='目涩干痒')

    face_head_zhong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='晨起眼睑浮肿', help_text='晨起眼睑浮肿')

    face_color = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    face_color_bai = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面白浮肿', help_text='面白浮肿')

    face_color_an = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面色晦暗', help_text='面色晦暗')

    face_color_huang = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面色萎黄', help_text='面色萎黄')

    face_color_dan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面色淡白无华', help_text='面色淡白无华')

    face_color_hong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面色潮红', help_text='面色潮红')

    face_color_hei = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面色黧黑', help_text='面色黧黑')

    face_color_chi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='面红唇赤', help_text='面红唇赤')

    mouth = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')
    mouth_chi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='齿松脱发', help_text='齿松脱发')

    mouth_gan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='口干不欲饮', help_text='口干不欲饮')

    mouth_nian = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='口黏', help_text='口黏')

    mouth_ku = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='口苦咽干', help_text='口苦咽干')

    mouth_throat = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='咽痛咽痒', help_text='咽痛咽痒')

    mouth_yi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='咽部异物感', help_text='咽部异物感')

    spirit = models.BooleanField(blank=True, null=True, default=None, verbose_name='精力充沛', help_text='精力充沛')

    spirit_kun = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='困倦嗜睡', help_text='困倦嗜睡')

    spirit_pi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='神疲乏力', help_text='神疲乏力')

    spirit_shao = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='少气懒言', help_text='少气懒言')

    spirit_jian = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='健忘', help_text='健忘')

    spirit_jiao = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='焦虑抑郁', help_text='焦虑抑郁')

    spirit_xi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='喜怒无常', help_text='喜怒无常')

    spirit_bei = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='悲伤欲哭', help_text='悲伤欲哭')

    limbs = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    limbs_wu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='无力', help_text='无力')

    limbs_ma = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='麻木', help_text='麻木')

    limbs_shou = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='手足心热', help_text='手足心热')

    limbs_zhi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='肢体困重', help_text='肢体困重')

    limbs_wei = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='畏寒肢冷', help_text='畏寒肢冷')

    body = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    body_zhong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='肢体浮肿', help_text='肢体浮肿')

    body_fei = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='形体肥胖', help_text='形体肥胖')

    body_shou = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='形体瘦弱', help_text='形体瘦弱')

    chest = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    chest_huang = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='心慌心悸', help_text='心慌心悸')

    chest_men = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='胸闷气短', help_text='胸闷气短')

    chest_exin = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='胸闷气短', help_text='胸闷气短')

    chest_xiong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='胸胁胀痛或刺痛', help_text='胸胁胀痛或刺痛')

    chest_ru = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='乳房胀痛或刺痛', help_text='乳房胀痛或刺痛')

    chest_yi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='溢乳', help_text='溢乳')

    chest_suan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='嗳气反酸', help_text='嗳气反酸')

    chest_tan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='痰涎', help_text='痰涎')

    cold_hot = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    cold_hot_wei = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='畏寒', help_text='畏寒')

    cold_hot_hong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='烘热汗出', help_text='烘热汗出')

    cold_hot_wu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='五心烦热', help_text='五心烦热')

    cold_hot_shi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='失眠盗汗', help_text='失眠盗汗')

    cold_hot_di = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='低热不退', help_text='低热不退')

    cold_hot_chao = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='潮热颧红', help_text='潮热颧红')

    cold_hot_dong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='动则汗出', help_text='动则汗出')

    cold_hot_dao = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='盗汗', help_text='盗汗')

    waist = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    waist_zhang = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='脘腹胀满', help_text='脘腹胀满')

    waist_juan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小腹胀痛拒按', help_text='小腹胀痛拒按')

    waist_xian = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小腹隐痛喜按', help_text='小腹隐痛喜按')

    waist_zhui = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小腹有下坠感', help_text='小腹有下坠感')

    waist_fuleng = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小腹冷痛', help_text='小腹冷痛')

    waist_fuci = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小腹刺痛', help_text='小腹刺痛')

    waist_yaosuan = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='腰酸痛', help_text='腰酸痛')

    waist_yaoleng = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='腰冷痛', help_text='腰冷痛')

    sleep = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    sleep_yi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='易醒', help_text='易醒')

    sleep_shi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='嗜睡', help_text='嗜睡')

    sleep_duo = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='多梦', help_text='多梦')

    sleep_mian = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='失眠', help_text='失眠')

    diet = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    diet_shiyu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='食欲不振', help_text='食欲不振')

    diet_duoshi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='多食易饥', help_text='多食易饥')

    diet_xire = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='喜热饮', help_text='喜热饮')

    diet_xileng = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='喜冷饮', help_text='喜冷饮')

    diet_xixin = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='喜辛辣', help_text='喜辛辣')

    diet_bushu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='食后胃脘不舒', help_text='食后胃脘不舒')

    diet_shishao = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='食少纳呆', help_text='食少纳呆')

    skin = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    skin_xihsu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='毛发稀疏', help_text='毛发稀疏')

    skin_nong = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='毛发浓密', help_text='毛发浓密')

    skin_cu = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='皮肤粗糙', help_text='皮肤粗糙')

    skin_zhi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='汗粘油脂多', help_text='汗粘油脂多')

    skin_re = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='身热不扬', help_text='身热不扬')

    skin_jia = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='肌肤甲错', help_text='肌肤甲错')

    pee = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    pee_pin = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='频数', help_text='频数')

    pee_ji = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='尿急尿痛', help_text='尿急尿痛')

    pee_ye = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='夜尿频多', help_text='夜尿频多')

    pee_li = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='余沥不尽', help_text='余沥不尽')

    pee_qing = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小便清长', help_text='小便清长')

    pee_chi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='小便黄赤', help_text='小便黄赤')

    feces = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    feces_tang = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='大便溏', help_text='大便溏')

    feces_mi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='大便秘结', help_text='大便秘结')

    feces_xi = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='时干时稀', help_text='时干时稀')

    feces_xie = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='天亮前泄泻', help_text='天亮前泄泻')

    feces_nian = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='大便黏腻', help_text='大便黏腻')

    feces_jia = models.CharField(blank=True, null=True, max_length=1, choices=LEVEL_CHOICE, verbose_name='夹杂未消化食物', help_text='夹杂未消化食物')

    # 舌质
    texture_danhong = models.BooleanField(blank=True, null=True, default=None, verbose_name='淡红', help_text='淡红')

    texture_danbai = models.BooleanField(blank=True, null=True, default=None, verbose_name='淡白', help_text='淡白')

    texture_xianhong = models.BooleanField(blank=True, null=True, default=None, verbose_name='鲜红', help_text='鲜红')

    texture_shenhong = models.BooleanField(blank=True, null=True, default=None, verbose_name='深红', help_text='深红')

    texture_zihong = models.BooleanField(blank=True, null=True, default=None, verbose_name='紫红', help_text='紫红')

    texture_anhong = models.BooleanField(blank=True, null=True, default=None, verbose_name='黯红', help_text='黯红')

    texture_danan = models.BooleanField(blank=True, null=True, default=None, verbose_name='淡黯', help_text='淡黯')

    texture_zian = models.BooleanField(blank=True, null=True, default=None, verbose_name='紫黯', help_text='紫黯')

    texture_yudian = models.BooleanField(blank=True, null=True, default=None, verbose_name='有瘀点或瘀斑', help_text='有瘀点或瘀斑')

    texture_jianhong = models.BooleanField(blank=True, null=True, default=None, verbose_name='舌边尖红', help_text='舌边尖红')

    texture_qita = models.CharField(blank=True, null=True, max_length=30, verbose_name='其他', help_text='其他')
    # 舌苔
    coating_bai = models.BooleanField(blank=True, null=True, default=None, verbose_name='白', help_text='白')

    coating_huang = models.BooleanField(blank=True, null=True, default=None, verbose_name='黄', help_text='黄')

    coating_bo = models.BooleanField(blank=True, null=True, default=None, verbose_name='薄', help_text='薄')

    coating_hou = models.BooleanField(blank=True, null=True, default=None, verbose_name='厚', help_text='厚')

    coating_ni = models.BooleanField(blank=True, null=True, default=None, verbose_name='腻', help_text='腻')

    coating_run = models.BooleanField(blank=True, null=True, default=None, verbose_name='润', help_text='润')

    coating_hua = models.BooleanField(blank=True, null=True, default=None, verbose_name='滑', help_text='滑')

    coating_gan = models.BooleanField(blank=True, null=True, default=None, verbose_name='干', help_text='干')

    coating_shaotai = models.BooleanField(blank=True, null=True, default=None, verbose_name='少苔', help_text='少苔')

    coating_huabo = models.BooleanField(blank=True, null=True, default=None, verbose_name='花剥', help_text='花剥')

    coating_wutai = models.BooleanField(blank=True, null=True, default=None, verbose_name='无苔', help_text='无苔')

    coating_qita = models.CharField(blank=True, null=True, max_length=30, verbose_name='其他', help_text='其他')
    # 舌体
    tongue_zhengchang = models.BooleanField(blank=True, null=True, default=None, verbose_name='正常', help_text='正常')

    tongue_shouxiao = models.BooleanField(blank=True, null=True, default=None, verbose_name='瘦小', help_text='瘦小')

    tongue_pangda = models.BooleanField(blank=True, null=True, default=None, verbose_name='胖大', help_text='胖大')

    tongue_youchihen = models.BooleanField(blank=True, null=True, default=None, verbose_name='有齿痕', help_text='有齿痕')

    tongue_youliewen = models.BooleanField(blank=True, null=True, default=None, verbose_name='有裂纹', help_text='有裂纹')

    tongue_qita = models.CharField(blank=True, null=True, max_length=30, verbose_name='其他', help_text='其他')
    # 脉象
    pulse_fu = models.BooleanField(blank=True, null=True, default=None, verbose_name='浮', help_text='浮')

    pulse_chen = models.BooleanField(blank=True, null=True, default=None, verbose_name='沉', help_text='沉')

    pulse_hua = models.BooleanField(blank=True, null=True, default=None, verbose_name='滑', help_text='滑')

    pulse_shu = models.BooleanField(blank=True, null=True, default=None, verbose_name='数', help_text='数')

    pulse_xian = models.BooleanField(blank=True, null=True, default=None, verbose_name='弦', help_text='弦')

    pulse_xi = models.BooleanField(blank=True, null=True, default=None, verbose_name='细', help_text='细')

    pulse_ruo = models.BooleanField(blank=True, null=True, default=None, verbose_name='弱', help_text='弱')

    pulse_huan = models.BooleanField(blank=True, null=True, default=None, verbose_name='缓', help_text='缓')

    pulse_chi = models.BooleanField(blank=True, null=True, default=None, verbose_name='迟', help_text='迟')

    pulse_se = models.BooleanField(blank=True, null=True, default=None, verbose_name='涩', help_text='涩')

    pulse_jin = models.BooleanField(blank=True, null=True, default=None, verbose_name='紧', help_text='紧')

    pulse_qita = models.CharField(blank=True, null=True, max_length=30, verbose_name='其他', help_text='其他')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '病情概要'
        verbose_name_plural = verbose_name


# 专科病史 history_owner
class History(CreatTimeModel):

    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='history')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_history')

    FIRST_CHOICE = (
        ('10岁以前', '10岁以前'),
        ('11岁以后', '11岁以后'),
        ('14岁以后', '14岁以后'),
        ('16岁以后', '16岁以后')
    )
    NORMAL_AGE = (
        ('21-25天', '21-25天'),
        ('26-30天', '26-30天'),
        ('31-35天', '31-35天')
    )
    ABNORMAL = (
        ('或1月多次', '或1月多次'),
        ('1-2个月1行', '1-2个月1行'),
        ('2-3个月1行', '2-3个月1行'),
        ('3-4个月1行', '3-4个月1行'),
        ('4-6个月1行', '4-6个月1行'),
        ('>6个月1行', '>6个月1行'),
    )
    CYCLICITY_SUM = (
        ('≤2天', '≤2天'),
        ('3-7天', '3-7天'),
        ('7天以上甚至半月', '7天以上甚至半月'),
    )
    BLOOD_COND = (
        ('≤5张卫生巾(日用)', '≤5张卫生巾(日用)'),
        ('6-10张卫生巾(日用)', '6-10张卫生巾(日用)'),
        ('11-19张卫生巾(日用)', '11-19张卫生巾(日用)'),
        ('≥20张卫生巾(日用或夜用)', '≥20张卫生巾(日用或夜用)'),
        ('几乎不用卫生巾,用护垫即可', '几乎不用卫生巾,用护垫即可'),
    )
    BLOOD_COLOR = (
        ('淡红', '淡红'),
        ('鲜红', '鲜红'),
        ('暗红', '暗红'),
        ('紫红', '紫红'),
        ('紫黯', '紫黯'),
        ('紫黑', '紫黑'),
        ('褐色', '褐色'),
        ('其他', '其他'),
    )
    BLOOD_QUALITY = (
        ('正常', '正常'),
        ('粘稠', '粘稠'),
        ('清稀', '清稀')
    )
    BLOOD_BLOCK = (
        ('无血块', '无血块'),
        ('偶有血块', '偶有血块'),
        ('夹少量小血块', '夹少量小血块'),
        ('夹较大血块', '夹较大血块'),
        ('经常出现血块', '经常出现血块'),
        ('其他', '其他')
    )
    BLOOD_CHARACTER = (
        ('顺畅', '顺畅'),
        ('势急暴下', '势急暴下'),
        ('淋漓不断', '淋漓不断'),
        ('点滴即净', '点滴即净'),
    )
    FIRST_LE = (
        ('带下量正常', '带下量正常'),
        ('带下量少', '带下量少'),
        ('带下量多', '带下量多')
    )
    SECOND_LE = (
        ('带下透明', '带下透明'),
        ('带下色黄', '带下色黄'),
        ('带下色白', '带下色白'),
        ('带下色黄绿', '带下色黄绿'),
        ('其他', '其他')
    )
    THIRD_LE = (
        ('带下质黏而不稠', '带下质黏而不稠'),
        ('带下质清稀', '带下质清稀'),
        ('带下质稠', '带下质稠')
    )
    MARRIAGE_BOX = (
        ('未婚无性生活', '未婚无性生活'),
        ('未婚有性生活', '未婚有性生活'),
        ('已婚同居', '已婚同居'),
        ('已婚分居', '已婚分居'),
        ('离婚', '离婚'),
        ('丧偶', '丧偶')
    )

    first_time = models.CharField(blank=True, null=True, max_length=10, choices=FIRST_CHOICE, verbose_name='月经初潮年龄', help_text='月经初潮年龄')

    first_time_qita = models.CharField(null=True, blank=True, verbose_name='其他', help_text='其他', max_length=100)

    is_normal = models.BooleanField(blank=True, null=True, default=None, verbose_name='是否规律', help_text='是否规律')

    normal = models.CharField(blank=True, null=True, choices=NORMAL_AGE, max_length=30, help_text="月经周期尚规律的间隔天数", verbose_name='月经周期尚规律的间隔天数',)

    abnormal = models.CharField(choices=ABNORMAL, max_length=30, blank=True, null=True, verbose_name=u'月经不规律的情况', help_text="月经不规律的情况")

    cyclicity_sum = models.CharField(choices=CYCLICITY_SUM, max_length=30, blank=True, null=True, help_text="行经天数", verbose_name='行经天数')

    cyclicity_sum_qita = models.CharField(null=True, blank=True, max_length=30, verbose_name='其他', help_text='其他')

    blood_cond = models.CharField(verbose_name=u'出血所需卫生巾数', choices=BLOOD_COND, max_length=30, blank=True, null=True, help_text="出血所需卫生巾数")

    blood_cond_qita = models.CharField(default=None, null=True, blank=True, max_length=30, verbose_name='出血量-其他', help_text='出血量-其他')

    blood_color = models.CharField(verbose_name=u'出血颜色', choices=BLOOD_COLOR, max_length=30, blank=True, null=True, help_text="出血颜色")

    blood_color_qita = models.CharField(default=None, null=True, blank=True, max_length=30, verbose_name='出血颜色-其他', help_text='出血颜色-其他')

    blood_quality = models.CharField(verbose_name=u'出血质地(1)', choices=BLOOD_QUALITY, max_length=30, blank=True, null=True, help_text="出血质地(1)")

    blood_block = models.CharField(verbose_name='出血质地(2)', choices=BLOOD_BLOCK, max_length=30, blank=True, null=True, help_text='出血质地(2)')

    blood_character = models.CharField(verbose_name=u'出血特点', choices=BLOOD_CHARACTER, max_length=30, blank=True, null=True, help_text="出血特点")

    menstruation_is_accompany = models.BooleanField(null=True, default=None, blank=True, verbose_name='经期是否伴随症状', help_text='经期是否伴随症状')

    last_time = models.DateField(null=True, default=None, blank=True, verbose_name='末次行经日期', help_text='末次行经日期')

    leucorrhea_quantity = models.CharField(blank=True, null=True, choices=FIRST_LE, max_length=30, verbose_name='平素带下情况-量', help_text='平素带下情况-量')

    leucorrhea_color = models.CharField(blank=True, null=True, max_length=30, choices=SECOND_LE, verbose_name='平素带下情况-色', help_text='平素带下情况-色')

    leucorrhea_color_qita = models.CharField(default=None, null=True, blank=True, max_length=30, verbose_name='平素带下情况颜色-其他', help_text='出血颜色-其他')

    leucorrhea_feature = models.CharField(blank=True, null=True, max_length=30, choices=THIRD_LE, verbose_name='平素带下情况-质', help_text='平素带下情况-质')

    marriage = models.CharField(verbose_name='婚姻史', help_text='婚姻史', blank=True, choices=MARRIAGE_BOX, null=True, default=None, max_length=6)

    pastpreg_yuncount = models.IntegerField(verbose_name=u'孕次总数', blank=True, null=True, help_text="孕次总数")

    pastpreg_shunchan = models.IntegerField(verbose_name=u'顺产次数', blank=True, null=True, help_text="顺产次数")

    pastpreg_pougong = models.IntegerField(verbose_name=u'剖宫产次数', blank=True, null=True, help_text="剖宫产次数")

    pastpreg_zaochan = models.IntegerField(verbose_name=u'早产次数', blank=True, null=True, help_text="早产次数")

    pastpreg_yaoliu = models.IntegerField(verbose_name=u'药物流产次数', blank=True, null=True, help_text="药物流产次数")

    pastpreg_renliu = models.IntegerField(verbose_name=u'人工流产次数', blank=True, null=True, help_text="人工流产次数")

    pastpreg_ziranliu = models.IntegerField(verbose_name=u'自然流产次数', blank=True, null=True, help_text="自然流产次数")

    pastpreg_yiweirenshen = models.IntegerField(verbose_name=u'异位妊娠次数', blank=True, null=True, help_text="异位妊娠次数")

    pastpreg_qinggongshu = models.IntegerField(verbose_name=u'清宫术次数', blank=True, null=True, help_text="清宫术次数")

    pastpreg_qita = models.CharField(verbose_name='其他', blank=True, null=True, default=None, max_length=100, help_text='其他')

    prevent_wu = models.BooleanField(verbose_name='无', null=True, blank=True, default=None, help_text="无")

    prevent_jieza = models.BooleanField(verbose_name='结扎', null=True, blank=True, default=None, help_text="结扎")

    prevent_jieyuqi = models.BooleanField(verbose_name='宫内节育器', null=True, blank=True, default=None, help_text="宫内节育器")

    prevent_biyuntao = models.BooleanField(verbose_name='避孕套', null=True, blank=True, default=None, help_text="避孕套")

    prevent_biyunyao = models.BooleanField(verbose_name='口服避孕药', null=True, blank=True, default=None, help_text="口服避孕药")

    is_pastfamily_womb = models.BooleanField(null=True, blank=True, verbose_name='是否有疾病史', help_text='是否有疾病史')

    pastfamily_minus = models.BooleanField(blank=True, null=True, default=None, verbose_name='甲减', help_text="甲减")

    pastfamily_plus = models.BooleanField(verbose_name='甲亢', null=True, blank=True, default=None, help_text="甲亢")

    pastfamily_duonangluanchao = models.BooleanField(verbose_name='多囊卵巢综合征', null=True, blank=True, default=None, help_text="多囊卵巢综合征")

    pastfamily_tangniaobing = models.BooleanField(verbose_name=u'糖尿病', null=True, blank=True, default=None, help_text="糖尿病")

    pastfamily_buxiang = models.BooleanField(verbose_name='不详', null=True, blank=True, default=False, help_text="不详")

    pastfamily_qita = models.CharField(verbose_name='其它', null=True, blank=True, max_length=30, default=None, help_text="其他")

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '专科病史'
        verbose_name_plural = verbose_name


# 实验室检查 experiment_owner
class Experiment(CreatTimeModel):

    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='experiment')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_experiment')

    check_gaotong = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='睾酮（T）', help_text='睾酮（T）')

    check_ci = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='雌二醇（E2）', help_text='雌二醇（E2）')

    check_huangti = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='黄体生成素（LH）', help_text='黄体生成素（LH）')

    check_luanpao = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='卵泡刺激素（FSH）', help_text='卵泡刺激素（FSH）')

    check_xue = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='血清泌乳素（PRL）', help_text='血清泌乳素（PRL）')

    check_kangmiao = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='抗苗勒氏测定（AMH）', help_text='抗苗勒氏测定（AMH）')

    check_xuetang = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='空腹血糖（FPG）', help_text='空腹血糖（FPG）')

    check_yidaosu = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='空腹胰岛素（FINS）', help_text='空腹胰岛素（FINS）')

    check_canxue = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后30mins血糖（FPG）', help_text='餐后30mins血糖（FPG）')

    check_canyi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后30mins胰岛素（FINS）', help_text='餐后30mins胰岛素（FINS）')

    check_cantang = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后60mins血糖（FPG）', help_text='餐后60mins血糖（FPG）')

    check_candao = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后60mins胰岛素（FINS）', help_text='餐后60mins胰岛素（FINS）')

    check_canxuetang = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后120mins血糖（FPG）', help_text='餐后120mins血糖（FPG）')

    check_canyidao = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='餐后1200mins胰岛素（FINS）', help_text='餐后1200mins胰岛素（FINS）')

    check_xuezhi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='空腹血脂', help_text='空腹血脂')

    check_gaomizhi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='空腹高密度脂蛋白', help_text='空腹高密度脂蛋白')

    check_dimizhi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name='空腹低密度脂蛋白', help_text='空腹低密度脂蛋白')

    yidaosu_dikang = models.CharField(max_length=10, null=True, blank=True, verbose_name='胰岛素抵抗指数（HOMA-IR）', help_text='胰岛素抵抗指数（HOMA-IR）')

    yidaosu_mingan = models.CharField(max_length=10, null=True, blank=True, verbose_name='胰岛素敏感指数（ISI）', help_text='胰岛素敏感指数（ISI）')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '实验室检查'
        verbose_name_plural = verbose_name


# 经阴道或经肛门B超 bxray_owner
class Bxray(CreatTimeModel):
    XING_CHOICE = (
        ('正常', '正常'),
        ('PCO （窦卵泡个数≥12个）', 'PCO （窦卵泡个数≥12个）')
    )
    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='bxray')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_bxray')

    zigong_chang = models.IntegerField(null=True, blank=True, verbose_name='子宫长径', help_text='子宫长径')

    zigong_kuan = models.IntegerField(null=True, blank=True, verbose_name='子宫宽径', help_text='子宫宽径')

    zigong_qianhou = models.IntegerField(null=True, blank=True, verbose_name='子宫前后径', help_text='子宫前后径')

    zigong_xingtai = models.BooleanField(null=True, blank=True, default=None, verbose_name='子宫形态', help_text='子宫形态')

    zigong_juti = models.CharField(null=True, blank=True, max_length=30, verbose_name='具体形态', help_text='具体形态')

    zigong_neimo = models.IntegerField(null=True, blank=True, verbose_name='子宫内膜厚度', help_text='子宫内膜厚度')

    zigong_is_jiliu = models.BooleanField(null=True, blank=True, default=None, verbose_name='是否存在子宫肌瘤', help_text='是否存在子宫肌瘤')

    zigong_weizhi = models.CharField(null=True, blank=True, max_length=30, verbose_name='子宫肌瘤的位置', help_text='子宫肌瘤的位置')

    zigong_geshu = models.IntegerField(null=True, blank=True, verbose_name='子宫肌瘤的个数', help_text='子宫肌瘤的个数')

    zigong_daxiaochang = models.IntegerField(null=True, blank=True, verbose_name='最大子宫肌瘤的大小-长', help_text='最大子宫肌瘤的大小-长')

    zigong_daxiaokuan = models.IntegerField(null=True, blank=True, verbose_name='最大子宫肌瘤的大小-宽', help_text='最大子宫肌瘤的大小-宽')

    zigong_daxiaogao = models.IntegerField(null=True, blank=True, verbose_name='最大子宫肌瘤的大小-高', help_text='最大子宫肌瘤的大小-高')

    zigong_xainji = models.BooleanField(null=True, blank=True, default=None, verbose_name='是否有子宫腺肌症', help_text='是否有子宫腺肌症')

    zuo_chang = models.IntegerField(null=True, blank=True, verbose_name='左卵巢长径', help_text='左卵巢长径')

    zuo_kuan = models.IntegerField(null=True, blank=True, verbose_name='左卵巢宽径', help_text='左卵巢宽径')

    zuo_qianhou = models.IntegerField(null=True, blank=True, verbose_name='左卵巢前后径', help_text='左卵巢前后径')

    zuo_tiji = models.IntegerField(null=True, blank=True, verbose_name='左卵巢体积', help_text='左卵巢体积')

    zuo_nangzhong = models.BooleanField(null=True, blank=True, verbose_name='左卵巢是否有直径>10mm的囊肿', help_text='左卵巢是否有直径>10mm的囊肿')

    zuo_daxiaochang = models.IntegerField(null=True, blank=True, verbose_name='左卵巢最大囊肿大小-长', help_text='左卵巢最大囊肿大小-长')

    zuo_daxiaokuan = models.IntegerField(null=True, blank=True, verbose_name='左卵巢最大囊肿大小-宽', help_text='左卵巢最大囊肿大小-宽')

    zuo_xingtai = models.CharField(max_length=10, choices=XING_CHOICE, null=True, blank=True, verbose_name='左卵巢形态', help_text='左卵巢形态')

    zuo_paoshu = models.IntegerField(null=True, blank=True, verbose_name='左卵巢窦卵泡数', help_text='左卵巢窦卵泡数')

    you_chang = models.IntegerField(null=True, blank=True, verbose_name='右卵巢长径', help_text='右卵巢长径')

    you_kuan = models.IntegerField(null=True, blank=True, verbose_name='右卵巢宽径', help_text='右卵巢宽径')

    you_qianhou = models.IntegerField(null=True, blank=True, verbose_name='右卵巢前后径', help_text='右卵巢前后径')

    you_tiji = models.IntegerField(null=True, blank=True, verbose_name='右卵巢体积', help_text='右卵巢体积')

    you_nangzhong = models.BooleanField(null=True, blank=True, verbose_name='右卵巢是否有直径>10mm的囊肿', help_text='右卵巢是否有直径>10mm的囊肿')

    you_daxiaochang = models.IntegerField(null=True, blank=True, verbose_name='右卵巢最大囊肿大小-长', help_text='右卵巢最大囊肿大小-长')

    you_daxiaokuan = models.IntegerField(null=True, blank=True, verbose_name='右卵巢最大囊肿大小-宽', help_text='右卵巢最大囊肿大小-宽')

    you_xingtai = models.CharField(max_length=10, choices=XING_CHOICE, null=True, blank=True, verbose_name='右卵巢形态', help_text='右卵巢形态')

    you_paoshu = models.IntegerField(null=True, blank=True, verbose_name='右卵巢窦卵泡数', help_text='右卵巢窦卵泡数')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '经阴道或经肛门B超'
        verbose_name_plural = verbose_name


# 治疗 cure_owner
class Cure(CreatTimeModel):
    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='cure')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_cure')

    is_jiehe = models.BooleanField(null=True, blank=True, default=None, verbose_name='中西医结合治疗', help_text='中西医结合治疗')

    xu_yiqi = models.BooleanField(null=True, blank=True, default=None, verbose_name='益气健脾，养血调经', help_text='益气健脾，养血调经')

    xu_bushen = models.BooleanField(null=True, blank=True, default=None, verbose_name='补肾助阳，温中健脾', help_text='补肾助阳，温中健脾')

    xu_jian = models.BooleanField(null=True, blank=True, default=None, verbose_name='健脾温阳调经', help_text='健脾温阳调经')

    xu_chong = models.BooleanField(null=True, blank=True, default=None, verbose_name='补肾益气，调补冲任', help_text='补肾益气，调补冲任')

    xu_ziyin = models.BooleanField(null=True, blank=True, default=None, verbose_name='滋阴补肾调经', help_text='滋阴补肾调经')

    xu_zhuyang = models.BooleanField(null=True, blank=True, default=None, verbose_name='温肾助阳，调补冲任', help_text='温肾助阳，调补冲任')

    xu_buxue = models.BooleanField(null=True, blank=True, default=None, verbose_name='补血益气调经', help_text='补血益气调经')

    xu_yangyin = models.BooleanField(null=True, blank=True, default=None, verbose_name='养阴清热调经', help_text='养阴清热调经')

    xu_qita = models.CharField(max_length=30, null=True, blank=True, verbose_name='虚症其他', help_text='虚症其他')

    shi_xie = models.BooleanField(null=True, blank=True, default=None, verbose_name='泻肝清热除湿', help_text='泻肝清热除湿')

    shi_tiao = models.BooleanField(null=True, blank=True, default=None, verbose_name='疏肝解郁，理气调冲', help_text='疏肝解郁，理气调冲')

    shi_qing = models.BooleanField(null=True, blank=True, default=None, verbose_name='疏肝解郁，清热调经', help_text='疏肝解郁，清热调经')

    shi_huo = models.BooleanField(null=True, blank=True, default=None, verbose_name='理气化瘀，活血调经', help_text='理气化瘀，活血调经')

    shi_zao = models.BooleanField(null=True, blank=True, default=None, verbose_name='燥湿化痰调经', help_text='燥湿化痰调经')

    shi_xue = models.BooleanField(null=True, blank=True, default=None, verbose_name='活血化瘀调经', help_text='活血化瘀调经')

    shi_tan = models.BooleanField(null=True, blank=True, default=None, verbose_name='化痰祛瘀，活血调经', help_text='化痰祛瘀，活血调经')

    shi_qita = models.CharField(max_length=30, null=True, blank=True, verbose_name='实证其他', help_text='实证其他')

    xushi_qushi = models.BooleanField(null=True, blank=True, default=None, verbose_name='健脾祛湿化痰', help_text='健脾祛湿化痰')

    xushi_yijing = models.BooleanField(null=True, blank=True, default=None, verbose_name='补肾益精，疏肝理气调经', help_text='补肾益精，疏肝理气调经')

    xushi_hua = models.BooleanField(null=True, blank=True, default=None, verbose_name='补肾益精，化痰燥湿', help_text='补肾益精，化痰燥湿')

    xushi_xue = models.BooleanField(null=True, blank=True, default=None, verbose_name='补肾益精，活血化痰调经', help_text='补肾益精，活血化痰调经')

    xushi_ziyin = models.BooleanField(null=True, blank=True, default=None, verbose_name='滋肾清热调经', help_text='滋肾清热调经')

    xushi_qita = models.CharField(max_length=30, null=True, blank=True, verbose_name='虚实夹杂其他', help_text='虚实夹杂其他')

    xuanji = models.CharField(max_length=30, null=True, blank=True, verbose_name='选用方剂', help_text='选用方剂')

    zucheng = models.CharField(max_length=50, null=True, blank=True, verbose_name='具体组成及剂量', help_text='具体组成及剂量')

    zhongcheng = models.BooleanField(null=True, blank=True, default=None, verbose_name='中成药', help_text='中成药')

    zhongcheng_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='中成药-具体药物', help_text='中成药-具体药物')

    gaoxiong = models.BooleanField(null=True, blank=True, default=None, verbose_name='高雄激素血症的治疗', help_text='高雄激素血症的治疗')

    gaoxiong_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='高雄激素血症的治疗-具体药物', help_text='高雄激素血症的治疗-具体药物')

    yidao = models.BooleanField(null=True, blank=True, default=None, verbose_name='高胰岛素血症的治疗', help_text='高胰岛素血症的治疗')

    yidao_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='高胰岛素血症的治疗-具体药物', help_text='高胰岛素血症的治疗-具体药物')

    tiaozhou = models.BooleanField(null=True, blank=True, default=None, verbose_name='调周治疗', help_text='调周治疗')

    tiaozhou_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='调周治疗-具体药物', help_text='调周治疗-具体药物')

    pailuan = models.BooleanField(null=True, blank=True, default=None, verbose_name='促排卵治疗', help_text='促排卵治疗')

    pailuan_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='促排卵治疗-具体药物', help_text='促排卵治疗-具体药物')

    shoushu = models.BooleanField(null=True, blank=True, default=None, verbose_name='手术治疗', help_text='手术治疗')

    shoushu_juti = models.CharField(max_length=50, null=True, blank=True, verbose_name='手术治疗-具体手术方式', help_text='手术治疗-具体手术方式')

    cure_qita = models.CharField(max_length=100, null=True, blank=True, verbose_name='其他治疗', help_text='其他治疗')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '治疗'
        verbose_name_plural = verbose_name


# 临床诊断
class Clinical(CreatTimeModel):
    info = models.OneToOneField(Info, on_delete=models.CASCADE, related_name='clinical')

    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='prj002_clinical')

    zhong_bijing = models.BooleanField(blank=True, null=True, verbose_name='闭经', help_text='闭经')

    zhong_buyun = models.BooleanField(blank=True, null=True, verbose_name='不孕', help_text='不孕')

    zhong_zhengjia = models.BooleanField(blank=True, null=True, verbose_name='癥瘕', help_text='癥瘕')

    zhong_benglou = models.BooleanField(blank=True, null=True, verbose_name='崩漏', help_text='崩漏')

    zhong_shitiao = models.BooleanField(blank=True, null=True, verbose_name='月经失调', help_text='月经失调')

    zhong_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='其他', help_text='其他')

    xu_piqixu = models.BooleanField(blank=True, null=True, verbose_name='脾气虚证', help_text='脾气虚证')

    xu_pishenyangxu = models.BooleanField(blank=True, null=True, verbose_name='脾肾阳虚证', help_text='脾肾阳虚证')

    xu_piyangxu = models.BooleanField(blank=True, null=True, verbose_name='脾阳虚证', help_text='脾阳虚证')

    xu_shenqixu = models.BooleanField(blank=True, null=True, verbose_name='肾气虚证', help_text='肾气虚证')

    xu_shenyinxu = models.BooleanField(blank=True, null=True, verbose_name='肾阴虚证', help_text='肾阴虚证')

    xu_shenyangxu = models.BooleanField(blank=True, null=True, verbose_name='肾阳虚证', help_text='肾阳虚证')

    xu_xuexu = models.BooleanField(blank=True, null=True, verbose_name='血虚证', help_text='血虚证')

    xu_yinxuneire = models.BooleanField(blank=True, null=True, verbose_name='阴虚内热证', help_text='阴虚内热证')

    xu_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='其他', help_text='其他')

    shi_shire = models.BooleanField(blank=True, null=True, verbose_name='肝经湿热证', help_text='肝经湿热证')

    shi_qizhi = models.BooleanField(blank=True, null=True, verbose_name='肝郁气滞证', help_text='肝郁气滞证')

    shi_yure = models.BooleanField(blank=True, null=True, verbose_name='肝经郁热证', help_text='肝经郁热证')

    shi_qizhixueyu = models.BooleanField(blank=True, null=True, verbose_name='气滞血瘀证', help_text='气滞血瘀证')

    shi_tanshi = models.BooleanField(blank=True, null=True, verbose_name='痰湿证', help_text='痰湿证')

    shi_xueyu = models.BooleanField(blank=True, null=True, verbose_name='血瘀证', help_text='血瘀证')

    shi_hujie = models.BooleanField(blank=True, null=True, verbose_name='痰瘀互结证', help_text='痰瘀互结证')

    shi_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='其他', help_text='其他')

    xushi_pixutanshi = models.BooleanField(blank=True, null=True, verbose_name='脾虚痰湿证', help_text='脾虚痰湿证')

    xushi_shenxuganyu = models.BooleanField(blank=True, null=True, verbose_name='肾虚肝郁证', help_text='肾虚肝郁证')

    xushi_shenxutanshi = models.BooleanField(blank=True, null=True, verbose_name='肾虛痰湿证', help_text='肾虛痰湿证')

    xushi_shenxuxueyu = models.BooleanField(blank=True, null=True, verbose_name='肾虚血瘀证', help_text='肾虚血瘀证')

    xushi_shenxuneire = models.BooleanField(blank=True, null=True, verbose_name='肾虚内热证', help_text='肾虚内热证')

    xushi_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='其他', help_text='其他')

    xiyi_duonang = models.BooleanField(blank=True, null=True, verbose_name='多囊卵巢综合征', help_text='多囊卵巢综合征')

    xiyi_qita = models.CharField(max_length=50, blank=True, null=True, verbose_name='其他', help_text='其他')

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = '临床诊断'
        verbose_name_plural = verbose_name
