permission_deny_message = {
    'exist_info': '对不起, 您没有对该记录操作的权限'
}

# for SearchSerializer
search_to_field = [
    'name', 'phone', 'hospital',
    'birth', 'career', 'address'
]

necessary_info = [
    'name', 'phone', 'hospital', 'birth',
    'nation', 'career', 'address', 'culture',
    'height', 'weight', 'waistline', 'hipline'
]
