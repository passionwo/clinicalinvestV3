from django.contrib import admin

from prj002.models import Info, Summary, History, Experiment, Bxray, Cure, Clinical
from prj002.utils.same_code import SCAdmin


@admin.register(Info)
class InfoAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('serial', 'name', 'hospital', 'degree_of_completion', 'check_status', 'create_time', 'owner_link')
    search_fields = ['serial', 'name', 'hospital', 'birth', 'nation', 'check_status']
    list_filter = ['owner']


@admin.register(Summary)
class SummaryAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'face_head', 'face_color', 'mouth', 'spirit', 'create_time', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'first_time', 'normal', 'abnormal', 'create_time', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']


@admin.register(Experiment)
class ExperimentAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'check_gaotong', 'create_time', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']


@admin.register(Bxray)
class BxrayAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'zigong_chang', 'create_time', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']


@admin.register(Cure)
class CureAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'is_jiehe', 'create_time', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']


@admin.register(Clinical)
class ClinicalAdmin(admin.ModelAdmin, SCAdmin):
    list_display = ('id', 'zhong_bijing', 'xu_piqixu', 'shi_shire', 'info_link', 'owner_link')
    search_fields = ['id', 'owner']
    list_filter = ['owner', 'info']
