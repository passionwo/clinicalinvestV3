from django.db.models import Q
from django.core.exceptions import FieldDoesNotExist

from rest_framework.decorators import action
from rest_framework.response import Response

from prj002.utils.same_code import SCView
from prj002.serializers import (
    InfoLinkSerializer, SummaryLinkSerializer,
    HistoryLinkSerializer, CureLinkSerializer,
    ExperimentLinkSerializer, ClinicalSerializer,
    SearchSerializer, BxrayLinkSerializer, CheckStatusSerializer
)
from prj002.models import (
    Info, Summary, History, Experiment, Bxray, Cure, Clinical
)

from functools import reduce

from operator import and_


class SearchView(SCView):
    """
    Override .get_queryset() to get instances
    """
    serializer_class = SearchSerializer

    def get_queryset(self):
        request_data = self.request.data
        search_query = []

        for k, v in request_data.items():
            if not v:
                continue
            try:
                is_exist = Info._meta.get_field(k)
                # a = Info._meta.fields  return a list
            except FieldDoesNotExist:
                continue
            else:
                if is_exist.name:
                    query = '__'.join([is_exist.name, 'icontains'])
                    search_query.append(Q(**{query: v}))
        if search_query:
            srearch_queryset = Info.objects.order_by('-create_time', '-pk').filter(reduce(and_, search_query))
        else:
            srearch_queryset = Info.objects.order_by('-create_time', '-pk')
        self.serializer_class = InfoLinkSerializer
        return srearch_queryset


class InfoView(SCView):
    """
    get([list, retrieve]):
    1. Return all info instance
    2. Return the info instance by id

    post(create):
    Save a info instance and Return it.

    patch(partial_update):
    Update the info instance and Return it.

    delete(destroy):
    Delete the info instance by id.
    """
    serializer_class = InfoLinkSerializer
    queryset = Info.objects.order_by('-create_time', '-serial')

    def get_serializer_class(self):
        if self.action == 'check':
            return CheckStatusSerializer

        return self.serializer_class

    @action(detail=True, methods=['post'])
    def check(self, request, pk, *args, **kwargs):
        info = Info.objects.get(pk=pk)
        data = request.data

        for key, value in data.items():
            if hasattr(info, key):
                setattr(info, key, value)

        info.save()
        # self.get_serializer => check status serializer class
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        # self.serializer_class => info serializer class
        info_serializer = self.serializer_class(info, context={'request': request})
        return Response(info_serializer.data)


class SummaryView(SCView):
    """
    get([list, retrieve]):
    1. Return all summary instance
    2. Return the summary instance by id

    post(create):
    Save a summary instance and Return it.

    patch(partial_update):
    Update the summary instance and Return it.

    delete(destroy):
    Delete the summary instance by id.
    """
    serializer_class = SummaryLinkSerializer
    queryset = Summary.objects.order_by('-create_time', '-pk')


class HistoryView(SCView):
    """
    get([list, retrieve]):
    1. Return all History instance
    2. Return the History instance by id

    post(create):
    Save a History instance and Return it.

    patch(partial_update):
    Update the History instance and Return it.

    delete(destroy):
    Delete the History instance by id.
    """
    serializer_class = HistoryLinkSerializer
    queryset = History.objects.order_by('-create_time', '-pk')


class ExperimentView(SCView):
    """
    get([list, retrieve]):
    1. Return all Experiment instance
    2. Return the Experiment instance by id

    post(create):
    Save a Experiment instance and Return it.

    patch(partial_update):
    Update the Experiment instance and Return it.

    delete(destroy):
    Delete the Experiment instance by id.
    """
    serializer_class = ExperimentLinkSerializer
    queryset = Experiment.objects.order_by('-create_time', '-pk')


class BxrayView(SCView):
    """
    get([list, retrieve]):
    1. Return all bxray instance
    2. Return the bxray instance by id

    post(create):
    Save a bxray instance and Return it.

    patch(partial_update):
    Update the bxray instance and Return it.

    delete(destroy):
    Delete the bxray instance by id.
    """
    serializer_class = BxrayLinkSerializer
    queryset = Bxray.objects.order_by('-create_time', '-pk')


class CureView(SCView):
    """
    get([list, retrieve]):
    1. Return all cure instance
    2. Return the cure instance by id

    post(create):
    Save a cure instance and Return it.

    patch(partial_update):
    Update the cure instance and Return it.

    delete(destroy):
    Delete the cure instance by id.
    """
    serializer_class = CureLinkSerializer
    queryset = Cure.objects.order_by('-create_time', '-pk')


class ClinicalView(SCView):
    """
    get([list, retrieve]):
    1. Return all clinical instance
    2. Return the clinical instance by id

    post(create):
    Save a clinical instance and Return it.

    patch(partial_update):
    Update the clinical instance and Return it.

    delete(destroy):
    Delete the clinical instance by id.
    """
    serializer_class = ClinicalSerializer
    queryset = Clinical.objects.order_by('-create_time', '-pk')
