from django.conf import settings

from rest_framework import serializers

from prj002.models import (
    Info, Summary, Cure, Bxray, History, Experiment, Clinical
)
from prj002.contants import search_to_field

from utils.samecode import ExtendsSerializer, SCSerializer
from utils.generate import create_dikang_mingan


class SearchSerializer(serializers.ModelSerializer):
    """ For search api """
    birth = serializers.CharField(required=False, help_text='出生年份')
    career = serializers.CharField(required=False, help_text='职业')
    name = serializers.CharField(required=False, help_text='姓名')
    phone = serializers.CharField(required=False, help_text='手机号')
    hospital = serializers.CharField(required=False, help_text='所在医院')
    address = serializers.CharField(required=False, help_text='地址')

    class Meta:
        model = Info
        fields = search_to_field


# class InfoSerializer(serializers.ModelSerializer):
#     create_time = serializers.DateTimeField(format=settings.DATETIME_FORMAT, read_only=True)

#     class Meta:
#         model = Info
#         fields = '__all__'
#         read_only_fields = ['owner', 'bmi', 'whr']


class InfoLinkSerializer(ExtendsSerializer, serializers.HyperlinkedModelSerializer, ):
    info = serializers.HyperlinkedIdentityField(read_only=True, view_name='prj002-info-detail', lookup_field='pk',)
    summary = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-summary-detail')
    history = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-history-detail')
    experiment = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-experiment-detail')
    bxray = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-bxray-detail')
    cure = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-cure-detail')
    clinical = serializers.HyperlinkedRelatedField(read_only=True, view_name='prj002-clinical-detail')
    create_time = serializers.DateTimeField(format=settings.DATETIME_FORMAT, read_only=True)

    class Meta:
        model = Info
        fields = '__all__'
        read_only_fields = ['serial', 'owner', 'bmi', 'whr', 'degree_of_completion', 'check_status', 'reason_for_check']
        extra_kwargs = {
            'url': {'view_name': 'prj002-info-detail', 'lookup_field': 'pk'},
        }
        project_name = 'prj002'


class CheckStatusSerializer(serializers.Serializer):
    CHECKED_STATUS = ('未审核', '审核通过', '审核不通过')
    CHECKED_CHOICE = (('未审核', '未审核'), ('审核通过', '审核通过'), ('审核不通过', '审核不通过'))

    check_status = serializers.ChoiceField(required=True, choices=CHECKED_CHOICE, help_text='审核状态')
    reason_for_check = serializers.CharField(required=False, help_text='审核不通过原因')

    def validate_check_status(self, value):
        if value not in self.CHECKED_STATUS:
            raise serializers.ValidationError('非法审核状态')

        return value


class SummarySerializer(serializers.ModelSerializer):

    class Meta:
        model = Summary
        fields = '__all__'
        read_only_fields = ['owner']


class SummaryLinkSerializer(SCSerializer):

    class Meta:
        model = Summary
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj002-summary-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }


class ExperimentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Experiment
        fields = '__all__'
        read_only_fields = ['owner', 'yidaosu_dikang', 'yidaosu_mingan']

    def create(self, validate_data):
        instance = super().create(validate_data)
        instance = create_dikang_mingan(instance)
        return instance

    def update(self, instance, validate_data):
        for key, value in validate_data.items():
            if hasattr(instance, key):
                setattr(instance, key, value)

        instance.save()

        instance = create_dikang_mingan(instance)
        instance.save()
        return instance


class ExperimentLinkSerializer(SCSerializer):

    class Meta:
        model = Experiment
        fields = '__all__'
        read_only_fields = ['owner', 'yidaosu_dikang', 'yidaosu_mingan']
        extra_kwargs = {
            'url': {'view_name': 'prj002-experiment-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }

    def create(self, validate_data):
        instance = super().create(validate_data)
        instance = create_dikang_mingan(instance)
        return instance

    def update(self, instance, validate_data):
        for key, value in validate_data.items():
            if hasattr(instance, key):
                setattr(instance, key, value)

        instance.save()

        instance = create_dikang_mingan(instance)
        return instance


class CureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cure
        fields = '__all__'
        read_only_fields = ['owner']


class CureLinkSerializer(SCSerializer):

    class Meta:
        model = Cure
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj002-cure-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }


class ClinicalSerializer(SCSerializer):

    class Meta:
        model = Clinical
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj002-clinical-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }


class BxraySerializer(serializers.ModelSerializer):

    class Meta:
        model = Bxray
        fields = '__all__'
        read_only_fields = ['owner']


class BxrayLinkSerializer(SCSerializer):

    class Meta:
        model = Bxray
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj002-bxray-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }


class HistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        fields = '__all__'
        read_only_fields = ['owner']


class HistoryLinkSerializer(SCSerializer):

    class Meta:
        model = History
        fields = '__all__'
        read_only_fields = ['owner']
        extra_kwargs = {
            'url': {'view_name': 'prj002-history-detail', 'lookup_field': 'pk'},
            'info': {'view_name': 'prj002-info-detail', }
        }
