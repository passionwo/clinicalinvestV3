from django.urls import path

from prj002.views import (
    InfoView, SummaryView, HistoryView, ExperimentView,
    BxrayView, CureView, SearchView, ClinicalView,
)


many_view_func = {
    'get': 'list',
    'post': 'create'
}

single_view_func = {
    'get': 'retrieve',
    'patch': 'partial_update',
    'delete': 'destroy'
}


urlpatterns = [
    path('search/', SearchView.as_view({'post': 'list'}), name='prj002-info-search'),

    path('info/check/<int:pk>/', InfoView.as_view({'post': 'check'}), name='prj002-info-check'),

    path('info/', InfoView.as_view(many_view_func), name='prj002-info'),
    path('info/<int:pk>/', InfoView.as_view(single_view_func), name='prj002-info-detail'),

    path('summary/', SummaryView.as_view(many_view_func), name='prj002-summary'),
    path('summary/<int:pk>/', SummaryView.as_view(single_view_func), name='prj002-summary-detail'),

    path('history/', HistoryView.as_view(many_view_func), name='prj002-history'),
    path('history/<int:pk>/', HistoryView.as_view(single_view_func), name='prj002-history-detail'),

    path('experiment/', ExperimentView.as_view(many_view_func), name='prj002-experiment'),
    path('experiment/<int:pk>/', ExperimentView.as_view(single_view_func), name='prj002-experiment-detail'),

    path('bxray/', BxrayView.as_view(many_view_func), name='prj002-bxray'),
    path('bxray/<int:pk>/', BxrayView.as_view(single_view_func), name='prj002-bxray-detail'),

    path('clinical/', ClinicalView.as_view(many_view_func), name='prj002-clinical'),
    path('clinical/<int:pk>/', ClinicalView.as_view(single_view_func), name='prj002-clinical-detail'),

    path('cure/', CureView.as_view(many_view_func), name='prj002-cure'),
    path('cure/<int:pk>/', CureView.as_view(single_view_func), name='prj002-cure-detail'),
]
