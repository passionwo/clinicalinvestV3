from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    # {'zigong_chang': [ErrorDetail(string='请填写合法的整数值。', code='invalid')], 'info': [ErrorDetail(string='该字段必须唯一。', code='unique')]}
    # {'view': < prj002.views.BxrayView object at 0x118d6bfd0 > ,
    # 'args': (), 'kwargs': {},
    # 'request': < rest_framework.request.Request object at 0x118d7b0b8 > }
    # < Response status_code = 400, "text/html; charset=utf-8" > == == >> > 6
    if response is not None:
        response.data['status_code'] = response.status_code

    return response
