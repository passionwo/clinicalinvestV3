from django.shortcuts import reverse
from django.utils.html import format_html

from rest_framework import viewsets

from oauth2_provider.contrib.rest_framework import TokenHasScope

from prj002.permissions import DiffRolePermisssion


class SCView(viewsets.ModelViewSet):
    """
    A viewset that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()` and `list()` actions.

    Override .perform_create to custom the save instance
    """
    permission_classes = [TokenHasScope, DiffRolePermisssion]
    required_scopes = ['prj002']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class SCAdmin(object):

    def owner_link(self, obj):
        url = reverse('admin:myuser_myuser_change', args=[obj.owner.id])
        return format_html("<a href='{}'>{}</a>", url, obj.owner)

    def info_link(self, obj):
        url = reverse('admin:prj002_info_change', args=[obj.info.id])
        return format_html("<a href='{}'>{}</a>", url, obj.info.id)

    owner_link.short_description = '账户'
    info_link.short_description = '一般信息ID'
